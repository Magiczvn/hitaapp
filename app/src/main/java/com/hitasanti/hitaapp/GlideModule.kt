package com.hitasanti.hitaapp

import android.content.Context
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import com.bumptech.glide.load.engine.cache.LruResourceCache
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator
import com.bumptech.glide.module.AppGlideModule


@GlideModule
class GlideModule : AppGlideModule() {

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val calculator = MemorySizeCalculator.Builder(context)
            .build()
        builder.setMemoryCache(LruResourceCache((calculator.memoryCacheSize * 0.8).toLong()))
        builder.setBitmapPool(LruBitmapPool((calculator.bitmapPoolSize * 0.8).toLong()))
        builder.setDiskCache(InternalCacheDiskCacheFactory(context, 1024 * 1024 * 100)) //100MB
        super.applyOptions(context, builder)
    }

//    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
////        super.registerComponents(context, glide, registry)
//
//        val client = OkHttpClient.Builder()
//                .addNetworkInterceptor(GlideInterceptor(context))
////                .readTimeout(15, TimeUnit.SECONDS)
////                .connectTimeout(15, TimeUnit.SECONDS)
//                .build()
//
//        val factory = OkHttpUrlLoader.Factory(client)
//
//        glide.registry.replace(GlideUrl::class.java, InputStream::class.java, factory)
//    }
}