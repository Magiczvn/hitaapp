package com.hitasanti.hitaapp

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.AppComponent
import com.hitasanti.hitaapp.dependency.AppModule
import com.hitasanti.hitaapp.dependency.DaggerAppComponent
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.feature.login.User


open class HitaApplication : Application(), HasComponent<AppComponent>  {

    private lateinit var _AppComponent: AppComponent
    var pendingReload = false
    var reloading = false
    private var _ActivityLifecycle: ActivityLifecycle? = null

    companion object {
        fun get(context: Context): HitaApplication = context.applicationContext as HitaApplication
    }

    override fun onCreate() {
        super.onCreate()
        _AppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()


        AppEventsLogger.activateApp(this);

        User.load()

    }



    override fun onTerminate() {
        super.onTerminate()
    }

    override val component: AppComponent
        get() = _AppComponent


    inner class ActivityLifecycle(bus: RxBus) : ActivityLifecycleCallbacks {

        private val _Bus = bus


        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {

        }

        override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

        }

        override fun onActivityStarted(activity: Activity) {


        }

        override fun onActivityResumed(activity: Activity) {

        }

        override fun onActivityPaused(activity: Activity) {

        }

        override fun onActivityStopped(activity: Activity) {


        }

        override fun onActivityDestroyed(activity: Activity) {

        }
    }

}