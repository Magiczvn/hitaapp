package com.hitasanti.hitaapp.domain.impl


import com.hitasanti.hitaapp.data.model.GoogleAuthResponse
import com.hitasanti.hitaapp.data.model.RegisterRequest
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.repository.NewsRepository
import com.hitasanti.hitaapp.repository.ProductRepository
import com.hitasanti.hitaapp.repository.model.*
import dagger.Lazy
import io.reactivex.Single
import java.util.concurrent.Callable


class UseCaseFactoryImpl(private val _NewsRepository: Lazy<NewsRepository>,
                         private val _ProductRepository: Lazy<ProductRepository>) : UseCaseFactory {

    override fun getLatestArticle(paging: Int, category: Int): Single<List<Article>> {
        return _NewsRepository.get().getLatestArticle(paging, category)
    }

    override fun getProducts(category: Int): Single<List<Product>> {
        return _ProductRepository.get().getProducts(category)
    }

    override fun getProductVariations(productId: Int): Single<List<ProductVariation>> {
        return _ProductRepository.get().getProductVariations(productId)
    }

    override fun getTopProducts(category: Int, per_page: Int): Single<List<Product>> {
        return _ProductRepository.get().getTopProducts(category, per_page)
    }

    override fun getAllProducts(): Single<HashMap<Int, List<Product>>> {
        return _ProductRepository.get().getAllProducts()
    }

    override fun getCategories(parentCategory: Int): Single<List<Category>> {
        return _ProductRepository.get().getCategories(parentCategory)
    }

    override fun Login(username: String, password: String): Single<Boolean> {
        return _ProductRepository.get().Login(username, password)
    }

    override fun placeOrder(shippingNote: String, shippingName: String): Single<Optional<Order>> {
        return _ProductRepository.get().placeOrder(shippingNote, shippingName)
    }

    override fun cancelOrder(orderId: String, reason: String): Single<Optional<Order>> {
        return _ProductRepository.get().cancelOrder(orderId, reason)
    }

    override fun getOrdersHistory(): Single<List<Order>> {
        return _ProductRepository.get().getOrdersHistory()
    }

    override fun getGoogleAuthCode(authCode: String, clientId: String): Single<GoogleAuthResponse> {
        return _ProductRepository.get().getGoogleAuthCode(authCode, clientId)
    }

    override fun facebookLogin(accessToken: String, email: String): Single<Int> {
        return _ProductRepository.get().facebookLogin(accessToken, email)
    }

    override fun register(registerRequest: RegisterRequest): Single<Int> {
        return _ProductRepository.get().register(registerRequest)
    }

    override fun googleLogin(googleAuthResponse: GoogleAuthResponse): Single<Int> {
        return _ProductRepository.get().googleLogin(googleAuthResponse)
    }

    override fun getCoupon(code: String): Single<Coupon> {
        return _ProductRepository.get().getCoupon(code)
    }

    override fun getUserInfo(userId: String): Single<UserInfo> {
        return _ProductRepository.get().getUserInfo(userId)
    }

    override fun updateUserInfo(userId: String,first_name: String,last_name: String,address: String,phone: String): Single<UserInfo> {
        return _ProductRepository.get().updateUserInfo(userId, first_name, last_name, address, phone)
    }

    override fun <T> doWork(callable: Callable<T>): Single<T> = Single.fromCallable(callable)
}