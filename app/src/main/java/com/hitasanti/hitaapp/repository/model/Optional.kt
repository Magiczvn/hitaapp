package com.hitasanti.hitaapp.repository.model

class Optional<out T>(val value: T?) {

    override fun equals(other: Any?) = other is Optional<*> &&
            other.value == value
}