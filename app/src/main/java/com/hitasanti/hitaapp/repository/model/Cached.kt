package com.hitasanti.hitaapp.repository.model

class Cached<T>(val timeToLive: Long) {

    private var _Time: Long = System.currentTimeMillis()
    private var _Value: T? = null

    val isValid get() = System.currentTimeMillis() - _Time < timeToLive

    val value get() = _Value

    fun update(value: T?, sync: Boolean) {
        _Value = value
        if (sync) {
            _Time = System.currentTimeMillis()
        }
    }
}