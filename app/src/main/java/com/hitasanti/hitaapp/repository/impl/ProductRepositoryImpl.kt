package com.hitasanti.hitaapp.repository.impl

import com.google.gson.Gson
import com.hitasanti.hitaapp.data.ApiException
import com.hitasanti.hitaapp.data.model.*
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.categories.item.CategoryDetails
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.repository.ProductRepository
import com.hitasanti.hitaapp.repository.datasource.ProductDataSource
import com.hitasanti.hitaapp.repository.model.*
import dagger.Lazy
import io.reactivex.Single

class ProductRepositoryImpl(private val _NetworkDataSourceLazy: Lazy<ProductDataSource>): ProductRepository {


    private val _ProductsCached =  HashMap<Int, Cached<List<Product>>>()
    private val _VariationCached =  HashMap<Int, Cached<List<ProductVariation>>>()
    private val _CategoriesCached = HashMap<Int, Cached<List<Category>>>()
    val cachedTime = 24 * 60 * 60 * 1000L


    override fun getProducts(category: Int): Single<List<Product>> {
        return Single.create<List<Product>> { emitter ->
            try {
                var cached = _ProductsCached[category]
                if(cached == null) {
                    cached = Cached(cachedTime)
                    _ProductsCached[category] = cached
                }

                if(cached.value != null && cached.isValid)
                {
                    val products = cached.value.orEmpty()
                    emitter.onSuccess(products)
                }
                else
                {

                        val products = _NetworkDataSourceLazy.get()
                            .getProducts(category)

                        if (!emitter.isDisposed) {
                            cached.update(products, true)
                            emitter.onSuccess(products)
                        }

                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun getGoogleAuthCode(authCode: String, clientId: String): Single<GoogleAuthResponse> {
        return Single.create<GoogleAuthResponse> { emitter ->
            try {
              val code = _NetworkDataSourceLazy.get().getGoogleAuthCode(authCode, clientId)
                emitter.onSuccess(code)
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun facebookLogin(accessToken: String, email: String): Single<Int> {
        return Single.create<Int> { emitter ->
            try {
                val token = Gson().toJson(FacebookLoginRequest(accessToken))
                val userId = _NetworkDataSourceLazy.get().facebookLogin(token, email)
                emitter.onSuccess(userId)
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun register(registerRequest: RegisterRequest): Single<Int> {
        return Single.create<Int> { emitter ->
            try {
                val userId = _NetworkDataSourceLazy.get().register(registerRequest)
                emitter.onSuccess(userId)
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun googleLogin(googleAuthResponse: GoogleAuthResponse): Single<Int> {
        return Single.create<Int> { emitter ->
            try {
                val token = Gson().toJson(googleAuthResponse)
                val userId = _NetworkDataSourceLazy.get().googleLogin(token)
                emitter.onSuccess(userId)
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun getCoupon(code: String): Single<Coupon> {
        return Single.create<Coupon> { emitter ->
            try {
                val coupon = _NetworkDataSourceLazy.get().getCoupon(code)
                emitter.onSuccess(coupon)
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun getProductVariations(productId: Int): Single<List<ProductVariation>> {
        return Single.create<List<ProductVariation>> { emitter ->
            try {
                var cached = _VariationCached[productId]
                if(cached == null) {
                    cached = Cached(cachedTime)
                    _VariationCached[productId] = cached
                }

                if(cached.value != null && cached.isValid)
                {
                    val products = cached.value.orEmpty()
                    emitter.onSuccess(products)
                }
                else
                {

                    val products = _NetworkDataSourceLazy.get()
                        .getProductVariations(productId)

                    if (!emitter.isDisposed) {
                        cached.update(products, true)
                        emitter.onSuccess(products)
                    }

                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun getTopProducts(category: Int, per_page: Int): Single<List<Product>> {
        return Single.create<List<Product>> { emitter ->
            try {
                val products = _NetworkDataSourceLazy.get()
                    .getTopProducts(category, per_page)

                if (!emitter.isDisposed) {
                    emitter.onSuccess(products)
                }
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun getAllProducts(): Single<HashMap<Int, List<Product>>> {
        return Single.create<HashMap<Int, List<Product>>>{ emitter ->
            try {

                val result = HashMap<Int, List<Product>>()

                val products = _NetworkDataSourceLazy.get()
                    .getAllProducts()

                val temp = HashMap<Int, MutableList<Product>>()

                products.forEach {
                    val categories = it.categories.orEmpty()
                    categories.forEach {cate->
                        val id = cate.id
                        if(id != null && (/*id == CategoryDetails.HitaFarmCategory.categoryId
                                    ||*/id == CategoryDetails.HitaTeaCategory.categoryId
                                    //||id == CategoryDetails.HitaChayCategory.categoryId
                                    ||id == CategoryDetails.HitaCofeeCategory.categoryId
                                    ||id == CategoryDetails.HitaNhangCategory.categoryId)){
                            if(temp[id] == null){
                                temp[id] = mutableListOf()
                            }
                            val list = temp[id]
                            list?.add(it)
                        }
                    }
                }

                //result[CategoryDetails.HitaFarmCategory.categoryId] = temp[CategoryDetails.HitaFarmCategory.categoryId]?.toList().orEmpty()
                result[CategoryDetails.HitaTeaCategory.categoryId] = temp[CategoryDetails.HitaTeaCategory.categoryId]?.toList().orEmpty()
                //result[CategoryDetails.HitaChayCategory.categoryId] = temp[CategoryDetails.HitaChayCategory.categoryId]?.toList().orEmpty()
                result[CategoryDetails.HitaCofeeCategory.categoryId] = temp[CategoryDetails.HitaCofeeCategory.categoryId]?.toList().orEmpty()
                result[CategoryDetails.HitaNhangCategory.categoryId] = temp[CategoryDetails.HitaNhangCategory.categoryId]?.toList().orEmpty()

                if (!emitter.isDisposed) {
                    emitter.onSuccess(result)
                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun getCategories(parentCategory: Int): Single<List<Category>> {
        return Single.create<List<Category>> { emitter ->
            try {
                var cached = _CategoriesCached[parentCategory]
                if(cached == null) {
                    cached = Cached(cachedTime)
                    _CategoriesCached[parentCategory] = cached
                }

                if(cached.value != null && cached.isValid)
                {
                    val products = cached.value.orEmpty()
                    emitter.onSuccess(products)
                }
                else {
                    val products = _NetworkDataSourceLazy.get()
                        .getCategories(parentCategory)

                    if (!emitter.isDisposed) {
                        cached.update(products, true)
                        emitter.onSuccess(products)
                    }
                }
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun Login(username: String, password: String): Single<Boolean> {
        return Single.create<Boolean> { emitter ->
            try {

                val result = _NetworkDataSourceLazy.get()
                    .Login(username, password)

                if (!emitter.isDisposed) {
                    emitter.onSuccess(result)
                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun placeOrder(shippingNote: String, shippingName: String): Single<Optional<Order>> {
        return Single.create<Optional<Order>> { emitter ->
            try {

                val id = User.mId?:0
                val name = User.getUserName()
                val email = User.mEmail.orEmpty()
                val shipping = OrderRequest.Shipping(shippingName, Cart.shippingAddress)
                val billing = OrderRequest.Billing(name, Cart.shippingAddress, email, Cart.shippingPhone)
                val coupon_lines = Cart.getCouponLinesItem()
                val products = Cart.getLinesItem()
                val shippingLine = OrderRequest.ShippingLine(Cart.getShippingFee().toString())

                val order = OrderRequest(id, shippingNote, billing, shipping, products, coupon_lines, listOf(shippingLine), Cart.getDiscountPrice())

                val result = _NetworkDataSourceLazy.get()
                    .placeOrder(order)


                if (!emitter.isDisposed) {
                    emitter.onSuccess(Optional(result))
                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun cancelOrder(orderId: String, reason: String): Single<Optional<Order>> {

            return Single.create<Optional<Order>> { emitter ->
                try {


                    val order = CancelOrderRequest(reason)

                    val result = _NetworkDataSourceLazy.get()
                        .cancelOrder(orderId, order)


                    if (!emitter.isDisposed) {
                        emitter.onSuccess(Optional(result))
                    }

                } catch (ex: Exception) {
                    if (!emitter.isDisposed) {
                        emitter.onError(ex)
                    }
                }
            }

    }

    override fun getOrdersHistory(): Single<List<Order>> {
        return Single.create<List<Order>> { emitter ->
            try {
                val userId = User.mId?: throw ApiException("Bạn chưa đăng nhập!")

                val result = _NetworkDataSourceLazy.get()
                    .getOrdersHistory(userId)

                if (!emitter.isDisposed) {
                    emitter.onSuccess(result)
                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun getUserInfo(userId: String): Single<UserInfo> {
        return Single.create<UserInfo> { emitter ->
            try {
                val result = _NetworkDataSourceLazy.get()
                    .getUserInfo(userId)

                if (!emitter.isDisposed) {
                    emitter.onSuccess(result)
                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }

    override fun updateUserInfo(userId: String,first_name: String,last_name: String,address: String,phone: String): Single<UserInfo> {
        return Single.create<UserInfo> { emitter ->
            try {
                val addressInfo = EditInfoRequest.Address(first_name, last_name, address, phone)
                val request = EditInfoRequest(first_name, last_name, addressInfo)

                val result = _NetworkDataSourceLazy.get().updateUserInfo(userId, request)

                if (!emitter.isDisposed) {
                    emitter.onSuccess(result)
                }

            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }
}