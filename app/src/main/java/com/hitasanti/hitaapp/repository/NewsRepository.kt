package com.hitasanti.hitaapp.repository

import com.hitasanti.hitaapp.repository.model.Article
import io.reactivex.Single

interface NewsRepository {
    fun getLatestArticle(paging:Int, category: Int): Single<List<Article>>
}