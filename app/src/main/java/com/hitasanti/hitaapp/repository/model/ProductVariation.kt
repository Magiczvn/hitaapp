package com.hitasanti.hitaapp.repository.model

class ProductVariation(val id: Int, val name: String, val price:Int, val regularPrice:Int?, val productId: Int, val menuOrder: Int){
    companion object {
        const val queryString = "_fields=id,price,regular_price,attributes,menu_order"
    }
}