package com.hitasanti.hitaapp.repository.model

class OrderDetail(val name: String, val product_id: Int, val variation_id: Int?, val quantity: Int, val total: Int, val price: Int, val subTotal: Int)