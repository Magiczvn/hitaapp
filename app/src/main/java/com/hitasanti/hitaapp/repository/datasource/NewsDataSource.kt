package com.hitasanti.hitaapp.repository.datasource

import com.hitasanti.hitaapp.repository.model.Article

interface NewsDataSource {
    fun getLatestArticle(paging:Int, category: Int): List<Article>
}