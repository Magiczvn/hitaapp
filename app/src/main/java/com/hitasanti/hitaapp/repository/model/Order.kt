package com.hitasanti.hitaapp.repository.model

import java.util.*


open class Order(val id: Int, val price:Int, val dateCreated: Date, val dateCreatedGMT: Date, val shippingAddress: String, val status: String, val orderDetails: List<OrderDetail>, val shippingTotal: Int, val shippingPhone: String, val shippingName:String, val shippingNotes: String, val discountTotal: Int)