package com.hitasanti.hitaapp.repository.datasource

import com.hitasanti.hitaapp.data.model.*
import com.hitasanti.hitaapp.repository.model.*

interface ProductDataSource {
    fun getProducts(category: Int): List<Product>

    fun getProductVariations(productId: Int): List<ProductVariation>

    fun getTopProducts(category: Int, per_page: Int): List<Product>

    fun getAllProducts(): List<Product>

    fun getCategories(parentCategory: Int): List<Category>

    fun Login(username: String, password: String): Boolean

    fun register(registerRequest: RegisterRequest): Int

    fun placeOrder(order: OrderRequest): Order?

    fun cancelOrder(orderId: String, order: CancelOrderRequest): Order?

    fun getOrdersHistory(customerId: Int): List<Order>

    fun getGoogleAuthCode(authCode: String, clientId: String): GoogleAuthResponse

    fun facebookLogin(accessToken: String, email: String): Int

    fun googleLogin(accessToken: String): Int

    fun getCoupon(code: String): Coupon

    fun getUserInfo(userId: String): UserInfo

    fun updateUserInfo(userId: String, request: EditInfoRequest): UserInfo
}
