package com.hitasanti.hitaapp.repository.model

open class Article (val id: Int?, val link: String?, val title: String?, val excerpt: String?, val thumbnailURL: String?)