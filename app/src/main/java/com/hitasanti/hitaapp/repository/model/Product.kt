package com.hitasanti.hitaapp.repository.model

open class Product(val id: Int, val name: String, val price:Int, val shortDescription: String, val regularPrice:Int?, val variations: List<ProductVariation>?, val onSale: Boolean?, val purchasable: Boolean?, val shippingRequired: Boolean?, val categories: List<Category>? , val imgages: List<Image>?){
    companion object {
        const val queryString = "_fields=id,name,short_description,price,regular_price,on_sale,purchasable,variations_value,shipping_required,categories,images"
    }
}