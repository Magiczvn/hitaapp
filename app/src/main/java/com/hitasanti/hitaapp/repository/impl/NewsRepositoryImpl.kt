package com.hitasanti.hitaapp.repository.impl

import com.hitasanti.hitaapp.repository.NewsRepository
import com.hitasanti.hitaapp.repository.datasource.NewsDataSource
import com.hitasanti.hitaapp.repository.model.Article
import dagger.Lazy
import io.reactivex.Single

class NewsRepositoryImpl(private val _NetworkDataSourceLazy: Lazy<NewsDataSource>):NewsRepository {
    override fun getLatestArticle(paging: Int, category: Int): Single<List<Article>> {
        return Single.create<List<Article>> { emitter ->
            try {
                val articles = _NetworkDataSourceLazy.get()
                    .getLatestArticle(paging, category)

                if (!emitter.isDisposed) {
                    emitter.onSuccess(articles)
                }
            } catch (ex: Exception) {
                if (!emitter.isDisposed) {
                    emitter.onError(ex)
                }
            }
        }
    }
}