package com.hitasanti.hitaapp.repository.model

open class Category(val id: Int?, val name: String?)