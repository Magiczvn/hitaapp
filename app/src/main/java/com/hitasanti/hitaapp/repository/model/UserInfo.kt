package com.hitasanti.hitaapp.repository.model

class UserInfo(val id: Int, val first_name:String, val last_name: String, val username: String, val avatar_url: String ,val email: String, val shippingAddress: String, val phone: String)