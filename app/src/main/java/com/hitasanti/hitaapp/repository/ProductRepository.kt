package com.hitasanti.hitaapp.repository

import com.hitasanti.hitaapp.data.model.GoogleAuthResponse
import com.hitasanti.hitaapp.data.model.RegisterRequest
import com.hitasanti.hitaapp.repository.model.*
import io.reactivex.Single

interface ProductRepository {
    fun getProducts(category: Int): Single<List<Product>>

    fun getProductVariations(productId: Int): Single<List<ProductVariation>>

    fun getTopProducts(category: Int, per_page: Int): Single<List<Product>>

    fun getAllProducts(): Single<HashMap<Int, List<Product>>>

    fun getCategories(parentCategory: Int): Single<List<Category>>

    fun Login(username: String, password: String): Single<Boolean>

    fun register(registerRequest: RegisterRequest): Single<Int>

    fun placeOrder(shippingNote: String, shippingName: String): Single<Optional<Order>>

    fun cancelOrder(orderId: String, reason: String): Single<Optional<Order>>

    fun getOrdersHistory(): Single<List<Order>>

    fun getGoogleAuthCode(authCode: String, clientId: String): Single<GoogleAuthResponse>

    fun facebookLogin(accessToken: String, email: String): Single<Int>

    fun googleLogin(googleAuthResponse: GoogleAuthResponse): Single<Int>

    fun getCoupon(code: String): Single<Coupon>

    fun getUserInfo(userId: String): Single<UserInfo>

    fun updateUserInfo(userId: String, first_name: String, last_name: String, address: String, phone: String): Single<UserInfo>
}