package com.hitasanti.hitaapp.repository.model

open class Image (val id: Int?, val link: String?, val name: String?)