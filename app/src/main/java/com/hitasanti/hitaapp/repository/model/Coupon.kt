package com.hitasanti.hitaapp.repository.model

import java.util.*

class Coupon(val id: Int, val code: String, val amount: Float, val discount_type: String, val date_expires: Date?, val usage_count: Int?, val product_ids: List<Int>, val excluded_product_ids: List<Int>, val usage_limit: Int?, val usage_limit_per_user: Int?, val free_shipping: Boolean, val product_categories:List<Int>, val excluded_product_categories: List<Int>, val exclude_sale_items: Boolean, val minimum_amount: Float, val maximum_amount: Float, val used_by: List<String>)