package com.hitasanti.hitaapp.view

import android.animation.Animator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.graphics.Canvas
import android.graphics.Paint
import android.view.MotionEvent
import android.view.View
import android.view.animation.DecelerateInterpolator
import java.util.*


class RippleAnimator(private val view: View) {
    private val PROPERTY_RADIUS = "radius"
    private val PROPERTY_ALPHA = "alpha"
    private var duration: Long = 0
    private var alpha = 0
    private val rippleObjects: MutableList<RippleObject>
    private val paint: Paint
    fun setDuration(duration: Long) {
        this.duration = duration
    }

    fun setAlpha(alpha: Int) {
        this.alpha = alpha
    }

    fun setColor(color: Int) {
        paint.color = color
    }

    /**
     * Create new [RippleObject] and re-draw the [.view]
     * if the given event is action down.
     */
    fun rippleWith(event: MotionEvent) {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val rippleObject = RippleObject()
            rippleObject.x = view.width / 2.toFloat()
            rippleObject.y = view.height / 2.toFloat()
            rippleObjects.add(rippleObject)
            val alphaHolder =
                PropertyValuesHolder.ofInt(PROPERTY_ALPHA, alpha, 0)
            val radiusHolder = PropertyValuesHolder.ofFloat(
                PROPERTY_RADIUS,
                0f,
                calcMaxRadius(rippleObject.x, rippleObject.y)
            )
            val animator = ValueAnimator()
            animator.setValues(alphaHolder, radiusHolder)
            animator.duration = duration
            animator.interpolator = DecelerateInterpolator()
            animator.addUpdateListener { animation: ValueAnimator ->
                rippleObject.a = animation.getAnimatedValue(PROPERTY_ALPHA) as Int
                rippleObject.r = animation.getAnimatedValue(PROPERTY_RADIUS) as Float
                view.invalidate()
            }
            animator.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    rippleObjects.remove(rippleObject)
                }

                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
            })
            animator.start()
        }
    }

    /**
     * Apply ripple animation to the given canvas using info from [.rippleObjects].
     */
    fun rippleOn(canvas: Canvas) {
        for (rippleObject in rippleObjects) {
            rippleObject.draw(canvas, paint)
        }
    }

    /**
     * Calculate distance from given coordinate to the farthest corner in [.view].
     */
    private fun calcMaxRadius(x: Float, y: Float): Float {
        val cornerX =
            if (2 * x < view.measuredWidth) view.measuredWidth.toFloat() else 0.toFloat()
        val cornerY =
            if (2 * y < view.measuredHeight) view.measuredHeight.toFloat() else 0.toFloat()
        return Math.sqrt(
            Math.pow(
                x - cornerX.toDouble(),
                2.0
            ) + Math.pow(y - cornerY.toDouble(), 2.0)
        ).toFloat()
    }

    internal inner class RippleObject {
        var x = 0f
        var y = 0f
        var r = 0f
        var a = 0
        fun draw(canvas: Canvas, paint: Paint) {
            paint.alpha = a
            canvas.drawCircle(x, y, r, paint)
        }
    }

    init {
        rippleObjects = ArrayList()
        paint = Paint()
        paint.isAntiAlias = true
        view.clipToOutline = true
    }
}
