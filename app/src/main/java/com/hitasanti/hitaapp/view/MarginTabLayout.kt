package com.hitasanti.hitaapp.view

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayoutSmoothScroll
import com.hitasanti.hitaapp.R

class MarginTabLayout : TabLayoutSmoothScroll {

    private var _InnerMargin = 0f
    private var _OuterMargin = 0f

    constructor(context: Context?) : super(context) {
        init(null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(attrs: AttributeSet?, defStyleAttr: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.MarginTabLayout, defStyleAttr, 0)
        _InnerMargin = a.getDimension(R.styleable.MarginTabLayout_tabMarginInner, _InnerMargin)
        _OuterMargin = a.getDimension(R.styleable.MarginTabLayout_tabMarginOuter, _OuterMargin)
        a.recycle()
    }

    override fun onPaperAdapterChanged() {
        (getChildAt(0) as? ViewGroup)?.let {
            for (i in 0 until it.childCount) {
                val tabView = it.getChildAt(i)
                val tabViewLayoutParams = tabView.layoutParams as ViewGroup.MarginLayoutParams
                when (i) {
                    0 -> {
                        tabViewLayoutParams.leftMargin = _OuterMargin.toInt()
                        tabViewLayoutParams.rightMargin = _InnerMargin.toInt()
                    }
                    it.childCount - 1 -> {
                        tabViewLayoutParams.leftMargin = _InnerMargin.toInt()
                        tabViewLayoutParams.rightMargin = _OuterMargin.toInt()
                    }
                    else -> {
                        tabViewLayoutParams.leftMargin = _InnerMargin.toInt()
                        tabViewLayoutParams.rightMargin = _InnerMargin.toInt()
                    }
                }
            }
        }
    }
}