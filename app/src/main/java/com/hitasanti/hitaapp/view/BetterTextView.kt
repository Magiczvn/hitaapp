package com.hitasanti.hitaapp.view

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import com.hitasanti.hitaapp.R


class BetterTextView: AdjustPaddingTextView {

    private var ellipsisStart = -1
    private var ellipsisScale = 1f
    private lateinit var ellipsisSpannable: SpannableString
    private var spans = emptyList<Any>()
    private val builder = SpannableStringBuilder()

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(attrs: AttributeSet?, defStyleAttr: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.BetterTextView, defStyleAttr, 0)
        val ellipsis = a.getString(R.styleable.BetterTextView_ellipsis)
            ?: getDefaultEllipsis().toString()
        val ellipsisColor = a.getColor(R.styleable.BetterTextView_ellipsisColor, getDefaultEllipsisColor())
        ellipsisStart = a.getInteger(R.styleable.BetterTextView_ellipsisStart, ellipsisStart)
        ellipsisScale = a.getFloat(R.styleable.BetterTextView_ellipsisScale, ellipsisScale)
        a.recycle()

        ellipsisSpannable = SpannableString(ellipsis)
        setEllipsisColor(ellipsisColor)
    }

    fun setEllipsisColor(ellipsisColor: Int) {
        if (ellipsisStart > -1) {
            spans.forEach { ellipsisSpannable.removeSpan(it) }
            val foregroundColorSpan = ForegroundColorSpan(ellipsisColor)
            ellipsisSpannable.setSpan(foregroundColorSpan, ellipsisStart, ellipsisSpannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            val relativeSizeSpan = RelativeSizeSpan(ellipsisScale)
            ellipsisSpannable.setSpan(relativeSizeSpan, ellipsisStart, ellipsisSpannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spans = listOf(foregroundColorSpan, relativeSizeSpan)
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        val layout = layout ?: return

        val line = layout.lineCount
        if (line <= 0) {
            return
        }

        val dropCount = layout.getEllipsisCount(line - 1)
        if (dropCount <= 0) {
            return
        }

        val offset = layout.getLineEnd(line - 1) - dropCount - ellipsisSpannable.length - 2
        if (offset < 0) {
            return
        }

        val charSequence = text
        val offset1 = charSequence.toString().lastIndexOf(' ', offset)
        if (offset1 < 0) {
            return
        }

        post {
            if (charSequence.toString() != text.toString()) return@post
            builder.clear()
            builder.clearSpans()
            builder.append(charSequence.subSequence(0, offset1))
            builder.append(ellipsisSpannable)
            text = builder
        }
    }

    private fun getDefaultEllipsis(): Char? {
        return Typography.ellipsis
    }

    private fun getDefaultEllipsisColor(): Int {
        return textColors.defaultColor
    }

    fun adjustMaxLines(maxHeight: Int?, originContent: String?): Boolean {
        maxHeight ?: return false
        originContent ?: return false
        val maxLines = (maxHeight - paddingTop - paddingBottom)/ lineHeight
        super.setMaxLines(maxLines)
        if (maxLines == 0) {
            super.setText("")
            return false
        } else {
            super.setText(originContent)
            return true
        }
        postInvalidate()
    }
}