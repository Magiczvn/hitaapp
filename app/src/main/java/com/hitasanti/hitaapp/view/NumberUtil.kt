package com.hitasanti.hitaapp.view

import android.content.Context
import com.hitasanti.hitaapp.R
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object NumberUtil {
    private val formatter = DecimalFormat("#,###")
    private val dateFormatter = SimpleDateFormat("HH:mm dd/MM/yyyy")
    private val phonePattern = Pattern.compile("(84|0[3|5|7|8|9])+([0-9]{8})\\b")

   fun formatPrice(context:Context, price: Int): String{
       return context.getString(R.string.txt_price_template, formatter.format(price))
   }

    fun formatDateTime(dateTime: Date): String{
        return dateFormatter.format(dateTime)
    }

    fun isValidPhoneNumber(phoneNumber: String): Boolean {
        return phonePattern.matcher(phoneNumber.trim { it <= ' ' }).matches()
    }

}