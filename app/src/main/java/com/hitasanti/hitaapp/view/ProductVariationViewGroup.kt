package com.hitasanti.hitaapp.view



import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioGroup
import com.hitasanti.hitaapp.repository.model.ProductVariation


class ProductVariationViewGroup : LinearLayout, View.OnClickListener {

    interface onCheckChangedListener{
        fun onCheckChanged(variationView: ProductVariationView)
    }

    private val variationViews = mutableListOf<ProductVariationView>()
    private var selectedView: ProductVariationView? = null
    private var checkChangedListener: onCheckChangedListener? = null


    override fun onClick(p0: View?) {
        if(p0 === selectedView)
            return

        selectedView = p0 as? ProductVariationView?
        variationViews.forEach {
            it.isChecked = false
        }
        selectedView?.onClick()
        if(selectedView != null)
            checkChangedListener?.onCheckChanged(selectedView!!)
    }

    override fun setClickable(clickable: Boolean) {
        variationViews.forEach {
            it.isClickable = clickable
        }
        super.setClickable(clickable)
    }

    fun setDefaultSelected(){
        if(variationViews.isNotEmpty())
            selectedView = variationViews[0]
        selectedView?.isChecked = true
        if(selectedView != null)
            checkChangedListener?.onCheckChanged(selectedView!!)
    }

    fun setSelected(variationId: Int){
        variationViews.forEach {
            if(it.productVariation?.id == variationId){
                selectedView = it
                selectedView?.isChecked = true
                if(selectedView != null)
                    checkChangedListener?.onCheckChanged(selectedView!!)
            }
        }
    }

    fun setOnCheckChangedListener(listener: onCheckChangedListener){
        checkChangedListener = listener
    }

    fun getSelectedVariation(): ProductVariation? {
        return  selectedView?.productVariation
    }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(attrs: AttributeSet?, defStyleAttr: Int) {
        orientation = LinearLayout.VERTICAL
    }

    override fun addView(child: View?) {
        child?.setOnClickListener(this)
        variationViews.add(child as ProductVariationView)
        super.addView(child)
    }

    override fun removeView(view: View?) {
        view?.setOnClickListener(null)
        variationViews.remove(view as ProductVariationView)
        super.removeView(view)
    }


}