package com.hitasanti.hitaapp.view

import android.content.Context
import android.util.AttributeSet

class ClosableRecyclerView : BaseRecyclerView, PullToCloseLayout.ClosableView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override val isReadyForPull: Boolean
        get() =  !canScrollVertically(1)
}