package com.hitasanti.hitaapp.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.hitasanti.hitaapp.R


class FixedWidthRatioFrameLayout : FrameLayout {
    private var _Ratio = 16 / 9f

    constructor(context: Context?) : super(context!!) {
        init(null, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
        init(attrs, 0)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context!!, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    fun setRatio(ratio: Float) {
        if (ratio == _Ratio) return
        _Ratio = ratio
        requestLayout()
    }

    private fun init(attrs: AttributeSet?, defStyleAttr: Int) {
        val a = context.obtainStyledAttributes(
            attrs,
            R.styleable.FixedWidthRatioFrameLayout,
            defStyleAttr,
            0
        )
        _Ratio = a.getFloat(R.styleable.FixedWidthRatioFrameLayout_fl_ratio, _Ratio)
        a.recycle()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val heightSpec = MeasureSpec.getMode(heightMeasureSpec)
        if (heightSpec == MeasureSpec.EXACTLY) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        } else {
            val width = MeasureSpec.getSize(widthMeasureSpec)
            val height =
                Math.round((width - paddingLeft - paddingRight) / _Ratio) + paddingTop + paddingBottom
            super.onMeasure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
            )
        }
    }
}
