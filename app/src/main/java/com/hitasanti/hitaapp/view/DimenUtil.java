package com.hitasanti.hitaapp.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class DimenUtil {

    public static int dpToPx(Context context, int dp) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics()) + 0.5f);
    }

    public static float dpToPx(Context context, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static float pxToDp(Context context, int px){
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int getNavigationBarHeight(Context context) {
        if (context != null) {
            int resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                return context.getResources().getDimensionPixelSize(resourceId);
            }
            return 0;
        }

        return -1;
    }

    public static boolean isSystemBarOnBottom(Context context) {
        try{
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Point realPoint = new Point();
            Display display = wm.getDefaultDisplay();
            display.getRealSize(realPoint);
            DisplayMetrics metrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(metrics);
            Configuration cfg = context.getResources().getConfiguration();
            boolean canMove = (metrics.widthPixels != metrics.heightPixels &&
                    cfg.smallestScreenWidthDp < 600);
            return (!canMove || metrics.widthPixels < metrics.heightPixels);
        }catch (Exception ex){
            return false;
        }
    }

    public static boolean hasNavigationBar(Activity activity, View rootView) {
        if (activity == null || rootView == null)
            return true;

        int realHeight = 0;
        int viewHeight = 0;
        try {
            Display d = activity.getWindowManager().getDefaultDisplay();
            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);

            viewHeight = rootView.getHeight();
            if (viewHeight == 0)
                return true;

            realHeight = realDisplayMetrics.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return realHeight != viewHeight;
    }

    public static boolean hasNavBar(Context context) {
        Point realSize = new Point();
        Point screenSize = new Point();
        boolean hasNavBar = false;
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager windowManager =
                    (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getRealMetrics(metrics);
            realSize.x = metrics.widthPixels;
            realSize.y = metrics.heightPixels;
            windowManager.getDefaultDisplay().getSize(screenSize);
            if (realSize.y != screenSize.y) {
                int difference = realSize.y - screenSize.y;
                int navBarHeight = 0;
                Resources resources = context.getResources();
                int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
                if (resourceId > 0) {
                    navBarHeight = resources.getDimensionPixelSize(resourceId);
                }
                if (navBarHeight != 0) {
                    if (difference == navBarHeight) {
                        hasNavBar = true;
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasNavBar;

    }

    public static int getScreenHeightIncludeNavigationBar(Context context) {
        int realScreenHeight = 0;
        if (context != null) {
            try {
                WindowManager windowManager =
                        (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                final Display display = windowManager.getDefaultDisplay();
                Point outPoint = new Point();
                // include navigation bar
                display.getRealSize(outPoint);

                if (outPoint.y > outPoint.x) {
                    realScreenHeight = outPoint.y;
                } else {
                    realScreenHeight = outPoint.x;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return realScreenHeight;
    }

    public static int getScreenWidthInMultiWindowMode(Context context) {
        int screenWidth = 0;
        if (context != null) {
            try {
                WindowManager windowManager =
                        (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                final Display display = windowManager.getDefaultDisplay();
                Point outPoint = new Point();
                // include navigation bar
                display.getSize(outPoint);
                screenWidth = outPoint.x;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return screenWidth;
    }

    public static int getStatusBarHeight(Context context) {
        if (context != null) {
            int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                return context.getResources().getDimensionPixelSize(resourceId);
            }
            return 0;
        }

        return -1;
    }
}
