package com.hitasanti.hitaapp.view

import android.content.Context
import android.content.res.Configuration
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import android.util.AttributeSet
import android.view.View
import com.hitasanti.hitaapp.R

import kotterknife.bindDimenInt

open class BaseRecyclerView : RecyclerView {

    private val _PaddingPortrait: Int by bindDimenInt(R.dimen.paddingListPort)
    private val _PaddingLandscape: Int by bindDimenInt(R.dimen.paddingListLand)

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        try {
            super.onLayout(changed, l, t, r, b)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setScrollBarColor(color: Int) {
        try {
            val mScrollCacheField = View::class.java.getDeclaredField("mScrollCache")
            mScrollCacheField.isAccessible = true
            val mScrollCache = mScrollCacheField.get(this)

            val scrollBarField = mScrollCache.javaClass.getDeclaredField("scrollBar")
            scrollBarField.isAccessible = true
            val scrollBar = scrollBarField.get(mScrollCache)

            val method = scrollBar.javaClass.getDeclaredMethod("setVerticalThumbDrawable", Drawable::class.java)
            method.isAccessible = true

            // Set your drawable here.
            method.invoke(scrollBar, ColorDrawable(color))
        } catch (e: Exception) {
        }
    }

    fun setOrientation(orientation: Int) {
        when (orientation) {
            Configuration.ORIENTATION_PORTRAIT -> setPadding(_PaddingPortrait, paddingTop, _PaddingPortrait, paddingBottom)
            Configuration.ORIENTATION_LANDSCAPE -> setPadding(_PaddingLandscape, paddingTop, _PaddingLandscape, paddingBottom)
        }
    }
}