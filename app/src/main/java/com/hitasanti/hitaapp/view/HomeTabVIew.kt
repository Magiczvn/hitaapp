package com.hitasanti.hitaapp.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Checkable
import android.widget.FrameLayout
import java.util.*

class HomeTabView : FrameLayout {
    interface OnCurrentTabChangedListener {
        fun onTabChanged(tab: Int)
        fun onTabReselected(tab: Int)
    }

    private val mCheckableViews: MutableList<Checkable> =
        ArrayList()
    private var mCurrentTab = 0
    private var mOnCurrentTabChangedListener: OnCurrentTabChangedListener? = null

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context!!, attrs, defStyleAttr) {
    }

    fun setOnCurrentTabChangedListener(listener: OnCurrentTabChangedListener?) {
        mOnCurrentTabChangedListener = listener
    }

    var currentTab: Int
        get() = mCurrentTab
        set(tab) {
            val v = findViewById<View>(tab)
            if (v is Checkable) onChildClick(v as Checkable, false)
        }

    private fun onChildClick(child: Checkable, byUser: Boolean) {
        for (view in mCheckableViews) {
            if (view !== child) view.isChecked = false
        }
        child.isChecked = true
        val tab = (child as View).id
        if (mCurrentTab != tab) {
            mCurrentTab = tab
            if (mOnCurrentTabChangedListener != null) mOnCurrentTabChangedListener!!.onTabChanged(
                mCurrentTab
            )
        } else if (mOnCurrentTabChangedListener != null && byUser) {
            mOnCurrentTabChangedListener!!.onTabReselected(mCurrentTab)
        }
    }

    override fun onViewAdded(child: View) {
        super.onViewAdded(child)
        if (child is Checkable) {
            mCheckableViews.add(child as Checkable)
            child.setOnClickListener { view: View ->
                onChildClick(
                    view as Checkable,
                    true
                )
            }
        }
    }

    override fun onViewRemoved(child: View) {
        super.onViewRemoved(child)
        if (child is Checkable) {
            mCheckableViews.remove(child)
            child.setOnClickListener(null)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        val childCount = childCount
        if (childCount > 0) {
            val childWidth = width / childCount
            for (i in 0 until childCount) {
                val child = getChildAt(i)
                if (i < childCount - 1) child.measure(
                    MeasureSpec.makeMeasureSpec(
                        childWidth,
                        MeasureSpec.EXACTLY
                    ), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                ) else child.measure(
                    MeasureSpec.makeMeasureSpec(
                        width - childWidth * (childCount - 1),
                        MeasureSpec.EXACTLY
                    ), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
                )
            }
        }
        setMeasuredDimension(width, height)
    }

    override fun onLayout(
        changed: Boolean,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int
    ) {
        var childLeft = 0
        val childTop = 0
        var i = 0
        val count = childCount
        while (i < count) {
            val child = getChildAt(i)
            child.layout(
                childLeft,
                childTop,
                childLeft + child.measuredWidth,
                childTop + child.measuredHeight
            )
            childLeft += child.measuredWidth
            i++
        }
    }
}