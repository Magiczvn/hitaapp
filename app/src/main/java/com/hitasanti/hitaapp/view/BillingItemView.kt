package com.hitasanti.hitaapp.view


import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.hitasanti.hitaapp.R
import kotterknife.bindView

class BillingItemView : LinearLayout {
    private val _TempPrice: TextView by bindView(R.id.cart_temp_price)
    private val _ShippingPrice: TextView by bindView(R.id.cart_shipping_price)
    private val _TotalPrice: TextView by bindView(R.id.cart_total_price)
    private val _DiscountPrice: TextView by bindView(R.id.cart_discount_price)
    private val _DiscountView: View by bindView(R.id.cart_discount_layout)

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        clipToPadding = false
    }

    fun setTempPrice(price: Int){
        _TempPrice.text = NumberUtil.formatPrice(context, price)
    }

    fun setShippingPrice(price: Int){
        _ShippingPrice.text = NumberUtil.formatPrice(context, price)
    }

    fun setTotalPrice(price: Int){
        _TotalPrice.text = NumberUtil.formatPrice(context, price)
    }

    fun setDiscountPrice(price: Int){
        _DiscountView.visibility = View.VISIBLE
        _DiscountPrice.text = NumberUtil.formatPrice(context, price)
    }




}