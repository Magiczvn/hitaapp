package com.hitasanti.hitaapp.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

import com.epi.app.drawable.LinearProgressDrawable;
import com.hitasanti.hitaapp.R;

public class ProgressView extends View {

    private boolean mAutostart = false;
    private boolean mCircular = true;
    private int mProgressWidth = -1;
    private int mProgressHeight = -1;
    private int mProgressId;

    public static final int MODE_DETERMINATE = 0;
    public static final int MODE_INDETERMINATE = 1;
    public static final int MODE_BUFFER = 2;
    public static final int MODE_QUERY = 3;

    private Drawable mProgressDrawable;

    public ProgressView(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    protected void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        setWillNotDraw(false);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProgressView, defStyleAttr, defStyleRes);

        int progressId = 0;
        int progressMode = -1;
        float progress = -1;
        float secondaryProgress = -1;

        for (int i = 0, count = a.getIndexCount(); i < count; i++) {
            int attr = a.getIndex(i);

            if (attr == R.styleable.ProgressView_pv_autostart)
                mAutostart = a.getBoolean(attr, false);
            else if (attr == R.styleable.ProgressView_pv_circular)
                mCircular = a.getBoolean(attr, true);
            else if (attr == R.styleable.ProgressView_pv_progressStyle)
                progressId = a.getResourceId(attr, 0);
            else if (attr == R.styleable.ProgressView_pv_progressMode)
                progressMode = a.getInteger(attr, 0);
            else if (attr == R.styleable.ProgressView_pv_progress)
                progress = a.getFloat(attr, 0);
            else if (attr == R.styleable.ProgressView_pv_secondaryProgress)
                secondaryProgress = a.getFloat(attr, 0);
            else if (attr == R.styleable.ProgressView_pv_width)
                mProgressWidth = a.getDimensionPixelSize(attr, 0);
            else if (attr == R.styleable.ProgressView_pv_height)
                mProgressHeight = a.getDimensionPixelSize(attr, 0);
        }

        a.recycle();

        boolean needStart = false;

        if (needCreateProgress(mCircular)) {
            mProgressId = progressId;
            if (mProgressId == 0)
                mProgressId = mCircular ? R.style.CircularProgressDrawable : R.style.LinearProgressDrawable;

            needStart = mProgressDrawable != null && ((Animatable) mProgressDrawable).isRunning();
            if (mProgressDrawable != null)
                mProgressDrawable.setCallback(null);
            mProgressDrawable = mCircular ? new CircularProgressDrawable.Builder(context, mProgressId).build() : new LinearProgressDrawable.Builder(context, mProgressId).build();
            mProgressDrawable.setCallback(this);
        } else if (mProgressId != progressId) {
            mProgressId = progressId;
            if (mProgressDrawable instanceof CircularProgressDrawable)
                ((CircularProgressDrawable) mProgressDrawable).applyStyle(context, mProgressId);
            else
                ((LinearProgressDrawable) mProgressDrawable).applyStyle(context, mProgressId);
        }

        if (progressMode >= 0) {
            if (mProgressDrawable instanceof CircularProgressDrawable)
                ((CircularProgressDrawable) mProgressDrawable).setProgressMode(progressMode);
            else
                ((LinearProgressDrawable) mProgressDrawable).setProgressMode(progressMode);
        }

        if (progress >= 0)
            setProgress(progress);

        if (secondaryProgress >= 0)
            setSecondaryProgress(secondaryProgress);

        if (needStart)
            start();
    }

    private boolean needCreateProgress(boolean circular) {
        if (mProgressDrawable == null)
            return true;

        if (circular)
            return !(mProgressDrawable instanceof CircularProgressDrawable);
        else
            return !(mProgressDrawable instanceof LinearProgressDrawable);
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);

        if (changedView != this)
            return;

        if (mAutostart) {
            if (visibility == GONE || visibility == INVISIBLE)
                stop();
            else
                start();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (getVisibility() == View.VISIBLE && mAutostart)
            start();
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mAutostart)
            stop();
        super.onDetachedFromWindow();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        mProgressDrawable.setState(getDrawableState());
    }

    @Override
    protected boolean verifyDrawable(@NonNull Drawable dr) {
        return super.verifyDrawable(dr) || dr == mProgressDrawable;
    }

    @Override
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        mProgressDrawable.jumpToCurrentState();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (mProgressWidth <= 0 || mProgressHeight <= 0)
            mProgressDrawable.setBounds(0, 0, w, h);
        else {
            int l = (w - mProgressWidth) / 2;
            int t = (h - mProgressHeight) / 2;
            mProgressDrawable.setBounds(l, t, l + mProgressWidth, t + mProgressHeight);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mProgressDrawable.draw(canvas);
    }

    public int getProgressMode() {
        if (mCircular)
            return ((CircularProgressDrawable) mProgressDrawable).getProgressMode();
        else
            return ((LinearProgressDrawable) mProgressDrawable).getProgressMode();
    }

    public void setStrokeCap(Paint.Cap strokeCap) {
        if (mCircular)
            return;

        ((LinearProgressDrawable) mProgressDrawable).setStrokeCap(strokeCap);
    }

    public void setStrokeSize(int size) {
        if (mCircular) {
            ((CircularProgressDrawable) mProgressDrawable).setStrokeSize(size);
        } else {
            ((LinearProgressDrawable) mProgressDrawable).setStrokeSize(size);
        }
    }

    public void setStrokeColor(@ColorInt int color) {
        if (mCircular) {
            ((CircularProgressDrawable) mProgressDrawable).setStrokeColor(color);
        } else {
            ((LinearProgressDrawable) mProgressDrawable).setStrokeColor(color);
        }
    }

    public void setStrokeSecondaryColor(@ColorInt int color) {
        if (mCircular)
            return;

        ((LinearProgressDrawable) mProgressDrawable).setStrokeSecondaryColor(color);
    }

    public void setProgress(float percent) {
        if (mCircular)
            ((CircularProgressDrawable) mProgressDrawable).setProgress(percent);
        else
            ((LinearProgressDrawable) mProgressDrawable).setProgress(percent);
    }

    public float getProgress() {
        if (mCircular)
            return ((CircularProgressDrawable) mProgressDrawable).getProgress();
        else
            return ((LinearProgressDrawable) mProgressDrawable).getProgress();
    }

    public void setSecondaryProgress(float percent) {
        if (mCircular)
            ((CircularProgressDrawable) mProgressDrawable).setSecondaryProgress(percent);
        else
            ((LinearProgressDrawable) mProgressDrawable).setSecondaryProgress(percent);
    }

    public float getSecondaryProgress() {
        if (mCircular)
            return ((CircularProgressDrawable) mProgressDrawable).getSecondaryProgress();
        else
            return ((LinearProgressDrawable) mProgressDrawable).getSecondaryProgress();
    }

    public void start() {
        if (mProgressDrawable != null)
            ((Animatable) mProgressDrawable).start();
    }

    public void stop() {
        if (mProgressDrawable != null)
            ((Animatable) mProgressDrawable).stop();
    }
}
