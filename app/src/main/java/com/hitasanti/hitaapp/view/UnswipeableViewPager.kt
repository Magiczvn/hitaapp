package com.hitasanti.hitaapp.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent

class UnswipeableViewPager : BetterViewPager {

    private var _Swipeable = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    fun setSwipeable(swipeable: Boolean) {
        _Swipeable = swipeable
    }

    override fun onInterceptTouchEvent(ev: MotionEvent) = _Swipeable && super.onInterceptTouchEvent(ev)

    override fun onTouchEvent(ev: MotionEvent) = _Swipeable && super.onTouchEvent(ev)
}