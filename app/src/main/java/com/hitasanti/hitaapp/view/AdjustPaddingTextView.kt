package com.hitasanti.hitaapp.view

import android.content.Context
import android.graphics.Rect
import androidx.appcompat.widget.AppCompatTextView
import android.util.AttributeSet
import com.hitasanti.hitaapp.R

open class AdjustPaddingTextView: AppCompatTextView {

    private val TEMP = "A"
    private val mRect = Rect()
    private var mBaseText = TEMP
    private var mAdjustTop = true
    private var mAdjustBottom = true
    private var mPaddingTop = paddingTop
    private var mPaddingBottom = paddingBottom

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    fun setTopPadding(top: Int) {
        mPaddingTop = top
        requestLayout()
    }

    private fun init(attrs: AttributeSet?, defStyleAttr: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.AdjustPaddingTextView, defStyleAttr, 0)
        mAdjustTop = a.getBoolean(R.styleable.AdjustPaddingTextView_tv_adjustTop, mAdjustTop)
        mAdjustBottom = a.getBoolean(R.styleable.AdjustPaddingTextView_tv_adjustBottom, mAdjustBottom)
        mBaseText = a.getString(R.styleable.AdjustPaddingTextView_tv_baseText) ?: mBaseText
        a.recycle()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (mAdjustTop || mAdjustBottom) {
            val metric = paint.fontMetrics
            val topPadding = if (mAdjustTop) Math.max(0, mPaddingTop - (Math.ceil((-metric.top).toDouble()).toInt() - getBaseHeight())) else mPaddingTop
            val bottomPadding = if (mAdjustBottom) Math.max(0, mPaddingBottom - Math.ceil(metric.bottom.toDouble()).toInt()) else mPaddingBottom
            setPadding(paddingLeft, topPadding, paddingRight, bottomPadding)
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    private fun getBaseHeight(): Int {
        paint.getTextBounds(mBaseText, 0, mBaseText.length, mRect)
        return mRect.height()
    }
}