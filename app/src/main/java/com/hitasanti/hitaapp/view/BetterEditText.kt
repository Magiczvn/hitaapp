package com.hitasanti.hitaapp.view

import android.content.Context
import androidx.appcompat.widget.AppCompatEditText
import android.util.AttributeSet
import android.view.KeyEvent
import android.widget.TextView
import android.graphics.drawable.Drawable

class BetterEditText : AppCompatEditText {

    interface OnBackListener {
        fun onBack()
    }

    private var _OnBackListener: OnBackListener? = null

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(attrs: AttributeSet?, defStyleAttr: Int) {
    }

    override fun onKeyPreIme(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
            clearFocus()
            _OnBackListener?.onBack()
            return false
        }

        return super.onKeyPreIme(keyCode, event)
    }
}