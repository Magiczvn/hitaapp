package com.hitasanti.hitaapp.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.hitasanti.hitaapp.R
import kotterknife.bindView

class OrderItemView : FrameLayout {
    private val _QuantityView: TextView by bindView(R.id.order_item_quantity)
    private val _NameView: TextView by bindView(R.id.order_item_name)
    private val _PriceView: TextView by bindView(R.id.order_item_price)
    private val _LineView: View by bindView(R.id.order_item_separator)
    private var productId:Int = 0
    private var variationId:Int? = 0
    private var _OrderListener: OrderListener? = null

    interface OrderListener {
        fun onOrderClick(productId: Int,  variationId: Int?)
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        clipToPadding = false
        this.setOnClickListener {
            _OrderListener?.onOrderClick(productId, variationId)
        }
    }

    fun setName(name: String){
        _NameView.text = name
    }

    fun setPrice(price: Int){
        _PriceView.text = NumberUtil.formatPrice(context, price)
    }

    fun setLastItem(isLastItem: Boolean){
        _LineView.visibility = if(isLastItem) View.GONE else View.VISIBLE
    }

    fun setQuantity(quantity: Int){
        _QuantityView.text = quantity.toString()
    }

    fun setProductId(id: Int){
        productId = id
    }

    fun setVariationId(id: Int?){
        variationId = id
    }

    fun setOrderListenter(orderListener: OrderListener){
        _OrderListener = orderListener
    }
}