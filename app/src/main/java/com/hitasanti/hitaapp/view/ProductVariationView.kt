package com.hitasanti.hitaapp.view


import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Checkable
import android.widget.FrameLayout
import android.R.attr
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.radiobutton.MaterialRadioButton
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.repository.model.ProductVariation
import kotterknife.bindView


class ProductVariationView:FrameLayout, Checkable {

    private val _OptionView: MaterialRadioButton by bindView(R.id.item_option)
    private val _PriceView: TextView by bindView(R.id.item_price)
    private val _RegularPriceView: TextView by bindView(R.id.item_regular_price)


    var productVariation: ProductVariation? = null

    private val CHECKED_STATE_SET = intArrayOf(
        attr.state_checked
    )

    override fun getId(): Int {
        return productVariation?.id?:super.getId()
    }


    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(attrs: AttributeSet?, defStyleAttr: Int) {

    }

    val disableColor = ColorStateList.valueOf(
        ContextCompat.getColor(context,R.color.text_gray)
    )
    val enableColor =  ColorStateList.valueOf(
        ContextCompat.getColor(context,R.color.buy_button_color)
    )

    override fun setClickable(clickable: Boolean) {
        if(clickable){
            _OptionView.buttonTintList = enableColor
        }
        else{
            _OptionView.buttonTintList = disableColor
        }
        super.setClickable(clickable)
    }

    fun onClick(){
        _OptionView.performClick()
    }

    fun setVariation(variation: ProductVariation){
        _OptionView.text = variation.name
        _PriceView.text = NumberUtil.formatPrice(context, variation.price)

        val regularPrice = variation.regularPrice?:variation.price


        if(variation.price != variation.regularPrice){
            val string = SpannableString(NumberUtil.formatPrice(context, regularPrice))
            string.setSpan(StrikethroughSpan(), 0, string.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            _RegularPriceView.text = string
            _RegularPriceView.visibility =  View.VISIBLE
            _PriceView.setTextColor(Color.parseColor("#a40429"))
        }
        else{
            _RegularPriceView.visibility = View.GONE
            _PriceView.setTextColor(Color.BLACK)
        }


        productVariation = variation
    }


    override fun onCreateDrawableState(extraSpace: Int): IntArray? {
        val drawableState = super.onCreateDrawableState(extraSpace + 1)
        if (_OptionView.isChecked) {
            View.mergeDrawableStates(drawableState, CHECKED_STATE_SET)
        }
        return drawableState
    }

    override fun toggle() {
        _OptionView.isChecked = !_OptionView.isChecked
    }

    override fun isChecked(): Boolean {
        return _OptionView.isChecked
    }

    override fun setChecked(checked: Boolean) {
        if (_OptionView.isChecked != checked) {
            _OptionView.isChecked = checked
            refreshDrawableState()
        }
    }
}