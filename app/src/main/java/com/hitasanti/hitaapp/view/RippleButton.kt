package com.hitasanti.hitaapp.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.Checkable
import android.widget.FrameLayout


class RippleButton : FrameLayout, Checkable {
    private var rippleAnimator: RippleAnimator? = null
    private var mChecked = false

    constructor(context: Context?) : super(context!!) {
        setWillNotDraw(false)
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
        setWillNotDraw(false)
        init()
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context!!, attrs, defStyleAttr) {
        setWillNotDraw(false)
        init()
    }

    private fun init() {
        rippleAnimator = RippleAnimator(this)
        rippleAnimator?.setDuration(450)
        rippleAnimator?.setColor(Color.rgb(177, 177, 177))
        rippleAnimator?.setAlpha(220)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        rippleAnimator?.rippleWith(event)
        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas) {
        rippleAnimator?.rippleOn(canvas)
        super.onDraw(canvas)
    }

    override fun onViewAdded(child: View) {
        super.onViewAdded(child)
        if (child is Checkable) (child as Checkable).isChecked = mChecked
    }

    override fun setChecked(b: Boolean) {
        if (mChecked != b) {
            mChecked = b
            var i = 0
            val count = childCount
            while (i < count) {
                val child = getChildAt(i)
                if (child is Checkable) (child as Checkable).isChecked = mChecked
                i++
            }
            refreshDrawableState()
        }
    }

    override fun isChecked(): Boolean {
        return mChecked
    }

    override fun toggle() {
        isChecked = !mChecked
    }

    public override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val drawableState = super.onCreateDrawableState(extraSpace + 1)
        val additionalStates =
            if (mChecked) STATE_CHECKED else null
        if (additionalStates != null) View.mergeDrawableStates(
            drawableState,
            additionalStates
        )
        return drawableState
    }

    companion object {
        private val STATE_CHECKED = intArrayOf(
            android.R.attr.state_checked
        )
    }
}
