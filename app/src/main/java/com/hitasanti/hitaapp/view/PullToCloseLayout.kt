package com.hitasanti.hitaapp.view

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import androidx.core.view.NestedScrollingParent
import com.github.florent37.viewanimator.AnimationListener.Update
import com.github.florent37.viewanimator.ViewAnimator


class PullToCloseLayout : FrameLayout, NestedScrollingParent {
    private var mTouchSlop = 0
    private var mLastMotionX = 0f
    private var mLastMotionY = 0f
    private var mInitialMotionY = 0f
    private var mIsBeingDragged = false
    private var mState = STATE_RESET
    private var mClosableView: View? = null
    private var mFooterLayout: View? = null
    private var mPullToCloseEnabled = true
    private var mEnableNestScrollingParent = false
    private var mIsGetXYAtOnMove = false // get XY after enter to pull to close
    private var mIsActionDown = false // set mIsGetXYAtOnMove once
    private var mPullScale = 0f
    private var mOffsetY = 0

    interface OnStateChangedListener {
        fun onStateChanged(state: Int, pullScale: Float)
    }

    private var mOnStateChangedListener: OnStateChangedListener? = null

    internal interface ClosableView {
        val isReadyForPull: Boolean
    }

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        clipToPadding = false
        val config = ViewConfiguration.get(context)
        mTouchSlop = config.scaledTouchSlop
    }

    override fun addView(
        child: View,
        index: Int,
        params: ViewGroup.LayoutParams
    ) {
        super.addView(child, index, params)
        if (child is ClosableView) {
            mClosableView = child
        } else {
            mFooterLayout = child
        }
    }

    fun setOnStateChangedListener(listener: OnStateChangedListener?) {
        mOnStateChangedListener = listener
    }

    fun setEnableNestScrollingParent(enableNestScrollingParent: Boolean) {
        mEnableNestScrollingParent = enableNestScrollingParent
    }

    var isPullToCloseEnabled: Boolean
        get() = mPullToCloseEnabled
        set(enabled) {
            mPullToCloseEnabled = enabled
            if (enabled) {
                mIsGetXYAtOnMove = false
            }
        }

    val isClosing: Boolean
        get() = mState == STATE_CLOSING

    private val isReadyForPull: Boolean
        private get() = (mClosableView as ClosableView?)!!.isReadyForPull

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val ws = MeasureSpec.makeMeasureSpec(widthSize, MeasureSpec.EXACTLY)
        var hs = MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY)
        mClosableView!!.measure(ws, hs)
        val params = mFooterLayout!!.layoutParams
        hs = MeasureSpec.makeMeasureSpec(params.height, MeasureSpec.EXACTLY)
        mFooterLayout!!.measure(ws, hs)
        setMeasuredDimension(widthSize, heightSize)
    }

    override fun onLayout(
        changed: Boolean,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int
    ) {
        val childLeft = 0
        var childTop = mOffsetY
        mClosableView!!.layout(
            childLeft,
            childTop,
            childLeft + mClosableView!!.measuredWidth,
            childTop + mClosableView!!.measuredHeight
        )
        childTop += mClosableView!!.measuredHeight
        if (childTop < height - mFooterLayout!!.measuredHeight) childTop =
            childTop + (height - mFooterLayout!!.height - childTop) / 2
        mFooterLayout!!.layout(
            childLeft,
            childTop,
            childLeft + mFooterLayout!!.measuredWidth,
            childTop + mFooterLayout!!.measuredHeight
        )
    }

    private fun offsetView(offset: Int) {
        if (mOffsetY != offset) {
            val needRelayout = mOffsetY == 0
            mOffsetY = offset
            val value = mOffsetY - mClosableView!!.top
            mClosableView!!.offsetTopAndBottom(value)
            var loadingTop = mOffsetY + mClosableView!!.height
            if (loadingTop < height - mFooterLayout!!.height) loadingTop =
                loadingTop + (height - mFooterLayout!!.height - loadingTop) / 2
            mFooterLayout!!.offsetTopAndBottom(loadingTop - mFooterLayout!!.top)
            if (needRelayout) requestLayout()
        }
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (!isPullToCloseEnabled) return false
        val action = event.action
        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            mIsBeingDragged = false
            return false
        }
        if (action != MotionEvent.ACTION_DOWN && mIsBeingDragged) return true
        when (action) {
            MotionEvent.ACTION_MOVE -> {
                if (isReadyForPull) {
                    if (!mIsGetXYAtOnMove) {
                        mInitialMotionY = event.y
                        mLastMotionY = mInitialMotionY
                        mLastMotionX = event.x
                        mIsGetXYAtOnMove = true
                    }
                    val y = event.y
                    val x = event.x
                    val diff: Float
                    val oppositeDiff: Float
                    val absDiff: Float
                    diff = y - mLastMotionY
                    oppositeDiff = x - mLastMotionX
                    absDiff = Math.abs(diff)
                    if (absDiff > mTouchSlop && absDiff > Math.abs(oppositeDiff) && diff <= -1f) {
                        mLastMotionY = y
                        mLastMotionX = x
                        mIsBeingDragged = true
                    }
                }
            }
            MotionEvent.ACTION_DOWN -> {
                if (isReadyForPull) {
                    mIsActionDown = true
                    mInitialMotionY = event.y
                    mLastMotionY = mInitialMotionY
                    mLastMotionX = event.x
                    mIsBeingDragged = false
                }
            }
        }
        return mIsBeingDragged
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isPullToCloseEnabled) return false
        if (event.action == MotionEvent.ACTION_DOWN && event.edgeFlags != 0) return false
        when (event.action) {
            MotionEvent.ACTION_MOVE -> {
                if (mIsBeingDragged) {
                    mLastMotionY = event.y
                    mLastMotionX = event.x
                    pullEvent()
                    return true
                }
            }
            MotionEvent.ACTION_DOWN -> {
                if (isReadyForPull) {
                    mInitialMotionY = event.y
                    mLastMotionY = mInitialMotionY
                    mLastMotionX = event.x
                    return true
                }
            }
            MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                if (mIsBeingDragged) {
                    mIsBeingDragged = false
                    if (mState == STATE_RELEASE_TO_CLOSE) {
                        setState(STATE_CLOSING)
                        return true
                    }

                    // If we're already closing,
                    if (isClosing) return true

                    // If we haven't returned by here, then we're not in a state
                    // to pull, so just reset
                    setState(STATE_RESET)
                    return true
                }
            }
        }
        return false
    }

    private fun pullEvent() {
        val scrollDistance: Int
        val itemDimension: Int
        scrollDistance = Math.abs(
            Math.round(
                Math.max(
                    mInitialMotionY - mLastMotionY,
                    0f
                ) / FRICTION
            )
        )
        itemDimension = mFooterLayout!!.height
        if (scrollDistance != 0 && !isClosing) {
            val scale = scrollDistance / itemDimension.toFloat()
            setPullScale(scale)
            if (mState != STATE_PULL_TO_CLOSE && itemDimension >= scrollDistance) setState(
                STATE_PULL_TO_CLOSE
            ) else if (mState == STATE_PULL_TO_CLOSE && itemDimension < scrollDistance) setState(
                STATE_RELEASE_TO_CLOSE
            )
        }
    }

    private fun setPullScale(scale: Float) {
        if (mPullScale != scale) {
            mPullScale = scale
            offsetView(-Math.round(mFooterLayout!!.height * mPullScale))
            if (mOnStateChangedListener != null) {
                mOnStateChangedListener!!.onStateChanged(mState, mPullScale)
            }
        }
    }

    private fun setState(state: Int) {
        if (mState != state) {
            mState = state
            when (mState) {
                STATE_RESET -> onReset()
                STATE_PULL_TO_CLOSE -> {
                }
                STATE_RELEASE_TO_CLOSE -> {
                }
                STATE_CLOSING -> onClosing()
            }
            if (mOnStateChangedListener != null) {
                mOnStateChangedListener!!.onStateChanged(mState, mPullScale)
            }
        }
    }

    //    protected void onPullToClose() {
    //    }
    //    protected void onReleaseToClose() {
    //    }
    protected fun onClosing() {
        mIsBeingDragged = false
        runClosingAnimation()
    }

    protected fun onReset() {
        mIsBeingDragged = false
        runClosingAnimation()
    }

    private fun runClosingAnimation() {
        if (mOffsetY == 0) return
        clearAnimation()
        val scale =
            Math.abs(mOffsetY).toFloat() / mFooterLayout!!.height
        ViewAnimator.animate(this)
            .custom(
                Update<View> { view: View?, value: Float -> setPullScale(value) },
                scale,
                0f
            )
            .duration(SMOOTH_SCROLL_DURATION_MS.toLong())
            .interpolator(DecelerateInterpolator())
            .onStop { setState(STATE_RESET) }
            .start()
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        if (state is Bundle) {
            val bundle = state

            // Let super Restore Itself
            super.onRestoreInstanceState(bundle.getParcelable(STATE_SUPER))
            val viewState = bundle.getInt(STATE_STATE, 0)
            setState(viewState)
            return
        }
        super.onRestoreInstanceState(state)
    }

    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putInt(STATE_STATE, mState)
        bundle.putParcelable(STATE_SUPER, super.onSaveInstanceState())
        return bundle
    }

    override fun onStartNestedScroll(
        child: View,
        target: View,
        nestedScrollAxes: Int
    ): Boolean {
        return mPullToCloseEnabled && mEnableNestScrollingParent
    }

    override fun onNestedScroll(
        target: View,
        dxConsumed: Int,
        dyConsumed: Int,
        dxUnconsumed: Int,
        dyUnconsumed: Int
    ) {
        super.onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed)
        if (dyUnconsumed > 0 && isReadyForPull) {
            requestDisallowInterceptTouchEvent(false)
            mPullToCloseEnabled = true
            if (mIsActionDown) {
                mIsGetXYAtOnMove = false
                mIsActionDown = false
            }
        }
    }

    companion object {
        const val FRICTION = 1.5f
        const val SMOOTH_SCROLL_DURATION_MS = 200
        const val STATE_STATE = "ptr_state"
        const val STATE_SUPER = "ptr_super"
        const val STATE_RESET = 0
        const val STATE_PULL_TO_CLOSE = 1
        const val STATE_RELEASE_TO_CLOSE = 2
        const val STATE_CLOSING = 3
    }
}
