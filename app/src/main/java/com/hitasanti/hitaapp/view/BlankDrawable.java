package com.hitasanti.hitaapp.view;


import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BlankDrawable extends Drawable {

    private ColorState _State = new ColorState();

    @Override
    public void draw(@NonNull Canvas canvas) {}

    @Override
    public void setAlpha(int alpha) {}

    @Override
    public void setColorFilter(ColorFilter colorFilter) {}

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }

    @Nullable
    @Override
    public ConstantState getConstantState() {
        return _State;
    }

    private class ColorState extends ConstantState {

        @NonNull
        @Override
        public Drawable newDrawable() {
            return new BlankDrawable();
        }

        @Override
        public int getChangingConfigurations() {
            return 0;
        }
    }
}
