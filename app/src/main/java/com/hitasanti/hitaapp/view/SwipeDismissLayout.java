package com.hitasanti.hitaapp.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class SwipeDismissLayout extends FrameLayout {

    private View mChildView;
    private View mBackgroundView;

    // Cached ViewConfiguration and system-wide constant values
    private int mSlop;
    private int mMinFlingVelocity;
    private int mMaxFlingVelocity;
    private long mAnimationTime;

    // Fixed properties
    private int mViewWidth = 1; // 1 and not 0 to prevent dividing by zero

    // Transient properties
    private float mDownX;
    private float mDownY;
    private boolean mSwiping;
    private int mSwipingSlop;
    private VelocityTracker mVelocityTracker;

    private boolean mAnimationRunning = false;
    public boolean mIsEnableSwipe = true;
    private boolean mIsConvertTranslucent = false;

    public interface SwipeCallback {

        void onSwiped();
    }

    public interface ConvertTranslucent{
        void convertToTranslucent(Boolean isTranslucent);
    }

    public interface TouchEvent{
        void onTouch();
    }

    private SwipeCallback mSwipeCallback;

    private ConvertTranslucent mConvertTranslucent;

    private TouchEvent mTouchEvent;

    public SwipeDismissLayout(Context context) {
        super(context);
        init(context);
    }

    public SwipeDismissLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SwipeDismissLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        ViewConfiguration vc = ViewConfiguration.get(context);
        mSlop = vc.getScaledTouchSlop();
        mMinFlingVelocity = vc.getScaledMinimumFlingVelocity() * 16;
        mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        mAnimationTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);

        mBackgroundView = new View(context);
        mBackgroundView.setBackgroundColor(0x80000000);
        mBackgroundView.setVisibility(GONE);
        addView(mBackgroundView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void setSwipeCallBack(SwipeCallback callBack) {
        mSwipeCallback = callBack;
    }

    public void setEnableSwipe(boolean enable) {
        mIsEnableSwipe = enable;
    }

    public void setConvertTranslucent(ConvertTranslucent callBack){
        mConvertTranslucent = callBack;
    }

    public void setOnTouchCallBack(TouchEvent callBack){
        mTouchEvent = callBack;
    }

    public void setEnableConvertTranslucent(boolean enable){
        mIsConvertTranslucent = enable;
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);
        if (child != mBackgroundView)
            mChildView = child;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        mViewWidth = mChildView.getWidth();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN){
            mTouchEvent.onTouch();
        }
        if (!mIsEnableSwipe)
            return false;
        if (mAnimationRunning)
            return false;

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getRawX();
                mDownY = event.getRawY();
                mVelocityTracker = VelocityTracker.obtain();
                mVelocityTracker.addMovement(event);
                return false;

            case MotionEvent.ACTION_MOVE:
                if (mVelocityTracker == null)
                    break;

                mVelocityTracker.addMovement(event);
                float deltaX = event.getRawX() - mDownX;
                float deltaY = event.getRawY() - mDownY;
                if (deltaX < 0) {
                    return false;
                }
                if (Math.abs(deltaX) > mSlop && Math.abs(deltaY) < Math.abs(deltaX) / 2) {
                    mSwiping = true;
                    mSwipingSlop = (deltaX > 0 ? mSlop : -mSlop);
                    mChildView.getParent().requestDisallowInterceptTouchEvent(true);

                    // Cancel listview's touch
                    MotionEvent cancelEvent = MotionEvent.obtain(event);
                    cancelEvent.setAction(MotionEvent.ACTION_CANCEL | (event.getActionIndex() << MotionEvent.ACTION_POINTER_INDEX_SHIFT));
                    mChildView.onTouchEvent(cancelEvent);
                    cancelEvent.recycle();
                }

                if (mSwiping) {
                    updateView(deltaX);
                    return true;
                }
                break;
        }

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getRawX();
                mDownY = event.getRawY();
                mVelocityTracker = VelocityTracker.obtain();
                mVelocityTracker.addMovement(event);
                return false;

            case MotionEvent.ACTION_UP: {
                if (mVelocityTracker == null)
                    break;

                float deltaX = event.getRawX() - mDownX;
                mVelocityTracker.addMovement(event);
                mVelocityTracker.computeCurrentVelocity(1000);
                float velocityX = mVelocityTracker.getXVelocity();
                float absVelocityX = Math.abs(velocityX);
                float absVelocityY = Math.abs(mVelocityTracker.getYVelocity());
                boolean dismiss = false;
                boolean dismissRight = false;
                if (Math.abs(deltaX) > mViewWidth / 2 && mSwiping) {
                    dismiss = true;
                    dismissRight = deltaX > 0;
                } else if (mMinFlingVelocity <= absVelocityX && absVelocityX <= mMaxFlingVelocity
                        && absVelocityY < absVelocityX
                        && absVelocityY < absVelocityX && mSwiping) {
                    // dismiss only if flinging in the same direction as dragging
                    dismiss = (velocityX < 0) == (deltaX < 0);
                    dismissRight = mVelocityTracker.getXVelocity() > 0;
                }

                if (dismiss)
                    runDismiss(dismissRight);
                else if (mSwiping)
                    cancelSwipe();
                mVelocityTracker.recycle();
                mVelocityTracker = null;
                mDownX = 0;
                mDownY = 0;
                mSwiping = false;
                break;
            }

            case MotionEvent.ACTION_CANCEL:
                if (mVelocityTracker == null)
                    break;

                cancelSwipe();
                mVelocityTracker.recycle();
                mVelocityTracker = null;
                mDownX = 0;
                mDownY = 0;
                mSwiping = false;
                break;

            case MotionEvent.ACTION_MOVE: {
                if (mVelocityTracker == null)
                    break;

                mVelocityTracker.addMovement(event);
                float deltaX = event.getRawX() - mDownX;
                float deltaY = event.getRawY() - mDownY;
                if (Math.abs(deltaX) > mSlop && Math.abs(deltaY) < Math.abs(deltaX) / 2) {
                    mSwiping = true;
                    mSwipingSlop = (deltaX > 0 ? mSlop : -mSlop);
                    mChildView.getParent().requestDisallowInterceptTouchEvent(true);

                    // Cancel listview's touch
                    MotionEvent cancelEvent = MotionEvent.obtain(event);
                    cancelEvent.setAction(MotionEvent.ACTION_CANCEL |
                            (event.getActionIndex() << MotionEvent.ACTION_POINTER_INDEX_SHIFT));
                    mChildView.onTouchEvent(cancelEvent);
                    cancelEvent.recycle();
                }

                if (mSwiping) {
                    updateView(deltaX);
                    return true;
                }
                break;
            }
        }
        return false;
    }

    private void runDismiss(boolean dismissRight) {
        if (!dismissRight)
            return;
        mChildView.animate()
                .translationX(dismissRight ? mViewWidth : -mViewWidth)
                .alpha(0)
                .setDuration(mAnimationTime)
                .setUpdateListener((animation) -> updateBackgroundView(mChildView.getTranslationX()))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        mAnimationRunning = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mAnimationRunning = false;
                        performDismiss();
                    }
                });
    }

    private void cancelSwipe() {
        mChildView.animate()
                .translationX(0)
                .alpha(1)
                .setDuration(mAnimationTime)
                .setUpdateListener((animation) -> updateBackgroundView(mChildView.getTranslationX()))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        mAnimationRunning = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mAnimationRunning = false;
                        if (mIsConvertTranslucent){
                            mConvertTranslucent.convertToTranslucent(false);
                            isFirstSwipe = true;
                        }
                    }
                });
    }

    private Boolean isFirstSwipe = true;
    private void updateView(float deltaX) {
        if (mIsConvertTranslucent){
            mConvertTranslucent.convertToTranslucent(true);
            if (isFirstSwipe){
                postDelayed(() -> {
                    mChildView.setTranslationX(Math.max(0, deltaX - mSwipingSlop));
                    updateBackgroundView(mChildView.getTranslationX());
                    isFirstSwipe = false;
                }, 50);
            }else {
                mChildView.setTranslationX(Math.max(0, deltaX - mSwipingSlop));
                updateBackgroundView(mChildView.getTranslationX());
            }
        }else {
            mChildView.setTranslationX(Math.max(0, deltaX - mSwipingSlop));
            updateBackgroundView(mChildView.getTranslationX());
        }
    }

    private void updateBackgroundView(float translationX) {
        mBackgroundView.setVisibility(translationX == 0 ? GONE : VISIBLE);
        if (translationX > 0)
            mBackgroundView.setTranslationX(translationX - mViewWidth);
        else
            mBackgroundView.setTranslationX(translationX + mViewWidth);
        mBackgroundView.setAlpha(Math.max(0f, Math.min(1f, 1f - Math.abs(translationX) / mViewWidth)));
    }

    private void performDismiss() {
        mBackgroundView.setVisibility(GONE);
        mChildView.setVisibility(GONE);
        if (mSwipeCallback != null)
            mSwipeCallback.onSwiped();
    }
}
