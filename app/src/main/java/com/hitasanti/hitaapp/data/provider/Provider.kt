package com.hitasanti.hitaapp.data.provider

interface Provider<T> {

    fun get(): T
}