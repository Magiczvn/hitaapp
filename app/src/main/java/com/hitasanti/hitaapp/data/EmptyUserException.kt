package com.hitasanti.hitaapp.data

class EmptyUserException(message: String): RuntimeException(message)