package com.hitasanti.hitaapp.data.provider.impl

import android.content.Context
import android.graphics.Point
import android.view.WindowManager
import com.hitasanti.hitaapp.data.provider.Provider

class ScreenSizeProviderImpl(private val _Context: Context)
    : Provider<IntArray> {

    override fun get(): IntArray {
        val wm = _Context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y
        return intArrayOf(width, height)
    }
}