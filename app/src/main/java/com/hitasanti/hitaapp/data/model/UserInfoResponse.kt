package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class UserInfoResponse {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("first_name")
    var first_name: String? = null

    @SerializedName("last_name")
    var last_name: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("avatar_url")
    var avatar_url: String? = null

    @SerializedName("billing")
    var billing: UserAddress? = null

    @SerializedName("shipping")
    var shipping: UserAddress? = null


    class UserAddress{
        @SerializedName("first_name")
        var first_name: String? = null

        @SerializedName("last_name")
        var last_name: String? = null

        @SerializedName("address_1")
        var address_1: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phone")
        var phone: String? = null
    }

}