package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class CategoryModelResponse {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null
}