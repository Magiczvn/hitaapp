package com.hitasanti.hitaapp.data.model

import com.hitasanti.hitaapp.repository.model.Coupon

class OrderRequest(val customer_id: Int, val customer_note: String, val billing: OrderRequest.Billing, val shipping: Shipping, val line_items:List<OrderRequest.Product>, val coupon_lines:List<CouponLine>, val shipping_lines: List<OrderRequest.ShippingLine>, val discount_total: Int) {
    val payment_method: String = "cod"
    val payment_method_title: String = "Trả tiền mặt khi nhận hàng"

    class Billing(val first_name: String, val address_1: String, val email: String, val phone: String) {
        var last_name: String = ""
        var address_2: String = ""
        var city: String = "HCM"
        var state: String = ""
        var postcode: String = ""
        var country: String = "VN"
    }

    class Shipping(val first_name: String, val address_1: String) {
        var last_name: String = ""
        var address_2: String = ""
        var city: String = "HCM"
        var state: String = ""
        var postcode: String = ""
        var country: String = "VN"
    }

    class ShippingLine(val total: String) {
        val method_id = "flat_rate"
        val method_title = "Flat Rate"
    }

    class CouponLine(val code: String, val amount: String)

    class Product(val product_id: Int, val quantity: Int)
}