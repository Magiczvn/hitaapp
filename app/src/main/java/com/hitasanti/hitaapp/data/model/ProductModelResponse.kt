package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class ProductModelResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("short_description")
    var short_description: String? = null

    @SerializedName("price")
    var price: String? = null

    @SerializedName("regular_price")
    var regular_price: String? = null

    @SerializedName("on_sale")
    var on_sale: Boolean? = null

    @SerializedName("purchasable")
    var purchasable: Boolean? = null

    @SerializedName("variations_value")
    var variations: List<VariationResponse>? = null

    @SerializedName("shipping_required")
    var shipping_required: Boolean? = null

    @SerializedName("categories")
    var categories: List<CategoryModelResponse>? = null

    @SerializedName("images")
    var images: List<ImageModelResponse>? = null

    class VariationResponse {
        @SerializedName("id")
        var id: Int? = null

        @SerializedName("display_price")
        var price: String? = null

        @SerializedName("regular_price")
        var regular_price: String? = null

        @SerializedName("option")
        var option: String? = null
    }





}