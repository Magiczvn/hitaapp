package com.hitasanti.hitaapp.data

import com.hitasanti.hitaapp.feature.login.User
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor: Interceptor {

    companion object {

    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val headers = chain.request().headers()
        val requestBuilder = chain.request()
            .newBuilder()
        /*if(headers.get("Authorization").isNullOrEmpty() && User.isLoggedIn() && User.mService == User.HITA){
            requestBuilder.addHeader("Authorization", "Bearer ${User.mToken}")
        }*/

        return chain.proceed(requestBuilder.build())
    }
}