package com.hitasanti.hitaapp.data.product

import com.hitasanti.hitaapp.data.model.*
import com.hitasanti.hitaapp.feature.categories.item.CategoryDetails
import com.hitasanti.hitaapp.repository.model.Product
import com.hitasanti.hitaapp.repository.model.ProductVariation
import retrofit2.Call
import retrofit2.http.*

interface ProductApi {
    @GET("wp-json/wc/v3/products?per_page=100&${Product.queryString}")
    fun getProducts(@Query("category") category: Int): Call<List<ProductModelResponse>>

    @GET("wp-json/wc/v3/products/{productId}/variations?${ProductVariation.queryString}")
    fun getProductVariation(@Path("productId") productId: Int): Call<List<VariationResponse>>

    @GET("wp-json/wc/v3/products?${Product.queryString}")
    fun getTopProducts(@Query("category") category: Int, @Query("per_page") per_page: Int): Call<List<ProductModelResponse>>

    @GET("wp-json/wc/v3/products?per_page=100&${Product.queryString}")
    fun getAllProducts(@Query("category") category: String = CategoryDetails.allProductQueryString): Call<List<ProductModelResponse>>

    @GET("wp-json/wc/v3/products/categories?_fields=id,name")
    fun getCategories(@Query("parent") parentCategory: Int): Call<List<CategoryModelResponse>>

    @POST("wp-json/jwt-auth/v1/token")
    fun getToken(@Query("username") username: String, @Query("password") password: String): Call<LoginResponse>

    @POST("wp-json/nextend-social-login/v1/facebook/get_user")
    fun facebookLogin(@Query("access_token") access_token: String): Call<String>

    @POST("wp-json/nextend-social-login/v1/google/get_user")
    fun googleLogin(@Query("access_token") access_token: String): Call<String>

    @Headers("Content-Type: application/json")
    @POST("wp-json/wc/v3/orders")
    fun placeOrder(@Body body: OrderRequest): Call<OrderResponse>

    @Headers("Content-Type: application/json")
    @POST("wp-json/wp/v2/users/register")
    fun register(@Body body: RegisterRequest): Call<RegisterResponse>

    @GET("wp-json/wc/v3/orders?per_page=100&_fields=id,status,total,shipping_total, discount_total,date_created,date_created_gmt,customer_id,customer_note,billing,shipping,payment_method,payment_method_title,line_items")
    fun getOrdersHistory(@Query("customer") customerId: Int): Call<List<OrderResponse>>

    @GET("wp-json/wc/v3/coupons")
    fun getCoupon(@Query("code") code: String): Call<List<CouponResponse>>

    @POST("https://www.googleapis.com/oauth2/v4/token")
    @FormUrlEncoded
    fun getGoogleAuthCode( @Field("code") authCode: String, @Field("client_id") client_id: String, @Field("client_secret") client_secret: String, @Field("grant_type") grant_type: String = "authorization_code",  @Field("redirect_uri") redirect_uri: String = ""): Call<GoogleAuthResponse>

    @GET("wp-json/wc/v3/customers/{userId}")
    fun getUserInfo(@Path("userId") userId: String): Call<UserInfoResponse>

    @Headers("Content-Type: application/json")
    @PUT("wp-json/wc/v3/customers/{userId}")
    fun updateUserInfo(@Path("userId") userId: String, @Body body: EditInfoRequest): Call<UserInfoResponse>

    @Headers("Content-Type: application/json")
    @PUT("wp-json/wc/v3/orders/{orderId}")
    fun cancelOrder(@Path("orderId") orderId: String, @Body body: CancelOrderRequest): Call<OrderResponse>

}