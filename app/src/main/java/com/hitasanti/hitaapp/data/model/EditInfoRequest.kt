package com.hitasanti.hitaapp.data.model

class EditInfoRequest(val first_name: String, val last_name: String, val billing: Address) {

    class Address(val first_name: String, val last_name: String, val address_1: String, val phone: String) {
        var city: String = "HCM"
        var postcode: String = "700000"
        var country: String = "VN"
    }
}