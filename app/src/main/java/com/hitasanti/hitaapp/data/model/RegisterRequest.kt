package com.hitasanti.hitaapp.data.model

class RegisterRequest(val username: String, val email: String, val password: String)