package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class NewsModelResponse {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: Content? = null

    @SerializedName("link")
    var link: String? = null

    @SerializedName("excerpt")
    var excerpt: Content? = null

    @SerializedName("_embedded")
    var embedded: Embedded? = null

    class Embedded{
        @SerializedName("wp:featuredmedia")
        var featuredMedia: List<Media>? = null
    }

    class Content {
        @SerializedName("rendered")
        var rendered: String? = null
    }

    class Media {
        @SerializedName("source_url")
        var url: String? = null
    }
}