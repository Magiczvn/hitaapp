package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("user_id")
    var id: Int? = null

    @SerializedName("user_display_name")
    var user_display_name: String? = null

    @SerializedName("user_email")
    var user_email: String? = null

    @SerializedName("token")
    var token: String? = null

}