package com.hitasanti.hitaapp.data

class ApiException(message: String): RuntimeException(message)