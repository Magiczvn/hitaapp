package com.hitasanti.hitaapp.data.model

import com.hitasanti.hitaapp.repository.model.Coupon

class CancelOrderRequest(val customer_note: String) {
    val status: String = "cancelled"
}