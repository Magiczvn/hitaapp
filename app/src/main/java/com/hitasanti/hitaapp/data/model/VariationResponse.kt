package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class VariationResponse {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("price")
    var price: String? = null

    @SerializedName("regular_price")
    var regular_price: String? = null

    @SerializedName("attributes")
    var attributes: List<Attribute>? = null

    @SerializedName("menu_order")
    var menu_order: Int? = null


    class Attribute{
        @SerializedName("option")
        var name: String? = null
    }

}