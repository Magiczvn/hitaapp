package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class ImageModelResponse {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("src")
    var src: String? = null

    @SerializedName("name")
    var name: String? = null
}