package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

class CouponResponse {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("discount_type")
    var discount_type: String? = null

    @SerializedName("usage_count")
    var usage_count: Int? = null

    @SerializedName("date_expires")
    var date_expires: Date? = null

    @SerializedName("minimum_amount")
    var minimum_amount: String? = null

    @SerializedName("maximum_amount")
    var maximum_amount: String? = null

    @SerializedName("usage_limit_per_user")
    var usage_limit_per_user: Int? = null

    @SerializedName("usage_limit")
    var usage_limit: Int? = null

    @SerializedName("product_ids")
    var product_ids: List<Int>? = null

    @SerializedName("excluded_product_ids")
    var excluded_product_ids: List<Int>? = null

    @SerializedName("product_categories")
    var product_categories: List<Int>? = null

    @SerializedName("excluded_product_categories")
    var excluded_product_categories: List<Int>? = null

    @SerializedName("used_by")
    var used_by: List<String>? = null

    @SerializedName("exclude_sale_items")
    var exclude_sale_items: Boolean? = null

    @SerializedName("free_shipping")
    var free_shipping :Boolean? = null

















}