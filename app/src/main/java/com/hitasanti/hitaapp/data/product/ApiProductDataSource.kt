package com.hitasanti.hitaapp.data.product

import com.facebook.Profile
import com.hitasanti.hitaapp.data.ApiException
import com.hitasanti.hitaapp.data.EmptyUserException
import com.hitasanti.hitaapp.data.model.*
import com.hitasanti.hitaapp.data.news.NewsApi
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.repository.datasource.NewsDataSource
import com.hitasanti.hitaapp.repository.datasource.ProductDataSource
import com.hitasanti.hitaapp.repository.model.*

class ApiProductDataSource(private val _Api: ProductApi): ProductDataSource {
    override fun getProducts(category: Int): List<Product> {
        val response = _Api.getProducts(category).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return emptyList()
            return body.mapNotNull {
                val categories = it.categories?.map {categoryModel->
                    Category(categoryModel.id, categoryModel.name)
                }
                val images = it.images?.map { imageModel->
                    Image(imageModel.id, imageModel.src, imageModel.name)
                }
                val id = it.id?:return@mapNotNull null
                val name = it.name?:return@mapNotNull null
                val price = it.price?:return@mapNotNull null
                if(price.isEmpty()) return@mapNotNull null

                var index = 0
                val variations = it.variations?.mapNotNull { variation->
                    val variation_id = variation.id?:return@mapNotNull null
                    val variation_name = variation.option?:return@mapNotNull null
                    val variation_price = variation.price?:return@mapNotNull null
                    if(variation_price.isEmpty()) return@mapNotNull null
                    val variation_regular_price = if(variation.regular_price.isNullOrEmpty()) variation_price.toInt() else variation.regular_price?.toInt()
                    ProductVariation(variation_id, variation_name.replace("-", " "), variation_price.toInt(), variation_regular_price, id, index++)
                }

                var cheapest: ProductVariation? = null
                if(!variations.isNullOrEmpty()){
                    variations.forEach { vari->
                        if(cheapest == null)
                            cheapest = vari
                        else{
                            if(vari.price < cheapest!!.price)
                                cheapest = vari
                        }
                    }
                }

                val product_price = if(variations.isNullOrEmpty()) price.toInt() else cheapest!!.price
                val product_regular_price = if(variations.isNullOrEmpty()) ( if(it.regular_price.isNullOrEmpty()) price.toInt() else it.regular_price?.toInt()) else cheapest!!.regularPrice


                val desc = it.short_description.orEmpty()
                Product(id, name, product_price, desc, product_regular_price, variations, it.on_sale, it.purchasable, it.shipping_required, categories, images)
            }
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getGoogleAuthCode(authCode: String, clientId: String): GoogleAuthResponse {
        val response = _Api.getGoogleAuthCode(authCode, clientId, "QxwEtGP7ohI_tzETUQp0zkCN").execute()
        return if (response.isSuccessful) {
            val body = response.body()?: throw ApiException("")
            body
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun register(registerRequest: RegisterRequest): Int {
        val response = _Api.register(registerRequest).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: throw ApiException("")
            val id = body.id?: throw ApiException("")
            id
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun facebookLogin(accessToken: String, email: String): Int {
        val response = _Api.facebookLogin(accessToken).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: throw ApiException("")
            if(body.isEmpty()) throw EmptyUserException("")

            val id = body.toInt()
            val name = Profile.getCurrentProfile()?.name.orEmpty()
            val token = ""
            val avatar = Profile.getCurrentProfile()?.getProfilePictureUri(600,600).toString()
            User.login(token, email, name, id, User.FACEBOOK, avatar)
            id
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun googleLogin(accessToken: String): Int {
        val response = _Api.googleLogin(accessToken).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: throw ApiException("")
            if(body.isEmpty()) throw EmptyUserException("")

            val id = body.toInt()
            val name = User.mGoogleAccount?.displayName?: throw ApiException("")
            val token = ""
            val email = User.mGoogleAccount?.email?: throw ApiException("")
            val avatar = User.mGoogleAccount?.photoUrl.toString()
            User.login(token, email, name, id, User.GOOGLE, avatar)
            id
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getCoupon(code: String): Coupon {
        val response = _Api.getCoupon(code).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: throw ApiException("")
            val coupon = body.first()
            val id = coupon.id?: throw ApiException("")
            val code = coupon.code?: throw ApiException("")
            val discount_type = coupon.discount_type?: throw ApiException("")
            val discount = coupon.amount?:"0"
            Coupon(id = id,
            code = code,
            amount = discount.toFloat(),
            date_expires = coupon.date_expires,
            discount_type = discount_type,
            exclude_sale_items = coupon.exclude_sale_items?:false,
            excluded_product_categories = coupon.excluded_product_categories.orEmpty(),
            excluded_product_ids =  coupon.excluded_product_ids.orEmpty(),
            free_shipping = coupon.free_shipping?:false,
            minimum_amount = (coupon.minimum_amount?:"0").toFloat(),
                maximum_amount = (coupon.maximum_amount?:"0").toFloat(),
                product_categories = coupon.product_categories.orEmpty(),
                product_ids = coupon.product_ids.orEmpty(),
                usage_count = coupon.usage_count?:0,
                usage_limit = coupon.usage_limit,
                usage_limit_per_user = coupon.usage_limit_per_user,
                used_by = coupon.used_by.orEmpty())

        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getProductVariations(productId: Int): List<ProductVariation> {
        val response = _Api.getProductVariation(productId).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return emptyList()
            return body.mapNotNull {
                val id = it.id?:return@mapNotNull null
                val name = it.attributes?.firstOrNull()?.name?:return@mapNotNull null
                val price = it.price?:return@mapNotNull null
                val menu_order = it.menu_order?:return@mapNotNull null
                if(price.isEmpty()) return@mapNotNull null

                ProductVariation(id, name, price.toInt(), if(it.regular_price.isNullOrEmpty()) price.toInt() else it.regular_price?.toInt(), productId, menu_order)
            }
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getTopProducts(category: Int, per_page: Int): List<Product> {
        val response = _Api.getTopProducts(category, per_page).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return emptyList()
            return body.mapNotNull {
                val categories = it.categories?.map {categoryModel->
                    Category(categoryModel.id, categoryModel.name)
                }
                val images = it.images?.map { imageModel->
                    Image(imageModel.id, imageModel.src, imageModel.name)
                }
                val id = it.id?:return@mapNotNull null
                val name = it.name?:return@mapNotNull null
                val price = it.price?:return@mapNotNull null
                if(price.isEmpty()) return@mapNotNull null

                var index = 0

                val variations = it.variations?.mapNotNull { variation->
                    val variation_id = variation.id?:return@mapNotNull null
                    val variation_name = variation.option?:return@mapNotNull null
                    val variation_price = variation.price?:return@mapNotNull null
                    if(variation_price.isEmpty()) return@mapNotNull null
                    val variation_regular_price = if(variation.regular_price.isNullOrEmpty()) variation_price.toInt() else variation.regular_price?.toInt()
                    ProductVariation(variation_id, variation_name.replace("-", " "), variation_price.toInt(), variation_regular_price, id, index++)
                }
                var cheapest: ProductVariation? = null
                if(!variations.isNullOrEmpty()){
                    variations.forEach { vari->
                        if(cheapest == null)
                            cheapest = vari
                        else{
                            if(vari.price < cheapest!!.price)
                                cheapest = vari
                        }
                    }
                }

                val product_price = if(variations.isNullOrEmpty()) price.toInt() else cheapest!!.price
                val product_regular_price = if(variations.isNullOrEmpty()) ( if(it.regular_price.isNullOrEmpty()) price.toInt() else it.regular_price?.toInt()) else cheapest!!.regularPrice


                val desc = it.short_description.orEmpty()
                Product(id, name, product_price, desc, product_regular_price, variations, it.on_sale, it.purchasable, it.shipping_required, categories, images)
            }
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getAllProducts(): List<Product> {
        val response = _Api.getAllProducts().execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return emptyList()
            return body.mapNotNull {
                val categories = it.categories?.map {categoryModel->
                    Category(categoryModel.id, categoryModel.name)
                }
                val images = it.images?.map { imageModel->
                    Image(imageModel.id, imageModel.src, imageModel.name)
                }
                val id = it.id?:return@mapNotNull null
                val name = it.name?:return@mapNotNull null
                val price = it.price?:return@mapNotNull null
                if(price.isEmpty()) return@mapNotNull null
                var index = 0
                val variations = it.variations?.mapNotNull { variation->
                    val variation_id = variation.id?:return@mapNotNull null
                    val variation_name = variation.option?:return@mapNotNull null
                    val variation_price = variation.price?:return@mapNotNull null
                    if(variation_price.isEmpty()) return@mapNotNull null
                    val variation_regular_price = if(variation.regular_price.isNullOrEmpty()) variation_price.toInt() else variation.regular_price?.toInt()
                    ProductVariation(variation_id, variation_name.replace("-", " "), variation_price.toInt(), variation_regular_price, id, index++)
                }
                var cheapest: ProductVariation? = null
                if(!variations.isNullOrEmpty()){
                    variations.forEach { vari->
                        if(cheapest == null)
                            cheapest = vari
                        else{
                            if(vari.price < cheapest!!.price)
                                cheapest = vari
                        }
                    }
                }

                val product_price = if(variations.isNullOrEmpty()) price.toInt() else cheapest!!.price
                val product_regular_price = if(variations.isNullOrEmpty()) ( if(it.regular_price.isNullOrEmpty()) price.toInt() else it.regular_price?.toInt()) else cheapest!!.regularPrice


                val desc = it.short_description.orEmpty()
                Product(id, name, product_price, desc, product_regular_price, variations, it.on_sale, it.purchasable, it.shipping_required, categories, images)
            }
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getCategories(parentCategory: Int): List<Category> {
        val response = _Api.getCategories(parentCategory).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return emptyList()
            return body.map {
                Category(it.id, it.name)
            }

        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun Login(username: String, password: String): Boolean {
        val response = _Api.getToken(username, password).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return false
            val id = body.id?:return false
            val name = body.user_display_name?:return false
            val token = body.token?:return false
            val email = body.user_email.orEmpty()
            User.login(token, email, name, id, User.HITA, null)
            return true
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getUserInfo(userId: String): UserInfo {
        val response = _Api.getUserInfo(userId).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: throw ApiException("")
            val id = body.id?: throw ApiException("")
            val first_name = body.first_name.orEmpty()
            val last_name = body.last_name.orEmpty()
            val email = body.email.orEmpty()
            val username = body.username.orEmpty()
            val phone = body.billing?.phone.orEmpty()
            val address = body.billing?.address_1.orEmpty()
            val avatar_url = body.avatar_url.orEmpty()

            UserInfo(id,first_name, last_name, username,avatar_url, email, address, phone)
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun updateUserInfo(userId: String, request:EditInfoRequest):UserInfo {
        val response = _Api.updateUserInfo(userId, request).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: throw ApiException("")
            val id = body.id?: throw ApiException("")
            val first_name = body.first_name.orEmpty()
            val last_name = body.last_name.orEmpty()
            val email = body.email.orEmpty()
            val username = body.username.orEmpty()
            val phone = body.billing?.phone.orEmpty()
            val address = body.billing?.address_1.orEmpty()
            val avatar_url = body.avatar_url.orEmpty()

            UserInfo(id,first_name, last_name, username,avatar_url, email, address, phone)
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }


    override fun placeOrder(order: OrderRequest): Order? {
        val response = _Api.placeOrder(order).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return null
            val id = body.id?: return null
            val dateCreated = body.date_created?: return null
            val dateCreatedGMT = body.date_created_gmt?: return null
            val status =  body.status?:return null
            val shippingAddress = body.shipping?.address_1.orEmpty()
            val shippingTotal = if(body.shipping_total.isNullOrEmpty()) 0 else body.shipping_total?.toInt()?:0
            val shippingPhone = body.billing?.phone.orEmpty()
            val shippingNotes = body.customer_note.orEmpty()
            val shippingName = body.shipping?.first_name.orEmpty()
            val discount =if(body.discount_total.isNullOrEmpty()) 0 else body.discount_total?.toInt()?:0
            var total = -discount + shippingTotal
            val details = body.line_items?.mapNotNull {
                val name = it.name?:return@mapNotNull null
                val productId = it.product_id?:return@mapNotNull null
                val variationId = it.variation_id
                val quantity = it.quantity?:return@mapNotNull null
                val totalPrice = it.total?:return@mapNotNull null
                val price = it.price?:return@mapNotNull null
                val subtotalPrice = it.subtotal?:totalPrice
                total+=subtotalPrice.toInt()
                OrderDetail(name, productId,variationId, quantity, totalPrice.toInt(), price.toInt(), subtotalPrice.toInt())
            }
            return Order(id, total, dateCreated, dateCreatedGMT, shippingAddress, status.toLowerCase(), details.orEmpty(), shippingTotal, shippingPhone, shippingName, shippingNotes, discount)
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun cancelOrder(orderId: String, order: CancelOrderRequest): Order? {
        val response = _Api.cancelOrder(orderId, order).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return null
            val id = body.id?: return null
            val dateCreated = body.date_created?: return null
            val dateCreatedGMT = body.date_created_gmt?: return null
            val status =  body.status?:return null
            val shippingAddress = body.shipping?.address_1.orEmpty()
            val shippingTotal = if(body.shipping_total.isNullOrEmpty()) 0 else body.shipping_total?.toInt()?:0
            val shippingPhone = body.billing?.phone.orEmpty()
            val shippingNotes = body.customer_note.orEmpty()
            val shippingName = body.shipping?.first_name.orEmpty()
            val discount =if(body.discount_total.isNullOrEmpty()) 0 else body.discount_total?.toInt()?:0

            var total = -discount + shippingTotal

            val details = body.line_items?.mapNotNull {
                val name = it.name?:return@mapNotNull null
                val productId = it.product_id?:return@mapNotNull null
                val variationId = it.variation_id
                val quantity = it.quantity?:return@mapNotNull null
                val totalPrice = it.total?:return@mapNotNull null
                val price = it.price?:return@mapNotNull null
                val subtotalPrice = it.subtotal?:totalPrice
                total += subtotalPrice.toInt()
                OrderDetail(name, productId,variationId, quantity, totalPrice.toInt(), price.toInt(), subtotalPrice.toInt())
            }
            return Order(id, total, dateCreated, dateCreatedGMT, shippingAddress, status.toLowerCase(), details.orEmpty(), shippingTotal, shippingPhone, shippingName, shippingNotes, discount)
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }

    override fun getOrdersHistory(customerId: Int): List<Order> {
        val response = _Api.getOrdersHistory(customerId).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return emptyList()
            return body.mapNotNull {
                val id = it.id?:return@mapNotNull null
                val dateCreated = it.date_created?: return@mapNotNull  null
                val dateCreatedGMT = it.date_created_gmt?: return@mapNotNull  null
                val status =  it.status?:return@mapNotNull null
                val shippingAddress = it.shipping?.address_1.orEmpty()
                val shippingTotal = if(it.shipping_total.isNullOrEmpty()) 0 else it.shipping_total?.toInt()?:0
                val shippingPhone = it.billing?.phone.orEmpty()
                val shippingNotes = it.customer_note.orEmpty()
                val shippingName = it.shipping?.first_name.orEmpty()
                val discount =if(it.discount_total.isNullOrEmpty()) 0 else it.discount_total?.toInt()?:0
                var total = -discount + shippingTotal
                val details = it.line_items?.mapNotNull { detail->
                    val name = detail.name?: return@mapNotNull null
                    val productId = detail.product_id?:return@mapNotNull null
                    val variationId = detail.variation_id
                    val quantity = detail.quantity?:return@mapNotNull null
                    val totalPrice = detail.total?:return@mapNotNull null
                    val price = detail.price?:return@mapNotNull null
                    val subtotalPrice = detail.subtotal?:totalPrice
                    total += subtotalPrice.toInt()
                    OrderDetail(name, productId,variationId, quantity, totalPrice.toInt(), price.toInt(), subtotalPrice.toInt())
                }
                Order(id, total, dateCreated, dateCreatedGMT, shippingAddress, status.toLowerCase(), details.orEmpty(), shippingTotal, shippingPhone, shippingName, shippingNotes, discount)
            }
        } else {
            throw ApiException("Error ${response.code()}")
        }
    }
}