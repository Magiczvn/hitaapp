package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class GoogleAuthResponse {
    @SerializedName("access_token")
    var access_token: String? = null

    @SerializedName("expires_in")
    var expires_in: Int? = null

    @SerializedName("token_type")
    var token_type: String? = null

    @SerializedName("id_token")
    var id_token: String? = null
}