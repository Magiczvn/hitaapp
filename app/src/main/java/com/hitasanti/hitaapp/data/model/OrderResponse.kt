package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

class OrderResponse {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("total")
    var total: Int? = null

    @SerializedName("discount_total")
    var discount_total: String? = null

    @SerializedName("shipping_total")
    var shipping_total: String? = null


    @SerializedName("date_created")
    var date_created: Date? = null

    @SerializedName("date_created_gmt")
    var date_created_gmt: Date? = null

    @SerializedName("customer_id")
    var customer_id: Int? = null

    @SerializedName("customer_note")
    var customer_note: String? = null

    @SerializedName("billing")
    var billing: BillingDetails? = null

    @SerializedName("shipping")
    var shipping: BillingDetails? = null

    @SerializedName("payment_method")
    var payment_method: String? = null

    @SerializedName("payment_method_title")
    var payment_method_title: String? = null

    @SerializedName("line_items")
    var line_items: List<ProductDetails>? = null


    class BillingDetails {
        @SerializedName("first_name")
        var first_name: String? = null

        @SerializedName("last_name")
        var last_name: String? = null

        @SerializedName("address_1")
        var address_1: String? = null

        @SerializedName("city")
        var city: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("phone")
        var phone: String? = null

    }

    class ProductDetails{
        @SerializedName("name")
        var name: String? = null

        @SerializedName("product_id")
        var product_id: Int? = null

        @SerializedName("variation_id")
        var variation_id: Int? = null

        @SerializedName("quantity")
        var quantity: Int? = null

        @SerializedName("total")
        var total: String? = null

        @SerializedName("subtotal")
        var subtotal: String? = null

        @SerializedName("price")
        var price: String? = null
    }


}