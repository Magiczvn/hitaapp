package com.hitasanti.hitaapp.data.news

import com.hitasanti.hitaapp.data.ApiException
import com.hitasanti.hitaapp.repository.datasource.NewsDataSource
import com.hitasanti.hitaapp.repository.model.Article

class ApiNewsDataSource(private val _Api: NewsApi):NewsDataSource {
    override fun getLatestArticle(paging: Int, category: Int): List<Article> {
        val response = _Api.getLatestArticle(paging = paging, categories = category).execute()
        return if (response.isSuccessful) {
            val body = response.body()?: return emptyList()
            return body.map {
                val thumbURL = it.embedded?.featuredMedia?.getOrNull(0)?.url
                Article(it.id, it.link,it.title?.rendered.orEmpty(), it.excerpt?.rendered.orEmpty(), thumbURL)
            }

        } else {
            throw ApiException("Error ${response.code()}")
        }
    }
}