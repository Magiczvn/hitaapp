package com.hitasanti.hitaapp.data.news

import com.hitasanti.hitaapp.data.model.NewsModelResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import retrofit2.http.Url

interface NewsApi {
    @GET("wp-json/wp/v2/posts?_embed&order=desc&orderby=date&_fields=title,link,id,excerpt,_links.wp:featuredmedia")
    fun getLatestArticle(@Query("categories") categories: Int, @Query("per_page") paging: Int, @Query("page") page: Int = 1): Call<List<NewsModelResponse>>
}