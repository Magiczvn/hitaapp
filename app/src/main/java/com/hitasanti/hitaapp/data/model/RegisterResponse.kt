package com.hitasanti.hitaapp.data.model

import com.google.gson.annotations.SerializedName

class RegisterResponse {
    @SerializedName("id")
    var id: Int? = null
}