package com.hitasanti.hitaapp.data.model

class FacebookLoginRequest (val access_token: String){
    val token_type = "bearer"
    val expires_in = 6000000
}