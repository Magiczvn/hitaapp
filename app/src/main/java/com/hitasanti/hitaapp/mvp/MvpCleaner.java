package com.hitasanti.hitaapp.mvp;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.lang.reflect.Field;

public class MvpCleaner {

    private static final String KEY_PRESENTER_ID = "com.rey.mvp.presenter_id";
    private static final String KEY_VIEWSTATE_TAG = "com.rey.mvp.viewstate_tag";

    private PresenterCache mPresenterCache;
    private ViewStateCache mViewStateCache;

    private static Field sStateField;

    static {
        try {
            sStateField = Fragment.SavedState.class.getDeclaredField("mState");
            sStateField.setAccessible(true);
        } catch (NoSuchFieldException e) {
        }
    }

    public MvpCleaner(PresenterCache presenterCache, ViewStateCache viewStateCache) {
        mPresenterCache = presenterCache;
        mViewStateCache = viewStateCache;
    }

    public void clean(@NonNull Fragment.SavedState savedState, boolean cleanViewState, boolean shouldCleanAds) {
        Bundle savedInstanceState = getSavedInstanceState(savedState);
        if (savedInstanceState == null) {
            return;
        }

        try {
            String viewStateTag = savedInstanceState.getString(KEY_VIEWSTATE_TAG);
            int presenterId = savedInstanceState.getInt(KEY_PRESENTER_ID);

            Presenter presenter = mPresenterCache.getPresenter(presenterId);
            if (presenter == null) {
                return;
            }


            mPresenterCache.setPresenter(presenterId, null);
            presenter.onDestroy();

            ViewState viewState = mViewStateCache.getViewState(viewStateTag);
            if (viewState == null) {
                return;
            }

            if (viewState.onUnbind(presenter) && cleanViewState) {
                mViewStateCache.removeViewState(viewState);
            }
        } catch (Exception ex) {
        }
    }

    @Nullable
    private Bundle getSavedInstanceState(@NonNull Fragment.SavedState savedState) {
        try {
            return (Bundle) sStateField.get(savedState);
        } catch (IllegalAccessException e) {
        }
        return null;
    }
}
