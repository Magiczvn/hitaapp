package com.hitasanti.hitaapp.mvp

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


abstract class BaseDialogFragment<T : Screen> : DialogFragment() {

    companion object {
        private const val KEY_SCREEN = "BaseDialogFragment_screen"
    }

    val screen: T by lazy {
        arguments?.getParcelable<T>(KEY_SCREEN) as T
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(layoutResource, container, false)

    protected fun setScreen(screen: T) {
        var args: Bundle? = arguments
        if (args == null) {
            args = Bundle()
            arguments = args
        }

        args.putParcelable(KEY_SCREEN, screen)
    }

    protected abstract val layoutResource: Int

    fun show(manager: FragmentManager) {
        val ft = manager.beginTransaction()
        ft.add(this, this::class.java.name)
        ft.commitAllowingStateLoss()
        manager.executePendingTransactions()
    }
}