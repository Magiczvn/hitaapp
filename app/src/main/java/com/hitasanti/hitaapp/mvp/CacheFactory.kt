package com.hitasanti.hitaapp.mvp

interface CacheFactory {

    val presenterCache: PresenterCache

    val viewStateCache: ViewStateCache
}