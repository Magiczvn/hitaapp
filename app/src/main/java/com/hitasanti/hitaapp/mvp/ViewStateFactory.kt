package com.hitasanti.hitaapp.mvp

interface ViewStateFactory<out S : ViewState> {

    fun createViewState(): S

    fun getViewStateTag(): String
}