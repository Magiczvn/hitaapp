package com.hitasanti.hitaapp.mvp

interface PresenterFactory<out P : Presenter<*, *>> {

    fun createPresenter(): P
}