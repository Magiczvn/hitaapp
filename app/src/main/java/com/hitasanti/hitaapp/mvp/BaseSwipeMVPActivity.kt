package com.hitasanti.hitaapp.mvp


import android.os.Bundle
import androidx.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.view.BlankDrawable
import com.hitasanti.hitaapp.view.SwipeDismissLayout


abstract class BaseSwipeMvpActivity<V, out P : Presenter<V, S>, S : ViewState, out T : Screen> : BaseMvpActivity<V, P, S, T>() {

    private lateinit var _SwipeDismissLayout: SwipeDismissLayout

    protected open val enterAnimation = R.anim.slide_from_right

    protected open val exitAnimation = R.anim.slide_to_right

    override fun onCreate(savedInstanceState: Bundle?) {
        window.setBackgroundDrawable(BlankDrawable())
        super.onCreate(savedInstanceState)
    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        _SwipeDismissLayout = SwipeDismissLayout(this)
        _SwipeDismissLayout.setSwipeCallBack {
            finish()
            overridePendingTransition(0, 0)
        }
        _SwipeDismissLayout.setConvertTranslucent(object : SwipeDismissLayout.ConvertTranslucent {
            override fun convertToTranslucent(isTranslucent: Boolean) {
                convertTranslucent(isTranslucent,false)
            }
        })

        _SwipeDismissLayout.setOnTouchCallBack(object : SwipeDismissLayout.TouchEvent{
            override fun onTouch() {
                onTouchRootView()
            }
        })

        val contentView = LayoutInflater.from(this).inflate(layoutResID, _SwipeDismissLayout, false)
        _SwipeDismissLayout.addView(contentView, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        if (enterAnimation != 0) {
            val slideAnimation = AnimationUtils.loadAnimation(this, enterAnimation)
            slideAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationEnd(animation: Animation?) {
                    convertTranslucent(false,true)
                }

                override fun onAnimationRepeat(animation: Animation?) {
                }

                override fun onAnimationStart(animation: Animation?) {
                }
            })
            _SwipeDismissLayout.startAnimation(slideAnimation)
        }
        super.setContentView(_SwipeDismissLayout)
    }

    protected open fun onDismiss() {
        finish()
    }

    protected open fun onTouchRootView(){}

    protected open fun convertTranslucent(isTranslucent: Boolean,enableOrientation: Boolean) {}

    protected fun enableConvertTranslucent(enable: Boolean) {
        _SwipeDismissLayout.setEnableConvertTranslucent(enable)
    }

    protected fun enableSwipe(enable: Boolean) {
        _SwipeDismissLayout.setEnableSwipe(enable)
    }

    protected fun isEnableSwipe(): Boolean {
        return _SwipeDismissLayout.mIsEnableSwipe
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(0, exitAnimation)
    }
}