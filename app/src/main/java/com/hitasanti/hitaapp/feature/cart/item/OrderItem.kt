package com.hitasanti.hitaapp.feature.cart.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.news.item.ArticleItem

class OrderItem (val id: Int, val name:String, val price: Int, val subTotal: Int?, val quantity: Int, val description: String, val imageURL: String, val variationId: Int?, val isLastItem:Boolean = false): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is OrderItem && (other === this || other.id == id)
}