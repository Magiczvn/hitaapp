package com.hitasanti.hitaapp.feature.productdialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import com.bumptech.glide.Glide
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.mvp.BaseDialogFragment
import com.hitasanti.hitaapp.repository.model.ProductVariation
import com.hitasanti.hitaapp.view.NumberUtil
import com.hitasanti.hitaapp.view.OrderItemView
import com.hitasanti.hitaapp.view.ProductVariationView
import com.hitasanti.hitaapp.view.ProductVariationViewGroup
import io.reactivex.Completable
import kotlinx.android.synthetic.main.product_dialog_fragment.*
import java.text.DecimalFormat

class ProductDialogFragment: BaseDialogFragment<ProductDialogScreen>(), ProductVariationViewGroup.onCheckChangedListener {

    interface Listener {
        fun onAddToCart( productId: Int, quantity: Int, isVariation: Boolean)

        fun onUpdateCart( productId: Int, quantity: Int, isVariation: Boolean)

        fun onCancel()
    }

    companion object {

        fun instance(screen: ProductDialogScreen): ProductDialogFragment {
            val fragment = ProductDialogFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Private
    private val _Listener: Listener? get() = parentFragment as? Listener ?: context as? Listener
    private var quantity = 1
    private var currentVariation: ProductVariation? = null
    //endregion Private

    //region MVP
    override val layoutResource get() = R.layout.product_dialog_fragment
    //endregion MVP

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(R.drawable.rounded_dialog_bg)


        return dialog
    }

    override fun onCheckChanged(variationView: ProductVariationView) {
        currentVariation = variationView.productVariation
        updateQuantity()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        GlideApp.with(this)
            .load(screen.imageUrl)
            .centerCrop()
            .dontAnimate()
            .into(product_logo)

        product_name?.text = screen.name
        product_detail?.movementMethod = ScrollingMovementMethod()
        product_detail?.text = HtmlCompat.fromHtml(screen.shortDescription,HtmlCompat.FROM_HTML_MODE_LEGACY)
        product_add?.setOnClickListener {
            quantity++
            updateQuantity()
        }
        product_remove?.setOnClickListener {
            if((screen.canRemove && quantity == 0)||(!screen.canRemove && quantity == 1))
                return@setOnClickListener
            quantity--
            updateQuantity()
        }
        product_buy?.setOnClickListener {
            positiveClick()
        }

        quantity = screen.quantity
        updateQuantity()

        val product = Cart.productsMap[screen.id]
        if(product != null && !product.variations.isNullOrEmpty()){
            var hasSetFirstItem = false
            var firstItemId = 0
            val variations = product.variations.orEmpty()


            /*product.variations.forEach {
                val variation = Cart.variationsMap[it]?:return@forEach
                variations.add(variation)
            }*/

            /*variations.sortBy {
                it.menuOrder
            }*/

            variations.forEach { variation ->
                val view = layoutInflater.inflate(R.layout.product_variation_view, variation_group, false) as ProductVariationView

                view.setVariation(variation)

                variation_group.addView(view)

                if(!hasSetFirstItem){
                    hasSetFirstItem = true
                }
            }


            if(hasSetFirstItem){
                variation_divider.visibility = View.VISIBLE
                variation_group.visibility = View.VISIBLE
                variation_group.setOnCheckChangedListener(this)
                if(screen.variationId == null)
                    variation_group.setDefaultSelected()
                else {
                    variation_group.setSelected(screen.variationId!!)
                }

            }
        }

        if(product != null && product.onSale == true){
            lblSale.visibility = View.VISIBLE
        }

        super.onViewCreated(view, savedInstanceState)
    }
    //endregion Lifecycle

    private fun updateQuantity() {
        product_quantity?.text = quantity.toString()
        val currentPrice = currentVariation?.price?:screen.price
        product_buy?.text = if(quantity == 0) context!!.getString(R.string.txt_remove_product) else NumberUtil.formatPrice(context!!, currentPrice*quantity)
    }

    //region Handle Click
    private fun positiveClick() {
        val id = currentVariation?.id?:screen.id
        if(screen.canRemove) {
            if(quantity == 0 && screen.quantity == Cart.totalItems){
                MaterialDialog.Builder(context!!)
                    .title(R.string.txt_empty_cart)
                    .theme(Theme.LIGHT)
                    .positiveText(R.string.txt_yes)
                    .onPositive { _, _ ->
                        dismissAllowingStateLoss()
                        val variationId = screen.variationId
                        if(variationId != null){
                            _Listener?.onUpdateCart(variationId, quantity, currentVariation != null)
                        }
                        else {
                            _Listener?.onUpdateCart(screen.id, quantity, currentVariation != null)
                        }
                    }
                    .negativeText(R.string.txt_no)
                    .show()
            }
            else{
                dismissAllowingStateLoss()
                val variationId = screen.variationId
                if(variationId != null){
                    val newId = currentVariation?.id
                    if(newId != null && newId != variationId){//switch option
                        val oldVariation = Cart.variationsMap[variationId]

                        if(quantity > 0) {
                            Cart.updateProduct(oldVariation!!, 0)
                            _Listener?.onAddToCart(newId, quantity, true)
                        }
                        else {
                            val newQuantity = Cart.cartMap[newId]?:0
                            if(newQuantity == 0){
                                _Listener?.onUpdateCart(variationId, quantity, true)
                            }
                            else{
                                _Listener?.onUpdateCart(newId, quantity, true)
                            }

                        }
                    }
                    else{
                        _Listener?.onUpdateCart(variationId, quantity, true)
                    }
                }
                else{
                    _Listener?.onUpdateCart(screen.id, quantity, false)
                }
            }

        }
        else {
            dismissAllowingStateLoss()
            _Listener?.onAddToCart(id, quantity, currentVariation != null)
        }
    }

    private fun negativeClick() {
        dismissAllowingStateLoss()
        _Listener?.onCancel()
    }
    //endregion Handle Click
}