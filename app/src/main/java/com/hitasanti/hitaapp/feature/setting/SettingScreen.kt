package com.hitasanti.hitaapp.feature.setting

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class SettingScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is SettingScreen && (other === this)

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SettingScreen> = object : Parcelable.Creator<SettingScreen> {
            override fun createFromParcel(parcel: Parcel): SettingScreen {
                return SettingScreen()
            }

            override fun newArray(size: Int) = arrayOfNulls<SettingScreen?>(size)
        }
    }
}