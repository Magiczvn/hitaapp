package com.hitasanti.hitaapp.feature.editinfo

import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.dependency.ActivityScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class EditInfoModule(private val _Activity: EditInfoActivity) {

    @Provides
    @ActivityScope
    internal fun provideGlide(): RequestManager = GlideApp.with(_Activity)


    @Provides
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>): EditInfoContract.Presenter = EditInfoPresenter(useCaseFactory, schedulerFactory)
}
