package com.hitasanti.hitaapp.feature.store

import com.hitasanti.hitaapp.dependency.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [StoreModule::class])
interface StoreComponent {

    val presenter: StoreContract.Presenter

    fun inject(fragment: StoreFragment)
}
