package com.hitasanti.hitaapp.feature.store


import com.hitasanti.hitaapp.mvp.BaseViewState

class StoreViewState(val screen: StoreScreen)
    : BaseViewState() {

    var currentTab = 1

}