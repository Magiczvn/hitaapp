package com.hitasanti.hitaapp.feature.order

import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class OrderModule() {
    @Provides
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>): OrderContract.Presenter = OrderPresenter(useCaseFactory, schedulerFactory)
}
