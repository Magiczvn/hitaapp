package com.hitasanti.hitaapp.feature.login

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [LoginModule::class])
interface LoginComponent {

    val presenter: LoginContract.Presenter

    fun inject(activity: LoginActivity)
}
