package com.hitasanti.hitaapp.feature.history

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.CartActivity
import com.hitasanti.hitaapp.feature.cart.CartScreen
import com.hitasanti.hitaapp.feature.cart.event.CartItemClickEvent
import com.hitasanti.hitaapp.feature.cart.event.NewOrderEvent
import com.hitasanti.hitaapp.feature.history.event.HistoryClickedEvent
import com.hitasanti.hitaapp.feature.history.event.OrderCancelEvent
import com.hitasanti.hitaapp.feature.history.event.ReorderClickEvent
import com.hitasanti.hitaapp.feature.home.event.OpenOrderPageEvent
import com.hitasanti.hitaapp.feature.home.event.OpenStorePageEvent
import com.hitasanti.hitaapp.feature.order.event.CartUpdateEvent
import com.hitasanti.hitaapp.feature.orderdetails.OrderDetailsActivity
import com.hitasanti.hitaapp.feature.orderdetails.OrderDetailsScreen
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import com.hitasanti.hitaapp.repository.model.Order
import com.hitasanti.hitaapp.view.ViewUtil
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.history_activity.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HistoryActivity : BaseSwipeMvpActivity<HistoryContract.View, HistoryContract.Presenter, HistoryViewState, HistoryScreen>(),
        HasComponent<HistoryComponent>, HistoryContract.View {

    companion object {

        fun instance(context: Context, screen: HistoryScreen): Intent {
            val intent = Intent(context, HistoryActivity::class.java)
            setScreen(intent, screen)
            return intent
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>
    //endregion Dependency
    @Inject
    lateinit var _Adapter: HistoryAdapter

    private var _Disposable: CompositeDisposable? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = HistoryViewState(screen)

    override val viewStateTag: String get() = HistoryViewState::class.java.name

    override val layoutResource get() = R.layout.history_activity
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(HistoryModule(this))
    }
    //endregion HasComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
        enableSwipe(true)

        history_rv?.adapter = _Adapter

        history_rv?.let {
            ViewUtil.doOnGlobalLayout(it) {
                _Adapter.setReady(true)
            }
        }
        history_rv?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        _Disposable = CompositeDisposable(_Adapter.event
            .throttleFirst(BuildConfig.BUTTON_DELAY, TimeUnit.MILLISECONDS)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                when (it) {
                    is HistoryClickedEvent ->{
                        showOrderHistory()
                    }
                    is ReorderClickEvent ->{
                        reOrder(it.order)
                    }
                }
            }, ErrorConsumer()),
            _Bus.register(NewOrderEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    presenter.addNewOrder(it.order)
                }, ErrorConsumer()),
            _Bus.register(OrderCancelEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    presenter.orderCancelled()
                }, ErrorConsumer()))

        loading_pv?.visibility = View.VISIBLE

        history_new_order?.setOnClickListener {
            _Bus.post(OpenOrderPageEvent(this))
            finish()
        }

    }

    private fun reOrder(order: Order){
        Cart.reOrder(order)
        _Bus.post(CartUpdateEvent())
        viewCart()
    }

    private fun viewCart(){
        val intent = CartActivity.instance(context = this, screen = CartScreen(1,""))
        startActivity(intent)
    }

    private fun showOrderHistory(){
        val intent = OrderDetailsActivity.instance(this, OrderDetailsScreen())
        startActivity(intent)
    }

    override fun showItems(items: List<Item>) {
        if(items.isEmpty())
            pnl_empty.visibility = View.VISIBLE
        else {
            _Adapter.updateItems(items)
            if(items.isNotEmpty())
                history_rv?.scrollToPosition(0)
        }

        loading_pv?.visibility = View.GONE
    }

    override fun onDestroy() {
        _Disposable?.dispose()
        _Adapter?.onDestroy()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
    }

}