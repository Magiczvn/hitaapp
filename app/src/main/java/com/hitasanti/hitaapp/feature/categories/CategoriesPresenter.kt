package com.hitasanti.hitaapp.feature.categories

import android.util.Log
import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.categories.item.CategoryDetails
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import timber.log.Timber
import java.util.concurrent.Callable


class CategoriesPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                    private val _SchedulerFactory: Lazy<SchedulerFactory>,
                    private val _ItemBuilder: Lazy<NewsItemBuilder>)
    : BasePresenter<CategoriesContract.View, CategoriesViewState>(), CategoriesContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private val _Items = ItemsResult()
    private var _GetProductsDisposable: Disposable? = null
    private var _GetCategoriesDisposable: Disposable? = null
    private var _GetVariationDisposable: Disposable? = null
    private var _GetCacheVariationDisposable: Disposable? = null
    private var _ShowProductsDisposable: Disposable? = null


    //endregion Private



    //region BasePresenter
    override fun onAttachView(view: CategoriesContract.View) {
        super.onAttachView(view)
        mViewState.items?.let {
            view.showItems(it)
        }
    }

    override fun startLoading() {
        getProducts()
    }

    private val listCategories = mutableListOf(/*CategoryDetails.HitaChayCategory, CategoryDetails.HitaFarmCategory,*/
        CategoryDetails.HitaNhangCategory, CategoryDetails.HitaTeaCategory)
    private val allCategories = HashSet<Int>()
    //private val allProductHasVariation = HashSet<Int>()

    private fun getCategories(categoryId: Int) {
        _GetCategoriesDisposable?.dispose()
        _GetCategoriesDisposable = _UseCaseFactory.get().getCategories(categoryId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe( Consumer {

                it.forEach {cate->
                    val id = cate.id?:return@forEach
                    allCategories.add(id)
                }

                if(listCategories.isNotEmpty()) {
                    val category = listCategories.removeAt(listCategories.size - 1)
                    getCategories(category.categoryId)
                }
                else{
                    val category = allCategories.first()
                    allCategories.remove(category)
                    getProducts(category)
                }
            }, ErrorConsumer())
    }

    private fun getProducts(categoryId: Int) {
        _GetProductsDisposable?.dispose()
        _GetProductsDisposable = _UseCaseFactory.get().getProducts(categoryId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe( Consumer {

                Cart.addToProductsMap(it)

                /*it.forEach {product->
                    if(!product.variations.isNullOrEmpty()){
                        allProductHasVariation.add(product.id)
                    }
                }*/

                if(allCategories.isNotEmpty()) {
                    val category = allCategories.first()
                    allCategories.remove(category)
                    getProducts(category)
                }
                /*else{
                    if(allProductHasVariation.isNotEmpty()) {
                        val product = allProductHasVariation.first()
                        allProductHasVariation.remove(product)
                        cacheProductVariation(product)
                    }
                }*/
            }, ErrorConsumer())
    }

    /*private fun cacheProductVariation(productId: Int){
        _GetCacheVariationDisposable?.dispose()
        _GetCacheVariationDisposable = _UseCaseFactory.get().getProductVariations(productId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe( Consumer {

                Cart.addToVariationMap(it)

                if(allProductHasVariation.isNotEmpty()) {
                    val product = allProductHasVariation.first()
                    allProductHasVariation.remove(product)
                    cacheProductVariation(product)
                }
            }, ErrorConsumer())
    }*/


    /*override fun getProductVariation(productId: Int) {
        _GetVariationDisposable?.dispose()
        _GetVariationDisposable = _UseCaseFactory.get().getProductVariations(productId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe( Consumer {
                Cart.addToVariationMap(it)
                val product = Cart.productsMap[productId]
                if(product!=null)
                    mView?.showProductDialog(product)
            }, ErrorConsumer())
    }*/

    private fun showProductsAsync() {

        val products = mViewState.productsMap?:return

        val work = Callable<Unit> {
            val oldItems = mViewState.items.orEmpty()
            val newItems = _ItemBuilder.get().addCategoryItems(oldItems, products)
            mViewState.items = newItems
            _Items.pushItems(newItems)
        }

        _ShowProductsDisposable?.dispose()
        _ShowProductsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
            }, ErrorConsumer())
    }


    private fun getProducts() {
        if(mViewState.productsMap != null) return
        _GetProductsDisposable?.dispose()
        _GetProductsDisposable = _UseCaseFactory.get().getAllProducts()
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_WorkerScheduler)
            .subscribe( Consumer {
                mViewState.productsMap = it

                Cart.addToProductsMap(it[CategoryDetails.HitaCofeeCategory.categoryId].orEmpty())
                Cart.addToProductsMap(it[CategoryDetails.HitaNhangCategory.categoryId].orEmpty())
                Cart.addToProductsMap(it[CategoryDetails.HitaTeaCategory.categoryId].orEmpty())
                //Cart.addToProductsMap(it[CategoryDetails.HitaChayCategory.categoryId].orEmpty())
                //Cart.addToProductsMap(it[CategoryDetails.HitaFarmCategory.categoryId].orEmpty())

                getCategories(CategoryDetails.HitaCofeeCategory.categoryId)

                showProductsAsync()
            }, ErrorConsumer())
    }


    override fun onDestroy() {
        _GetProductsDisposable?.dispose()
        _ShowProductsDisposable?.dispose()
        _GetCategoriesDisposable?.dispose()
        _GetCacheVariationDisposable?.dispose()
        _GetVariationDisposable?.dispose()
        super.onDestroy()

    }

    private fun showItemResult() {
        val items = _Items.popItems()

        items?.let { mView?.showItems(it) }
    }
    //endregion BasePresenter

}