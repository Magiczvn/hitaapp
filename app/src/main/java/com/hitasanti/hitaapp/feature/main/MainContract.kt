package com.hitasanti.hitaapp.feature.main

interface MainContract {

    interface View {


    }

    interface Presenter:com.hitasanti.hitaapp.mvp.Presenter<View, MainViewState> {
        var isShowedSplashScreen: Boolean

    }
}