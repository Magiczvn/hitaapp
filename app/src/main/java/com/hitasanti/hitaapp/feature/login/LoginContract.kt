package com.hitasanti.hitaapp.feature.login

import com.facebook.AccessToken
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.data.model.GoogleAuthResponse
import com.hitasanti.hitaapp.feature.cart.item.AddressItem

interface LoginContract {

    interface View {
        fun showLoginResult(result: Boolean)

        fun showLoginResult(result: Boolean, service: String)

        fun showGoogleAuthCode(googleAuthResponse: GoogleAuthResponse)

    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, LoginViewState> {
        fun Login(username: String, password: String)

        fun getGoogleAuthCode(authCode: String, clientId: String)

        fun facebookLoogin(accessToken: AccessToken, email: String, fbId: String)

        fun register(userId: String, email: String, service: Int)

        fun googleLogin(googleAuthResponse: GoogleAuthResponse)
    }
}