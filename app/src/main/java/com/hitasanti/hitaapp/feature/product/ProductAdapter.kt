package com.hitasanti.hitaapp.feature.product

import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemAdapter
import com.hitasanti.hitaapp.feature.product.item.ProductItem
import com.hitasanti.hitaapp.feature.product.viewholder.ProductItemViewHolder


class ProductAdapter(private val _Glide: RequestManager) : BaseItemAdapter() {

    companion object {
        private const val ITEM_PRODUCT = 1
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItemAt(position)
        return when (item) {
            is ProductItem -> ITEM_PRODUCT

            else -> throw RuntimeException("Not support item $item")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        ITEM_PRODUCT -> ProductItemViewHolder(parent, R.layout.product_item_viewholder, event, _Glide)
        else -> throw RuntimeException("Not support type $viewType")
    }

}