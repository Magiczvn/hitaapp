package com.hitasanti.hitaapp.feature.product

import com.hitasanti.hitaapp.dependency.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [ProductModule::class])
interface ProductComponent {

    val presenter: ProductContract.Presenter

    fun inject(fragment: ProductFragment)
}
