package com.hitasanti.hitaapp.feature.orderdetails

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.CartActivity
import com.hitasanti.hitaapp.feature.cart.CartScreen
import com.hitasanti.hitaapp.feature.cart.event.NewOrderEvent
import com.hitasanti.hitaapp.feature.history.event.HistoryClickedEvent
import com.hitasanti.hitaapp.feature.history.event.OrderCancelEvent
import com.hitasanti.hitaapp.feature.history.event.ReorderClickEvent
import com.hitasanti.hitaapp.feature.order.event.CartUpdateEvent
import com.hitasanti.hitaapp.feature.orderdetails.event.CancelOrderButtonClickedEvent
import com.hitasanti.hitaapp.feature.orderdetails.event.ReorderButtonClickedEvent
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import com.hitasanti.hitaapp.repository.model.Order
import com.hitasanti.hitaapp.view.ViewUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.cart_activity.cart_rv
import kotlinx.android.synthetic.main.order_details_activity.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class OrderDetailsActivity : BaseSwipeMvpActivity<OrderDetailsContract.View, OrderDetailsContract.Presenter, OrderDetailsViewState, OrderDetailsScreen>(),
    HasComponent<OrderDetailsComponent>, OrderDetailsContract.View {
    companion object {

        fun instance(context: Context, screen: OrderDetailsScreen): Intent {
            val intent = Intent(context, OrderDetailsActivity::class.java)
            setScreen(intent, screen)
            return intent
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _Adapter: OrderDetailsAdapter

    //endregion Dependency

    //region Private
    private var _Disposable: CompositeDisposable? = null
    private var _Dialog: Dialog? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = OrderDetailsViewState(screen)

    override val viewStateTag: String get() = OrderDetailsViewState::class.java.name

    override val layoutResource get() = R.layout.order_details_activity

    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(OrderDetailsModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        cart_rv?.adapter = _Adapter

        cart_rv?.let {
            ViewUtil.doOnGlobalLayout(it) {
                _Adapter.setReady(true)
            }
        }
        cart_rv?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val orderDetails = Cart.orderDetails

        val status = Cart.getOrderStatus(this, orderDetails?.status.orEmpty())

        orderDetails?.let {
            order_detail_title.text = "Đơn hàng #${it.id} ($status)"
        }

        enableSwipe(true)


        _Disposable = CompositeDisposable(_Adapter.event
            .throttleFirst(BuildConfig.BUTTON_DELAY, TimeUnit.MILLISECONDS)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                when (it) {
                    is CancelOrderButtonClickedEvent ->{
                        showReasonDialog()
                    }
                    is ReorderButtonClickedEvent ->{
                        val order = orderDetails?: return@Consumer
                        reOrder(order)
                    }
                }
            }, ErrorConsumer()),
            _Bus.register(NewOrderEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    finish()
                }, ErrorConsumer()))
    }

    var reason: String? = null
    private fun showReasonDialog(){
        val dialog = MaterialDialog.Builder(this)
            .title(R.string.txt_select_cancel_reason)
            .items(R.array.cancel_reasons)
            .autoDismiss(false)
            .alwaysCallSingleChoiceCallback()
            .positiveText(R.string.txt_send)
            .theme(Theme.LIGHT)
            .itemsCallbackSingleChoice(-1) { dialog, itemView, which, text ->
                dialog.getActionButton(DialogAction.POSITIVE).isEnabled = true
                reason = text.toString()
                return@itemsCallbackSingleChoice true
            }
            .onPositive { dialog, which ->
                val orderDetails = Cart.orderDetails
                val id = orderDetails?.id
                if(id!= null && reason != null){
                    presenter.cancelOrder(id.toString(), reason.orEmpty())
                    showProgressBar()
                }
                dialog.dismiss()
            }
            .show()
        dialog.getActionButton(DialogAction.POSITIVE).isEnabled = false
    }

    private fun showProgressBar(){
        _Dialog = MaterialDialog.Builder(this)
            .contentGravity(GravityEnum.CENTER)
            .title("Đang xử lý")
            .progress(true, 0)
            .theme(Theme.LIGHT)
            .show()
    }

    private fun reOrder(order: Order){
        Cart.reOrder(order)
        _Bus.post(CartUpdateEvent())
        viewCart()
    }

    private fun viewCart(){
        val intent = CartActivity.instance(context = this, screen = CartScreen(1,""))
        startActivity(intent)
    }


    override fun showCancelOrderResult(result: Boolean) {
        _Dialog?.dismiss()

        val toast = Toast.makeText(this,if(result) "Hủy đơn hàng thành công!" else "Có lỗi khi hủy đơn hàng", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER,0,0)
        toast.show()

        if(result) {
            val orderDetails = Cart.orderDetails

            val status = Cart.getOrderStatus(this, orderDetails?.status.orEmpty())
            orderDetails?.let {
                order_detail_title.text = "Đơn hàng #${it.id} ($status)"
            }

            _Bus.post(OrderCancelEvent())

        }
    }

    override fun showItems(items: List<Item>) {
        _Adapter.updateItems(items)
    }

    override fun onDestroy() {
        _Disposable?.dispose()
        _Dialog?.dismiss()

        super.onDestroy()
    }
}