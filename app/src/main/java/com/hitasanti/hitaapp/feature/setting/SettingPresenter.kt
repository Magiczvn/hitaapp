package com.hitasanti.hitaapp.feature.setting

import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable


class SettingPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                       private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<SettingContract.View, SettingViewState>(), SettingContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private var _GetUserInfoDisposable: Disposable? = null

    override fun getUserInfo() {
        val id = User.mId?: return
        _GetUserInfoDisposable?.dispose()
        _GetUserInfoDisposable = _UseCaseFactory.get().getUserInfo(id.toString())
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                User.mUserInfo = it
                User.save()
            }, {

            })
    }

    //endregion Private

    //region BasePresenter
    override fun onAttachView(view: SettingContract.View) {
        super.onAttachView(view)

    }

    override fun onDestroy() {
        super.onDestroy()

    }
    //endregion BasePresenter

}