package com.hitasanti.hitaapp.feature.order

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [OrderModule::class])
interface OrderComponent {

    val presenter: OrderContract.Presenter

    fun inject(activity: OrderActivity)
}
