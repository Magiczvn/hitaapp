package com.hitasanti.hitaapp.feature.home

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class HomeScreen : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is HomeScreen

    override fun toString(): String {
        return "HomeScreen"
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeScreen> = object : Parcelable.Creator<HomeScreen> {
            override fun createFromParcel(parcel: Parcel): HomeScreen {
                return HomeScreen()
            }

            override fun newArray(size: Int) = arrayOfNulls<HomeScreen?>(size)
        }
    }
}