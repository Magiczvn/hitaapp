package com.hitasanti.hitaapp.feature.product

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewpager.widget.ViewPager
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.order.event.CartUpdateEvent
import com.hitasanti.hitaapp.feature.product.event.ProductViewEvent
import com.hitasanti.hitaapp.feature.productdialog.ProductDialogFragment
import com.hitasanti.hitaapp.feature.productdialog.ProductDialogScreen
import com.hitasanti.hitaapp.mvp.BaseMvpFragment
import com.hitasanti.hitaapp.repository.model.Product
import com.hitasanti.hitaapp.view.ViewUtil
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.product_fragment.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ProductFragment : BaseMvpFragment<ProductContract.View, ProductContract.Presenter, ProductViewState, ProductScreen>(),
        HasComponent<ProductComponent>, ProductContract.View, ProductDialogFragment.Listener {

    companion object {

        fun instance(screen: ProductScreen): ProductFragment {
            val fragment = ProductFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>
    //endregion Dependency
    @Inject
    lateinit var _Adapter: ProductAdapter

    private var _Disposable: CompositeDisposable? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context?) = component.presenter

    override fun onCreateViewState(context: Context?) = ProductViewState(screen)


    override val viewStateTag: String get() = "${ProductViewState::class.java.name}_${screen.categoryId}"

    override val layoutResource get() = R.layout.product_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(context!!).component.plus(ProductModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component.inject(this)
        product_rv?.adapter = _Adapter

        product_rv?.let {
            ViewUtil.doOnGlobalLayout(it) {
                _Adapter.setReady(true)
            }
        }
        product_rv?.layoutManager = GridLayoutManager(context!!, 2)


        _Disposable = CompositeDisposable(_Adapter.event
            .throttleFirst(BuildConfig.BUTTON_DELAY, TimeUnit.MILLISECONDS)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                when (it) {
                    is ProductViewEvent ->{
                        showProduct(it)
                    }
                }
            }, ErrorConsumer()))


        super.onViewCreated(view, savedInstanceState)
    }

    private fun showProduct(event: ProductViewEvent){
        val product = Cart.productsMap[event.id]?:return
        /*if(!product.variations.isNullOrEmpty())
            presenter.getProductVariation(event.id)
        else*/
            showProductDialog(product)
    }

    override fun showProductDialog(product: Product) {
        val productDialogScreen = ProductDialogScreen( id = product.id,
            price = product.price,
            shortDescription = product.shortDescription,
            name = product.name,
            imageUrl =  product.imgages?.getOrNull(0)?.link.orEmpty(),
            quantity = 1,
            variationId = null,
            canRemove = false)
        ProductDialogFragment.instance(productDialogScreen)
            .show(childFragmentManager)
    }

    override fun onDestroyView() {
        _Disposable?.dispose()
        _Adapter.onDestroy()

        super.onDestroyView()
    }

    override fun onResume() {
        super.onResume()
    }
    //endregion Lifecycle

    override fun showItems(items: List<Item>) {
        _Adapter.updateItems(items)
    }

    override fun onAddToCart(productId: Int, quantity: Int, isVariation: Boolean) {
        if(!isVariation) {
            val product = Cart.productsMap[productId] ?: return
            Cart.addProduct(product, quantity)
            val toast = Toast.makeText(context, context?.getString(R.string.txt_added_to_cart, quantity, product.name), Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
            Cart.saveProducts()
        }
        else{
            val variation = Cart.variationsMap[productId]?:return
            val product = Cart.productsMap[variation.productId]?:return
            Cart.addProduct(variation, quantity)
            val toast = Toast.makeText(context, context?.getString(R.string.txt_added_to_cart, quantity, "${product.name} ${variation.name}"), Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
            Cart.saveProducts()
        }
        _Bus.post(CartUpdateEvent())
    }

    override fun onUpdateCart(productId: Int, quantity: Int, isVariation: Boolean) {

    }

    override fun onCancel() {

    }

    private inner class OnPageChangeListener : ViewPager.OnPageChangeListener {

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {

        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }
    //endregion Helper
}