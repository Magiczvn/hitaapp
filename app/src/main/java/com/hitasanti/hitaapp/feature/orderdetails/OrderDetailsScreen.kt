package com.hitasanti.hitaapp.feature.orderdetails

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class OrderDetailsScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is OrderDetailsScreen &&  (other === this)


    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<OrderDetailsScreen> = object : Parcelable.Creator<OrderDetailsScreen> {
            override fun createFromParcel(parcel: Parcel): OrderDetailsScreen {

                return OrderDetailsScreen( )
            }

            override fun newArray(size: Int) = arrayOfNulls<OrderDetailsScreen?>(size)
        }
    }
}