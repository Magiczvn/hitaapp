package com.hitasanti.hitaapp.feature.news.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item

class ArticleItem(val title: String, val thumb: String, val desc: String, val link: String): Item {


        override fun hashCode() = super.hashCode()

        override fun equals(other: Any?) = other is ArticleItem

}