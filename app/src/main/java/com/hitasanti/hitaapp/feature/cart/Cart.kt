package com.hitasanti.hitaapp.feature.cart

import android.content.Context
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.data.model.OrderRequest
import com.hitasanti.hitaapp.feature.cart.event.ApplyCouponResult
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.repository.model.*
import io.paperdb.Paper
import java.util.*
import kotlin.collections.HashSet

object Cart {
    private const val CART = "cart"
    private const val PRODUCTS = "products"

    private val _Book = Paper.book(CART)


    val shoppingList: MutableList<Int>//Id
    val cartMap:MutableMap<Int, Int>//Id, quantity
    val productsMap :MutableMap<Int, Product>
    val variationsMap :MutableMap<Int, ProductVariation>
    var totalPrice: Int = 0
    var totalItems: Int = 0
    private val shippingPrice: Int = 20000
    var shippingAddress: String = ""
    var shippingPhone: String = ""
    var orderDetails: Order? = null
    var coupon:Coupon? = null
    var couponResult: ApplyCouponResult? = null


    init {
        shoppingList = mutableListOf()
        cartMap = mutableMapOf()
        productsMap = mutableMapOf()
        variationsMap = mutableMapOf()
        totalItems = 0
        totalPrice = 0
    }

    fun addToProductsMap(products: List<Product>){
        products.forEach {
            productsMap[it.id] = it
            it.variations?.forEach { variation->
                variationsMap[variation.id]  = variation
            }
        }
    }

    fun addToVariationMap(productVariation: List< ProductVariation>){
        productVariation.forEach {
            variationsMap[it.id] = it
        }
    }

    fun getShippingFee(): Int {
        return if(couponResult?.freeShip == true) 0 else shippingPrice
    }

    fun getDiscountPrice(): Int{
        return couponResult?.amount ?:0
    }

    fun getFinalPrice(): Int{
        return totalPrice + getShippingFee() - getDiscountPrice()
    }

    fun addProduct(product: Product, quantity: Int){
        totalItems += quantity
        totalPrice+= product.price*quantity

        if(!cartMap.contains(product.id)){
            shoppingList.add(product.id)
        }

        val currentQuantity = cartMap[product.id]?:0
        cartMap[product.id] = currentQuantity + quantity
    }

    fun addProduct(product: ProductVariation, quantity: Int){
        totalItems += quantity
        totalPrice+= product.price*quantity

        if(!cartMap.contains(product.id)){
            shoppingList.add(product.id)
        }

        val currentQuantity = cartMap[product.id]?:0
        cartMap[product.id] = currentQuantity + quantity
    }

    fun getLinesItem(): List<OrderRequest.Product>{
        return cartMap.map {
            OrderRequest.Product(it.key, it.value)
        }
    }

    fun getCouponLinesItem(): List<OrderRequest.CouponLine>{
        val c = coupon
        if(c != null && couponResult != null){
            return listOf(OrderRequest.CouponLine(c.code, c.amount.toString()))
        }
        return emptyList()
    }

    fun updateProduct(product: Product, quantity: Int){
        val currentQuantity = cartMap[product.id]?:return

        val diff = (currentQuantity - quantity)
        totalItems -= diff
        totalPrice -= diff*product.price
        if(quantity == 0)
        {
            cartMap.remove(product.id)
            shoppingList.remove(product.id)
        }
        else{
            cartMap[product.id] =  quantity
        }
    }

    fun updateProduct(productVariation: ProductVariation, quantity: Int){
        val currentQuantity = cartMap[productVariation.id]?:return

        val diff = (currentQuantity - quantity)
        totalItems -= diff
        totalPrice -= diff*productVariation.price
        if(quantity == 0)
        {
            cartMap.remove(productVariation.id)
            shoppingList.remove(productVariation.id)
        }
        else{
            cartMap[productVariation.id] =  quantity
        }
    }

    fun applyCoupon(coupon: Coupon): ApplyCouponResult{
        val expire = coupon.date_expires
        if(expire != null && Date().after(expire)){
            return ApplyCouponResult("Coupon đã hết hạn", 0, false, false)
        }
        val minimum_amount = coupon.minimum_amount
        if(minimum_amount > 0 && totalPrice < minimum_amount){
             return ApplyCouponResult("Đơn hàng của bạn không đủ điều kiện áp dụng coupon", 0, false, false)
        }

        val useage_count = coupon.usage_count
        val useage_limit = coupon.usage_limit
        if(useage_count != null && useage_limit != null && useage_count >= useage_limit){
            return ApplyCouponResult( "Số lượng coupon đã được sử dụng hết.", 0, false, false)
        }

        val usage_limit_per_user = coupon.usage_limit_per_user
        val used_by = coupon.used_by
        val userId = User.mId
        if(usage_limit_per_user != null && userId != null && used_by.isNotEmpty()){
            val strId = userId.toString()
            val times = used_by.count {
                it == strId
            }
            if(times > usage_limit_per_user){
                return ApplyCouponResult( "Bạn đã sử dụng coupon này.", 0, false, false)
            }
        }


        val validProducts = mutableListOf<Product>()
        if(coupon.product_ids.isNotEmpty()){
            val product_ids = coupon.product_ids.toHashSet()
            var hasItems = false

            shoppingList.forEach {
                val product = productsMap[it]
                val variation = variationsMap[it]
                if(variation != null && (product_ids.contains(variation.id) || product_ids.contains(variation.productId))) {
                    hasItems = true
                    val prod = productsMap[variation.productId]
                    if(prod != null)
                        validProducts.add(prod)
                }
                else if(product != null && product_ids.contains(product.id)) {
                    hasItems = true
                    validProducts.add(product)
                }
            }
            if(!hasItems){
                return ApplyCouponResult( "Coupon này không áp dụng cho các sản phẩm trong giỏ hàng", 0, false, false)
            }
        }

        if(coupon.product_categories.isNotEmpty()){
            val product_categories  = coupon.product_categories.toHashSet()
            var hasItems = false

            shoppingList.forEach {
                val product = productsMap[it]
                val variation = variationsMap[it]

                if(product == null && variation!= null){
                    val prod = productsMap[variation.productId]?: return@forEach
                    val cates = prod.categories.orEmpty()
                    cates.forEach {cate->
                        if (product_categories.contains(cate.id)) {

                            hasItems = true
                            validProducts.add(prod)
                        }
                    }
                }
                else if(product != null){
                    val cates = product.categories.orEmpty()
                    cates.forEach {cate->
                        if(product_categories.contains(cate.id)) {
                            hasItems = true
                            validProducts.add(product)
                        }
                    }
                }
            }

            if(!hasItems){
                return ApplyCouponResult( "Coupon này không áp dụng cho các danh mục sản phẩm trong giỏ hàng", 0, false, false)
            }

        }

        val validProductsMap = HashSet<Int>()
        validProducts.forEach {
            validProductsMap.add(it.id)
        }

        var total_valid = 0
        cartMap.forEach {
            val id = it.key
            val quantity = it.value
            val product = productsMap[id]
            val productVariation = variationsMap[id]

            if(productVariation != null){
                val prod = productsMap[productVariation.productId]
                if(prod != null && validProductsMap.contains(prod.id)){
                    total_valid += productVariation.price*quantity
                }
            }
            else if(product != null){
                if(validProductsMap.contains(product.id)){
                    total_valid += product.price*quantity
                }
            }
        }

        var total_discount = 0
        val discount_type = coupon.discount_type
        when(discount_type){
            "fixed_cart" ->{
                total_discount = coupon.amount.toInt()
            }
            "percent" -> {
                total_discount = ((if(validProducts.isEmpty()) totalPrice else total_valid)*coupon.amount*0.01).toInt()
            }
            "fixed_product"->{
                total_discount = coupon.amount.toInt()
            }
        }
        return ApplyCouponResult("", total_discount, true, coupon.free_shipping)
    }

    fun clearAllProducts(){
        shoppingList.clear()
        cartMap.clear()
        totalPrice = 0
        totalItems = 0
    }

    fun reOrder(order: Order){
        clearAllProducts()
        order.orderDetails.forEach {
            val product = productsMap[it.product_id]
            val productVariation = variationsMap[it.variation_id]

            if(productVariation != null)
                addProduct(productVariation, it.quantity)
            else if(product != null)
                addProduct(product, it.quantity)
        }
    }

    fun getOrderStatus(context: Context, status: String): String{
        return when(status){
            "completed" -> context.getString(R.string.order_status_done)
            "pending" -> context.getString(R.string.order_status_pending)
            "cancelled" -> context.getString(R.string.order_status_cancelled)
            else -> context.getString(R.string.order_status_processing)
        }
    }

    fun getOrderStatusDetails(context: Context, status: String): String{
        return when(status){
            "completed" -> context.getString(R.string.order_status_done_detail)
            "pending" -> context.getString(R.string.order_status_pending_detail)
            "cancelled" -> context.getString(R.string.order_status_cancelled_detail)
            else -> context.getString(R.string.order_status_processing_detail)
        }
    }

    fun saveProducts(){
        val listProducts = cartMap.map {
            OrderRequest.Product(it.key, it.value)
        }

        try {
            _Book.write(PRODUCTS,listProducts)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun loadProducts(){
        try {
           val listProducts = _Book.read<List<OrderRequest.Product>>(PRODUCTS)
            clearAllProducts()
            listProducts.forEach {
                val product = productsMap[it.product_id]
                val productVariation = variationsMap[it.product_id]

                if(productVariation != null)
                    addProduct(productVariation, it.quantity)
                else if(product != null)
                    addProduct(product, it.quantity)
            }

        } catch (ex: Exception) {
        }
    }
}