package com.hitasanti.hitaapp.feature.store

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.webtab.WebTabFragment
import com.hitasanti.hitaapp.feature.webtab.WebTabScreen
import com.hitasanti.hitaapp.mvp.BaseMvpFragment
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class StoreFragment : BaseMvpFragment<StoreContract.View, StoreContract.Presenter, StoreViewState, StoreScreen>(),
        HasComponent<StoreComponent>, StoreContract.View {

    companion object {

        fun instance(screen: StoreScreen): StoreFragment {
            val fragment = StoreFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>
    //endregion Dependency

    //region Private
    private var _Adapter: StoreAdapter? = null
    private var _Disposable: CompositeDisposable? = null
    var _WebTabFragment: WebTabFragment? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context?) = component.presenter

    override fun onCreateViewState(context: Context?) = StoreViewState(screen)

    override val viewStateTag: String get() = StoreViewState::class.java.name

    override val layoutResource get() = R.layout.store_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(context!!).component.plus(StoreModule())
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component.inject(this)



        _Disposable = CompositeDisposable()

        if (childFragmentManager.findFragmentById(R.id.web_fl_fragment) == null) {
            val webTabScreen = WebTabScreen(url = "https://www.google.com/maps/place/HITA/@10.7772639,106.6528724,17z/data=!3m1!4b1!4m5!3m4!1s0x31752f4b07193da5:0x8179ab138f79c5c0!8m2!3d10.7772639!4d106.6550611",

                swipeToClose = false,
                showProgress = false)
            val fragment = WebTabFragment.instance(screen = webTabScreen)
            _WebTabFragment = fragment
            val transaction = childFragmentManager.beginTransaction()
            transaction.replace(R.id.web_fl_fragment, fragment)
            transaction.commitAllowingStateLoss()
        }


        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        _Disposable?.dispose()

        super.onDestroyView()
    }

    override fun onResume() {
        super.onResume()
    }
    //endregion Lifecycle

    private inner class OnPageChangeListener : ViewPager.OnPageChangeListener {

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {

        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }
    //endregion Helper
}