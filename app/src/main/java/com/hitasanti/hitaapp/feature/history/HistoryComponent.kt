package com.hitasanti.hitaapp.feature.history

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [HistoryModule::class])
interface HistoryComponent {

    val presenter: HistoryContract.Presenter

    fun inject(activity: HistoryActivity)
}
