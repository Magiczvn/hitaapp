package com.hitasanti.hitaapp.feature.news

import android.content.Context
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.mvp.Presenter

interface NewsContract {

    interface View {
        fun showItems(items: List<Item>)

        fun notifyNewsLoaded()
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, NewsViewState> {

    }
}