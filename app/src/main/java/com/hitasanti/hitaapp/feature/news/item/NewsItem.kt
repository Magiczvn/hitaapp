package com.hitasanti.hitaapp.feature.news.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item

class NewsItem(val articles: List<ArticleItem>, val title: String): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is NewsItem && (other === this || (other.articles == articles))

}