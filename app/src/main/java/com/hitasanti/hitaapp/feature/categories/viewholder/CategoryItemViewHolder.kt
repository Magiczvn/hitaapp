package com.hitasanti.hitaapp.feature.categories.viewholder

import android.content.res.ColorStateList
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.categories.SubProductAdapter
import com.hitasanti.hitaapp.feature.categories.item.CategoryDetails
import com.hitasanti.hitaapp.feature.categories.item.CategoryItem
import com.hitasanti.hitaapp.feature.news.event.HeaderClickedEvent
import com.hitasanti.hitaapp.view.BaseRecyclerView
import com.hitasanti.hitaapp.view.ViewUtil
import io.reactivex.subjects.Subject
import kotterknife.bindView

class CategoryItemViewHolder (parent: ViewGroup,
                              resId: Int,
                              private val _EventSubject: Subject<Any>,
                              private val _Glide: RequestManager) : BaseItemViewHolder<CategoryItem>(parent, resId) {

    private val _RecyclerView: BaseRecyclerView by bindView(R.id.categories_rv)
    private val _RootView: ViewGroup by bindView(R.id.category_root)
    private val _HeaderView: ViewGroup by bindView(R.id.category_header)
    private val _IconView: ImageView by bindView(R.id.category_icon)
    private val _NameView: TextView by bindView(R.id.category_name)
    private val _EmptyView: TextView by bindView(R.id.txtComingSoon)
    private val _ArrowView: View by bindView(R.id.categories_arrow)



    private var _Adapter = SubProductAdapter(_Glide, _EventSubject)
    init {
        _RecyclerView.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        _RecyclerView.adapter = _Adapter
        _Adapter.setReady(true)
        _RootView.setOnClickListener {
            onItemClick()
        }
        _ArrowView.setOnClickListener {
            onItemClick()
        }

    }

    private fun onItemClick(){
        val id = item?.categoryDetails?.categoryId?:return
        _EventSubject.onNext(HeaderClickedEvent(id))
    }

    override fun onBindItem(item: CategoryItem) {
        val oldItem = this.item

        if (oldItem == null || oldItem.products != item.products) {
            _EmptyView.visibility = if(item.products.isEmpty()) View.VISIBLE else View.GONE
            _Adapter.updateItems(item.products.take(10))
        }

        if(oldItem == null || oldItem.categoryDetails.colorId!= item.categoryDetails.colorId){
            val colorTint = ColorStateList.valueOf(
                ContextCompat.getColor(itemView.context, item.categoryDetails.colorId)
            )
            _RootView.backgroundTintList = colorTint
            _HeaderView.backgroundTintList = colorTint
        }

        if(oldItem == null || oldItem.categoryDetails.iconId!= item.categoryDetails.iconId){
            _Glide.load(item.categoryDetails.iconId)
                .into(_IconView)
        }

        if(oldItem == null || oldItem.categoryDetails.title!= item.categoryDetails.title){
          _NameView.setText(item.categoryDetails.title)
        }


        super.onBindItem(item)
    }
}