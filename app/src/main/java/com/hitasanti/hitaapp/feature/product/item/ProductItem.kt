package com.hitasanti.hitaapp.feature.product.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.news.item.ArticleItem

class ProductItem (val id: Int, val name:String, val price: Int, val regularPrice: Int, val description: String, val imageURL: String, val onSale: Boolean): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is ProductItem && (other === this || other.id == id)
}