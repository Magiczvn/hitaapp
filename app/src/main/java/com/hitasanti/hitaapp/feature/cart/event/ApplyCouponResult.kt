package com.hitasanti.hitaapp.feature.cart.event

class ApplyCouponResult(val message: String?, val amount: Int, val result: Boolean, val freeShip: Boolean)