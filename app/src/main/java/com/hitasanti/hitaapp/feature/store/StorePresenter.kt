package com.hitasanti.hitaapp.feature.store

import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler


class StorePresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<StoreContract.View, StoreViewState>(), StoreContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }

    //endregion Private

    //region BasePresenter
    override fun onAttachView(view: StoreContract.View) {
        super.onAttachView(view)

    }

    override fun onDestroy() {
        super.onDestroy()

    }
    //endregion BasePresenter

}