package com.hitasanti.hitaapp.feature.news.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item

class HeaderItem(): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is HeaderItem

}