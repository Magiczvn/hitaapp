package com.hitasanti.hitaapp.feature.web

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import com.epi.feature.webtab.event.GoBackEvent
import com.hitasanti.hitaapp.feature.webtab.event.WebTabFragmentEvent
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.main.MainActivity
import com.hitasanti.hitaapp.feature.webtab.WebTabFragment
import com.hitasanti.hitaapp.feature.webtab.WebTabScreen
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import com.hitasanti.hitaapp.view.ViewUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.webtab_fragment.*
import javax.inject.Inject


class WebActivity : BaseSwipeMvpActivity<WebContract.View, WebContract.Presenter, WebViewState, WebScreen>(),
        HasComponent<WebComponent>, WebContract.View {
    companion object {

        fun instance(context: Context, screen: WebScreen): Intent {
            if (isWebViewExists()) {
                val intent = Intent(context, WebActivity::class.java)
                setScreen(intent, screen)
                return intent
            } else {
                return Intent(Intent.ACTION_VIEW, Uri.parse(screen.url))
            }
        }

        private fun isWebViewExists(): Boolean {
            try {
                Class.forName("android.webkit.WebView")
                return true
            } catch (ex: ClassNotFoundException) {
            }

            return false
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus

    //endregion Dependency

    //region Private
    private var _Disposable: CompositeDisposable? = null

    private var _IsActivityTranslucent = true
    //endregion Private

    var _WebTabFragment: WebTabFragment? = null

    var systemWindowInsetTop: Int = 0

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = WebViewState()

    override val viewStateTag: String get() = WebViewState::class.java.name

    override val layoutResource get() = R.layout.web_activity

    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(WebModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        enableSwipe(!isTaskRoot)

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
            enableConvertTranslucent(true)
        }

        _Disposable = CompositeDisposable(
                _Bus.register(GoBackEvent::class.java)
                        .filter { it.parent == this }
                        .observeOn(_SchedulerFactory.main())
                        .subscribe(Consumer {
                            finish()
                        }, ErrorConsumer()),
                _Bus.register(WebTabFragmentEvent::class.java)
                        .filter { it.url == screen.url }
                        .observeOn(_SchedulerFactory.main())
                        .subscribe(Consumer {
                            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                                if (it.type == WebTabFragmentEvent.Type.SCREEN_LANSCAPE) {
                                    ViewUtil.convertActivityFromTranslucent(this)
                                    handleWebTabFragmentEvent(it)
                                } else {
                                    Handler().postDelayed({
                                        if (!isDestroyed) {
                                            ViewUtil.convertActivityFromTranslucent(this)
                                            handleWebTabFragmentEvent(it)
                                        }
                                    }, 500)
                                }
                            } else {
                                handleWebTabFragmentEvent(it)
                            }
                        }, ErrorConsumer())
        )

        if (supportFragmentManager.findFragmentById(R.id.web_fl_fragment) == null) {
            val webTabScreen = WebTabScreen(url = screen.url, // "https://dev-maubinh.zingplay.me/?action=static",

                    swipeToClose = screen.swipeToClose)
            val fragment = WebTabFragment.instance(screen = webTabScreen)
            _WebTabFragment = fragment
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.web_fl_fragment, fragment)
            transaction.commitAllowingStateLoss()
        }

        if (!screen.swipeToClose) {
            enableSwipe(false)
        }

    }

    override fun convertTranslucent(isTranslucent: Boolean, enableOrientation: Boolean) {
        if (Build.VERSION.SDK_INT != 26) {
            return
        }
        if (isTranslucent && !_IsActivityTranslucent) {
            ViewUtil.convertActivityToTranslucent(this)
            _IsActivityTranslucent = true
        } else if (!isTranslucent) {
            ViewUtil.convertActivityFromTranslucent(this)
            _IsActivityTranslucent = false
        }
    }

    override fun onBackPressed() {
        if (_WebTabFragment?.webview_wv?.canGoBack() == true) {
            _WebTabFragment?.webview_wv?.goBack()

        } else {
            super.onBackPressed()
        }
    }

    fun goFirstPage() {
        val history = _WebTabFragment?.webview_wv?.copyBackForwardList() ?: return
        var index = -1
        var url: String? = null

        while (_WebTabFragment?.webview_wv?.canGoBackOrForward(index) ?: false) {
            if (history.getItemAtIndex(history.getCurrentIndex() + index).getUrl() != "about:blank") {
                _WebTabFragment?.webview_wv?.goBackOrForward(index)
                url = history.getItemAtIndex(-index).getUrl()
                break
            }
            index--

        }
        // no history found that is not empty
        if (url == null) {
            finish()
        }
    }

    override fun onDestroy() {
        _Disposable?.dispose()

        super.onDestroy()
    }


    override fun finish() {
        super.finish()
        if (isTaskRoot) {
            val intent = MainActivity.instance(context = this,
                    showWelcomeAds = false,
                    showSplash = false,
                    forceLogin = false,
                    isReloading = false)
            startActivity(intent)
        }
    }
    //endregion Lifecycle

    private fun handleWebTabFragmentEvent(event: WebTabFragmentEvent) {
        if (event.type == WebTabFragmentEvent.Type.SCREEN_LANSCAPE) {
            if (requestedOrientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE


            }
        } else if (event.type == WebTabFragmentEvent.Type.SCREEN_PORTRAIT) {
            if (requestedOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

            }
        } else if (event.type == WebTabFragmentEvent.Type.GO_FIRST_PAGE) {
            goFirstPage()
        }
    }

    //endregion Handle Click
}