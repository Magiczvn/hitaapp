package com.hitasanti.hitaapp.feature.categories

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.repository.model.Product

interface CategoriesContract {

    interface View {
        fun showItems(items: List<Item>)

        fun showProductDialog(product: Product)
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, CategoriesViewState> {
        fun startLoading()

        //fun getProductVariation(productId: Int)
    }
}