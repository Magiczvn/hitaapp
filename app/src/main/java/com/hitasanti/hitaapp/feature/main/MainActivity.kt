package com.hitasanti.hitaapp.feature.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.florent37.viewanimator.ViewAnimator

import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.event.BackPressEvent
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.home.HomeFragment
import com.hitasanti.hitaapp.feature.home.HomeScreen
import com.hitasanti.hitaapp.mvp.BaseMvpActivity
import dagger.Lazy
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.main_activity.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MainActivity : BaseMvpActivity<MainContract.View, MainContract.Presenter, MainViewState, MainScreen>(),
    HasComponent<MainComponent>, MainContract.View {

    companion object {

        const val DURATION_ANIMATE: Long = 300L
        const val SELF_OPEN = "selfOpen"
        const val SHOW_SPLASH = "showSplash"
        const val FORCE_LOGIN = "forceLogin"


        fun instance(context: Context, showWelcomeAds: Boolean, showSplash: Boolean, forceLogin: Boolean, isReloading: Boolean): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(SELF_OPEN, true)
            intent.putExtra(SHOW_SPLASH, showSplash)
            intent.putExtra(FORCE_LOGIN, forceLogin)
            return intent
        }
    }

    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>

    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context): MainViewState {
        val forceLogin = intent.extras?.getBoolean(FORCE_LOGIN, true) ?: true
        return MainViewState(forceLogin)
    }

    override val viewStateTag: String get() = MainViewState::class.java.name

    override val layoutResource get() = com.hitasanti.hitaapp.R.layout.main_activity

    val showSplash: Boolean by lazy { intent.extras?.getBoolean(SHOW_SPLASH, true) ?: true }
    var _IsSpashHided = false

    private var _Disposable: CompositeDisposable? = null
    private var _AdsTimeoutDisposable: Disposable? = null
    private var _IsQuit = false

    override val component by lazy {
        HitaApplication.get(this).component.plus(MainModule(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        if (intent.action == Intent.ACTION_MAIN && !isTaskRoot) {
            finish()
        }


        if (savedInstanceState == null) {
             if (!showSplash) {
                hideSplash()
            }
        } else {
            hideSplash()
        }

        _Disposable = CompositeDisposable(_Bus.register(BackPressEvent::class.java)
            .filter { it.sender !== this }
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                when {
                    _IsQuit -> {
                        onAppExit()
                        finish()
                        overridePendingTransition(0, 0)
                    }
                    else -> {
                        Toast.makeText(this, R.string.msgBackToExit, Toast.LENGTH_SHORT).show()
                        _IsQuit = true
                        Observable.timer(2000, TimeUnit.MILLISECONDS)
                            .subscribe { _ -> _IsQuit = false }
                    }
                }
            }, ErrorConsumer()))
        scheduleHideSplash(2000L)

        val homeScreen = HomeScreen()
        val homeFragment = HomeFragment.instance(homeScreen)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, homeFragment)
        transaction.commitAllowingStateLoss()
    }

    private fun scheduleHideSplash(millisec: Long) {
        _AdsTimeoutDisposable?.dispose()
        _AdsTimeoutDisposable = Observable.timer(millisec, TimeUnit.MILLISECONDS)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                hideSplash()
            }, ErrorConsumer())
    }

    private fun hideSplash() {
        if (main_iv_logo?.parent == null) return

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE

        ViewAnimator.animate(main_iv_logo)
            .fadeOut()
            .duration(DURATION_ANIMATE)
            .onStop {
                main_iv_logo?.let {
                    (it.parent as? ViewGroup)?.removeView(it)
                }

                presenter.isShowedSplashScreen = true
            }
            .andAnimate(main_container)
            .fadeIn()
            .duration(DURATION_ANIMATE)
            .start()

        _IsSpashHided = true
    }

    override fun onBackPressed() {
        _Bus.post(BackPressEvent(this))
    }

    override fun onDestroy() {
        _Disposable?.dispose()
        super.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        hideSplash()

    }
}