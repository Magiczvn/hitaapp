package com.hitasanti.hitaapp.feature.web

import android.app.Activity
import com.hitasanti.hitaapp.dependency.ActivityScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides


@Module
class WebModule(private val _Activity: Activity) {

    @Provides
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>): WebContract.Presenter = WebPresenter(useCaseFactory, schedulerFactory)
}
