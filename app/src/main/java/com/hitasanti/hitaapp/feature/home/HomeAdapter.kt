package com.hitasanti.hitaapp.feature.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.hitasanti.hitaapp.app.adapter.viewpager.BetterFragmentStatePagerAdapter
import com.hitasanti.hitaapp.feature.categories.CategoriesFragment
import com.hitasanti.hitaapp.feature.categories.CategoriesScreen
import com.hitasanti.hitaapp.feature.news.NewsFragment
import com.hitasanti.hitaapp.feature.news.NewsScreen
import com.hitasanti.hitaapp.feature.order.OrderActivity
import com.hitasanti.hitaapp.feature.order.OrderScreen
import com.hitasanti.hitaapp.feature.setting.SettingFragment
import com.hitasanti.hitaapp.feature.setting.SettingScreen
import com.hitasanti.hitaapp.feature.store.StoreFragment
import com.hitasanti.hitaapp.feature.store.StoreScreen
import com.hitasanti.hitaapp.mvp.Screen

class HomeAdapter(fm: FragmentManager,
                  private var _Screens: List<Screen>)
    : BetterFragmentStatePagerAdapter(fm) {

    private val TAB_NEWS = "tab_news"
    private val TAB_ORDER = "tab_order"
    private val TAB_STORE = "tab_store"
    private val TAB_SETTING = "tab_setting"

    private var _OldScreens = emptyList<Screen>()


    fun getScreen(position: Int) = when {
        position < 0 -> null
        position > _Screens.size - 1 -> null
        else -> _Screens[position]
    }

    fun getScreens() = _Screens

    fun setScreens(screens: List<Screen>) {
        _OldScreens = _Screens
        _Screens = screens
        notifyDataSetChanged()
    }

    fun getSavedStates(): Collection<Fragment.SavedState> = mSavedState.values

    override fun getItem(position: Int): Fragment {
        val screen = _Screens[position]
        return when (screen) {
            is NewsScreen -> NewsFragment.instance(screen)
            is CategoriesScreen -> CategoriesFragment.instance(screen)
            is StoreScreen -> StoreFragment.instance(screen)
            is SettingScreen -> SettingFragment.instance(screen)
            else -> throw RuntimeException("Not support screen type $screen")
        }
    }

    override fun getItemPosition(`object`: Any): Int {
        val state = when (`object`) {
            is NewsFragment -> {
                if (_OldScreens.indexOf(`object`.screen) == _Screens.indexOf(`object`.screen)) {
                    POSITION_UNCHANGED
                } else {
                    POSITION_NONE
                }
            }
            is CategoriesFragment -> {
                if (_OldScreens.indexOf(`object`.screen) == _Screens.indexOf(`object`.screen)) {
                    POSITION_UNCHANGED
                } else {
                    POSITION_NONE
                }
            }
            is SettingFragment -> {
                if (_OldScreens.indexOf(`object`.screen) == _Screens.indexOf(`object`.screen)) {
                    POSITION_UNCHANGED
                } else {
                    POSITION_NONE
                }
            }
            is StoreFragment -> {
                if (_OldScreens.indexOf(`object`.screen) == _Screens.indexOf(`object`.screen)) {
                    POSITION_UNCHANGED
                } else {
                    POSITION_NONE
                }
            }
            else -> POSITION_NONE
        }
        return state
    }

    override fun getId(position: Int, fragment: Fragment?): String {
        val id = when (fragment) {
            is NewsFragment -> TAB_NEWS
            is CategoriesFragment -> TAB_ORDER
            is SettingFragment -> TAB_SETTING
            is StoreFragment -> TAB_STORE
            else -> {
                val screen = _Screens[position]
                when (screen) {
                    is NewsScreen -> TAB_NEWS
                    is CategoriesScreen -> TAB_ORDER
                    is StoreScreen -> TAB_STORE
                    is SettingScreen -> TAB_SETTING
                    else -> throw RuntimeException("Not support screen type $screen")
                }
            }
        }
        return id
    }

    override fun getCount() = _Screens.size
}