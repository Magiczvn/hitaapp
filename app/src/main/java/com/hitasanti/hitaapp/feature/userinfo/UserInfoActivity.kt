package com.hitasanti.hitaapp.feature.userinfo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.editinfo.EditInfoActivity
import com.hitasanti.hitaapp.feature.editinfo.EditInfoScreen
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.feature.userinfo.event.UpdateInfoSuccessEvent
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.setting_fragment.*
import kotlinx.android.synthetic.main.userinfo_activity.*
import kotlinx.android.synthetic.main.userinfo_activity.avatar
import kotlinx.android.synthetic.main.userinfo_activity.txtUsername
import javax.inject.Inject


class UserInfoActivity : BaseSwipeMvpActivity<UserInfoContract.View, UserInfoContract.Presenter, UserInfoViewState, UserInfoScreen>(),
    HasComponent<UserInfoComponent>, UserInfoContract.View {
    companion object {

        fun instance(context: Context, screen: UserInfoScreen): Intent {
            val intent = Intent(context, UserInfoActivity::class.java)
            setScreen(intent, screen)
            return intent
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus

    //endregion Dependency

    //region Private
    private var _Disposable: CompositeDisposable? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = UserInfoViewState(screen)

    override val viewStateTag: String get() = UserInfoViewState::class.java.name

    override val layoutResource get() = R.layout.userinfo_activity

    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(UserInfoModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
        enableSwipe(true)

        _Disposable = CompositeDisposable(_Bus.register(UpdateInfoSuccessEvent::class.java)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                showInfo()
            }, ErrorConsumer()))


        showInfo()
        val avatarUrl = User.getAvatarUrl()
        if(avatarUrl == null) {
            GlideApp.with(this)
                .load(R.drawable.ic_avatar)
                .dontAnimate()
                .into(avatar)
        }
        else{
            GlideApp.with(this)
                .load(avatarUrl)
                .dontAnimate()
                .into(avatar)
        }

        btnChange.setOnClickListener {
            val intent = EditInfoActivity.instance(this, EditInfoScreen())
            startActivity(intent)
        }

    }

    private fun showInfo(){
        val name = User.getUserName()

        txtUsername.text = name
        txtName.text = name

        txtAddress.text = if(!User.mUserInfo?.shippingAddress.isNullOrEmpty()) User.mUserInfo?.shippingAddress else getString(R.string.txtNoInfo)
        txtPhone.text = if(!User.mUserInfo?.phone.isNullOrEmpty()) User.mUserInfo?.phone else getString(R.string.txtNoInfo)
        txtEmail.text = if(!User.mEmail.isNullOrEmpty()) User.mEmail else getString(R.string.txtNoInfo)
    }


    override fun onDestroy() {
        _Disposable?.dispose()

        super.onDestroy()
    }
}