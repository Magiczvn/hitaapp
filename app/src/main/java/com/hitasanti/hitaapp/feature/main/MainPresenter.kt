package com.hitasanti.hitaapp.feature.main

import android.app.Application
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler

class MainPresenter(private val _Application: Lazy<Application>,
                    private val _UseCaseFactory: Lazy<UseCaseFactory>,
                    private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<MainContract.View, MainViewState>(), MainContract.Presenter {

    //region Private
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }

    //endregion Private

    //region BasePresenter
    override fun onCreate(viewState: MainViewState) {
        super.onCreate(viewState)

    }

    override fun onAttachView(view: MainContract.View) {
        super.onAttachView(view)


    }

    override fun onDestroy() {
        super.onDestroy()
    }
    //endregion BasePresenter

    //region MainContract.Presenter
    override var isShowedSplashScreen: Boolean
        get() = mViewState.isShowedSplashScreen
        set(value) {
            mViewState.isShowedSplashScreen = value
        }

}

