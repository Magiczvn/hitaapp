package com.hitasanti.hitaapp.feature.home

import com.hitasanti.hitaapp.dependency.FragmentScope

import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [HomeModule::class])
interface HomeComponent {

    val presenter: HomeContract.Presenter

    fun inject(fragment: HomeFragment)
}
