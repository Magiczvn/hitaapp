package com.hitasanti.hitaapp.feature.editinfo

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.feature.userinfo.event.UpdateInfoSuccessEvent
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import com.hitasanti.hitaapp.view.NumberUtil
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.editinfo_activity.*
import javax.inject.Inject


class EditInfoActivity : BaseSwipeMvpActivity<EditInfoContract.View, EditInfoContract.Presenter, EditInfoViewState, EditInfoScreen>(),
    HasComponent<EditInfoComponent>, EditInfoContract.View {
    companion object {

        fun instance(context: Context, screen: EditInfoScreen): Intent {
            val intent = Intent(context, EditInfoActivity::class.java)
            setScreen(intent, screen)
            return intent
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus

    //endregion Dependency

    //region Private
    private var _Disposable: CompositeDisposable? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = EditInfoViewState(screen)

    override val viewStateTag: String get() = EditInfoViewState::class.java.name

    override val layoutResource get() = R.layout.editinfo_activity

    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(EditInfoModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
        enableSwipe(true)

        _Disposable = CompositeDisposable()

        val name = User.getUserName()
        val index = name.indexOfFirst {
            it == ' '
        }

        val last_name = if(index == -1) "" else name.substring(0, index)
        val first_name = if(index == -1) "" else name.substring(index + 1, name.length)

        txt_first_name.setText(first_name)
        txt_last_name.setText(last_name)
        txt_address.setText(User.mUserInfo?.shippingAddress.orEmpty())
        txt_phone.setText(User.mUserInfo?.phone.orEmpty())

        btnSave.setOnClickListener {
            saveInfo()
        }

        setupUI(root_view)

    }

    override fun showResult(result: Boolean) {
        txtSaveInfo.visibility = View.VISIBLE
        loading_pv.visibility = View.INVISIBLE
        if(result){
            val toast = Toast.makeText(
                applicationContext,
                "Cập nhật thông tin thành công!",
                Toast.LENGTH_LONG
            )
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
            _Bus.post(UpdateInfoSuccessEvent())
            finish()
        }
        else{
            val toast = Toast.makeText(
                applicationContext,
                "Có lỗi khi cập nhật thông tin!",
                Toast.LENGTH_LONG
            )
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
        }

    }

    fun hideSoftKeyboard() {
        val inputMethodManager: InputMethodManager = getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            currentFocus?.windowToken, 0
        )
    }

    fun setupUI(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { v, event ->
                hideSoftKeyboard()
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView)
            }
        }
    }

    private fun saveInfo(){

        if(!NumberUtil.isValidPhoneNumber(txt_phone.text.toString())){
            val toast = Toast.makeText(this, "Số điện thoại không hợp lệ", Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.CENTER,0,0)
            toast.show()
            return
        }

        txtSaveInfo.visibility = View.INVISIBLE
        loading_pv.visibility = View.VISIBLE

        presenter.updateInfo(txt_first_name.text.toString(), txt_last_name.text.toString(), txt_address.text.toString(), txt_phone.text.toString())
    }


    override fun onDestroy() {
        _Disposable?.dispose()

        super.onDestroy()
    }
}