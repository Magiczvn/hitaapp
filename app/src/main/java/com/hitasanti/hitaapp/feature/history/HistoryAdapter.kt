package com.hitasanti.hitaapp.feature.history


import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemAdapter
import com.hitasanti.hitaapp.app.adapter.viewpager.BetterFragmentStatePagerAdapter
import com.hitasanti.hitaapp.feature.history.item.OrderHistoryItem
import com.hitasanti.hitaapp.feature.history.viewholder.HistoryItemViewHolder
import com.hitasanti.hitaapp.feature.product.ProductFragment
import com.hitasanti.hitaapp.feature.product.ProductScreen
import com.hitasanti.hitaapp.feature.product.item.ProductItem
import com.hitasanti.hitaapp.feature.product.viewholder.ProductItemViewHolder
import com.hitasanti.hitaapp.mvp.Screen


class HistoryAdapter(private val _Glide: RequestManager) : BaseItemAdapter() {

    companion object {
        private const val ITEM_HISTORY = 1
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItemAt(position)
        return when (item) {
            is OrderHistoryItem -> ITEM_HISTORY

            else -> throw RuntimeException("Not support item $item")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        ITEM_HISTORY -> HistoryItemViewHolder(parent, R.layout.order_history_viewholder, event, _Glide)
        else -> throw RuntimeException("Not support type $viewType")
    }

}