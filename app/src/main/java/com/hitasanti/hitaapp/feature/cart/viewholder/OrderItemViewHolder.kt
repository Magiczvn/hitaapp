package com.hitasanti.hitaapp.feature.cart.viewholder


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.event.CartItemClickEvent
import com.hitasanti.hitaapp.feature.cart.item.CartItem
import com.hitasanti.hitaapp.view.BillingItemView
import com.hitasanti.hitaapp.view.OrderItemView
import io.reactivex.subjects.Subject
import kotterknife.bindView

class OrderItemViewHolder(parent: ViewGroup,
                            resId: Int,
                            private val _EventSubject: Subject<Any>) :BaseItemViewHolder<CartItem>(parent, resId), OrderItemView.OrderListener {

    private val _Container: ViewGroup by bindView(R.id.order_container)

    init {

    }

    override fun onOrderClick(productId: Int, variationId: Int?) {
        _EventSubject.onNext(CartItemClickEvent(productId, variationId))
    }

    override fun onBindItem(item: CartItem) {

        val layoutInflater: LayoutInflater = LayoutInflater.from(itemView.context)

        _Container.removeAllViews()
        item.orders.forEachIndexed { index, orderItem ->
            val view = layoutInflater.inflate(R.layout.order_item_layout, _Container, false) as OrderItemView
            view.setName(orderItem.name)
            view.setPrice(orderItem.subTotal?:orderItem.price*orderItem.quantity)
            view.setProductId(orderItem.id)
            view.setVariationId(orderItem.variationId)
            view.setQuantity(orderItem.quantity)
            view.setLastItem(index == item.orders.size - 1)
            view.setOrderListenter(this)

            _Container.addView(view)
        }

        val view = layoutInflater.inflate(R.layout.billing_item_layout, _Container, false) as BillingItemView
        view.setShippingPrice(item.shippingPrice)
        view.setTempPrice(item.tempPrice)
        view.setTotalPrice(item.tempPrice + item.shippingPrice - item.disCountPrice)
        if(item.disCountPrice > 0)
            view.setDiscountPrice(item.disCountPrice)

        _Container.addView(view)

        super.onBindItem(item)
    }
}