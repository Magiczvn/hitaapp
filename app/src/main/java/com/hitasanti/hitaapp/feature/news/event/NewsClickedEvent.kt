package com.hitasanti.hitaapp.feature.news.event

class NewsClickedEvent(val viewHolder: Any, val link: String)