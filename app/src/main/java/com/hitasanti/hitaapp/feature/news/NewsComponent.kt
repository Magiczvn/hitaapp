package com.hitasanti.hitaapp.feature.news

import com.hitasanti.hitaapp.dependency.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [NewsModule::class])
interface NewsComponent {

    val presenter: NewsContract.Presenter

    fun inject(fragment: NewsFragment)
}
