package com.hitasanti.hitaapp.feature.news.viewholder

import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.news.ArticlesAdapter
import com.hitasanti.hitaapp.feature.news.item.NewsItem
import com.hitasanti.hitaapp.view.BaseRecyclerView
import com.hitasanti.hitaapp.view.ViewUtil
import io.reactivex.subjects.Subject
import kotlinx.android.synthetic.main.news_fragment.*
import kotterknife.bindView

class NewsItemViewHolder(parent: ViewGroup,
                            resId: Int,
                            eventSubject: Subject<Any>,
                            glide: RequestManager)
    : BaseItemViewHolder<NewsItem>(parent, resId) {

    private val _RecyclerView: BaseRecyclerView by bindView(R.id.news_rv)
    private val _Title: TextView by bindView(R.id.news_tv)

    private var _Adapter = ArticlesAdapter(glide, eventSubject)

    init {
        _RecyclerView.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        _RecyclerView.adapter = _Adapter
        _RecyclerView.setHasFixedSize(true)
        _RecyclerView.setItemViewCacheSize(10)
        _Adapter.setReady(true)
    }

    override fun onBindItem(item: NewsItem) {
        val oldItem = this.item

        if (oldItem == null || oldItem.articles != item.articles) {
            _Adapter.updateItems(item.articles)
        }

        if (oldItem == null || oldItem.title != item.title) {
            _Title.text = item.title
        }

        super.onBindItem(item)
    }
}
