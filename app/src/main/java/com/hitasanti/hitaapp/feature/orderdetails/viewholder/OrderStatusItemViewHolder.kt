package com.hitasanti.hitaapp.feature.orderdetails.viewholder


import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.item.StatusItem
import com.hitasanti.hitaapp.feature.orderdetails.event.CancelOrderButtonClickedEvent
import com.hitasanti.hitaapp.feature.orderdetails.event.ReorderButtonClickedEvent
import io.reactivex.subjects.Subject
import kotterknife.bindView

class OrderStatusItemViewHolder(parent: ViewGroup,
                                   resId: Int,
                                   private val _EventSubject: Subject<Any>) :BaseItemViewHolder<StatusItem>(parent, resId) {

    private val _Status: TextView by bindView(R.id.txtStatus)
    private val _StatusDetail: TextView by bindView(R.id.txtStatusDetail)
    private val _StatusReason: TextView by bindView(R.id.txtCancelReason)

    private val _Reorder: TextView by bindView(R.id.btn_reorder)
    private val _CancelOrder: TextView by bindView(R.id.btn_cancel_order)

    init {
        _Reorder.setOnClickListener {
            _EventSubject.onNext(ReorderButtonClickedEvent())
        }

        _CancelOrder.setOnClickListener {
            _EventSubject.onNext(CancelOrderButtonClickedEvent())
        }
    }

    override fun onBindItem(item: StatusItem) {
        _Status.text = Cart.getOrderStatus(itemView.context, item.status)
        _StatusDetail.text = Cart.getOrderStatusDetails(itemView.context, item.status)

         if(item.status == "cancelled"){
             _StatusReason.visibility = View.VISIBLE
             _StatusReason.text = itemView.context.getString(R.string.txt_reason, item.reason)
         }
        else{
             _StatusReason.visibility = View.GONE
         }

        _CancelOrder.visibility = if(item.status  == "pending" || item.status == "processing") View.VISIBLE else View.GONE

        super.onBindItem(item)
    }
}