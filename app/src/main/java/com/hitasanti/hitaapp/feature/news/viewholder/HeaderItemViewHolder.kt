package com.hitasanti.hitaapp.feature.news.viewholder

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.text.HtmlCompat
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.categories.item.CategoryDetails
import com.hitasanti.hitaapp.feature.news.event.HeaderClickedEvent

import com.hitasanti.hitaapp.feature.news.item.HeaderItem
import io.reactivex.subjects.Subject
import kotterknife.bindView

class HeaderItemViewHolder(parent: ViewGroup,
                            resId: Int,
                            private val _EventSubject: Subject<Any>) :BaseItemViewHolder<HeaderItem>(parent, resId) {

    private val _NhangItem: View by bindView(R.id.hita_nhang)
    private val _CoffeeItem: View by bindView(R.id.hita_coffee)
    //private val _ChayItem: View by bindView(R.id.hita_chay)
    private val _TeaItem: View by bindView(R.id.hita_tea)
    //private val _FarmItem: View by bindView(R.id.hita_farm)
    private val _AboutItem: View by bindView(R.id.hita_other)

    init {
        _NhangItem.setOnClickListener {
            _EventSubject.onNext(HeaderClickedEvent(CategoryDetails.HitaNhangCategory.categoryId))
        }

        _CoffeeItem.setOnClickListener {
            _EventSubject.onNext(HeaderClickedEvent(CategoryDetails.HitaCofeeCategory.categoryId))
        }

        /*_ChayItem.setOnClickListener {
            _EventSubject.onNext(HeaderClickedEvent(CategoryDetails.HitaChayCategory.categoryId))
        }*/

        _TeaItem.setOnClickListener {
            _EventSubject.onNext(HeaderClickedEvent(CategoryDetails.HitaTeaCategory.categoryId))
        }

        /*_FarmItem.setOnClickListener {
            _EventSubject.onNext(HeaderClickedEvent(CategoryDetails.HitaFarmCategory.categoryId))
        }*/

        _AboutItem.setOnClickListener {
            _EventSubject.onNext(HeaderClickedEvent(null))
        }




    }

    override fun onBindItem(item: HeaderItem) {
        val oldItem = this.item


        super.onBindItem(item)
    }
}