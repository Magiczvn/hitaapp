package com.hitasanti.hitaapp.feature.webtab


import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class WebTabModule {

    @Provides
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>): WebTabContract.Presenter = WebTabPresenter(useCaseFactory, schedulerFactory)
}
