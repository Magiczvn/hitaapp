package com.hitasanti.hitaapp.feature.productdialog


import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen
import com.hitasanti.hitaapp.repository.model.ProductVariation

class ProductDialogScreen(val id: Int, val name: String, val price:Int, val shortDescription: String, val imageUrl: String, val variationId: Int?, val quantity:Int  = 1, val canRemove:Boolean = false) : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(id)
        dest?.writeString(name)
        dest?.writeInt(price)
        dest?.writeString(shortDescription)
        dest?.writeString(imageUrl)
        dest?.writeInt(variationId?:0)
        dest?.writeInt(quantity)
        dest?.writeInt(if(canRemove) 1 else 0)
    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is ProductDialogScreen && (other === this || (other.id == id &&
            other.name == name &&
            other.price == price &&
            other.shortDescription == shortDescription &&
            other.price == price &&
            other.variationId == variationId &&
            other.quantity == quantity &&
            other.canRemove == canRemove))

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ProductDialogScreen> = object : Parcelable.Creator<ProductDialogScreen> {
            override fun createFromParcel(parcel: Parcel): ProductDialogScreen {
                val id = parcel.readInt()
                val name = parcel.readString().orEmpty()
                val price = parcel.readInt()
                val shortDescription = parcel.readString().orEmpty()
                val imageUrl = parcel.readString().orEmpty()
                val variationId = parcel.readInt()
                val quantity = parcel.readInt()
                val canRemove = parcel.readInt() == 1

                return ProductDialogScreen(id, name, price, shortDescription, imageUrl, if(variationId == 0) null else variationId, quantity, canRemove)
            }

            override fun newArray(size: Int) = arrayOfNulls<ProductDialogScreen?>(size)
        }
    }
}