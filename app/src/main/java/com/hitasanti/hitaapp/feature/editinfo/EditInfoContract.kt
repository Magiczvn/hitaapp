package com.hitasanti.hitaapp.feature.editinfo

interface EditInfoContract {

    interface View {
        fun showResult(result: Boolean)
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, EditInfoViewState> {
        fun updateInfo(first_name: String, last_name: String, address: String, phone: String)
    }
}