package com.hitasanti.hitaapp.feature.product

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class ProductScreen(val categoryId: Int, val title: String)
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(categoryId)
        dest?.writeString(title)
    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is ProductScreen &&  (other === this || (other.categoryId == categoryId &&
            other.title == title))

    override fun toString() = "ProductScreen ${categoryId}"

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ProductScreen> = object : Parcelable.Creator<ProductScreen> {
            override fun createFromParcel(parcel: Parcel): ProductScreen {
                val categoryId = parcel.readInt()
                val title = parcel.readString() ?: ""

                return ProductScreen(categoryId, title)
            }

            override fun newArray(size: Int) = arrayOfNulls<ProductScreen?>(size)
        }
    }
}