package com.hitasanti.hitaapp.feature.setting

interface SettingContract {

    interface View {

    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, SettingViewState> {
        fun getUserInfo()
    }
}