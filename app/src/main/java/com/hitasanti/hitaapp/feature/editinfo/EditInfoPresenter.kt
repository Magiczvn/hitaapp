package com.hitasanti.hitaapp.feature.editinfo

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.history.HistoryContract
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.Callable


class EditInfoPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<EditInfoContract.View, EditInfoViewState>(), EditInfoContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }

    private var _UpdateUserInfoDisposable: Disposable? = null

    //endregion Private

    override fun onAttachView(view: EditInfoContract.View) {
        super.onAttachView(view)

    }

    override fun updateInfo(first_name: String, last_name: String, address: String, phone: String) {
        val id = User.mId?: return
        _UpdateUserInfoDisposable?.dispose()
        _UpdateUserInfoDisposable = _UseCaseFactory.get().updateUserInfo(userId = id.toString(), first_name = first_name, last_name = last_name, address = address, phone = phone)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                User.mUserInfo = it
                User.save()
                mView?.showResult(true)
            }, {
                mView?.showResult(false)
            })

    }

    override fun onDestroy() {
        _UpdateUserInfoDisposable?.dispose()
        super.onDestroy()

    }
    //endregion BasePresenter

}