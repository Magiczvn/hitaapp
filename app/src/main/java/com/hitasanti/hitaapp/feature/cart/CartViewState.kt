package com.hitasanti.hitaapp.feature.cart


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import com.hitasanti.hitaapp.mvp.BaseViewState
import com.hitasanti.hitaapp.repository.model.Product

class CartViewState(val screen: CartScreen)
    : BaseViewState() {

    var products:List<Product>? = null
    var items: List<Item>? = null
    var addressItem: AddressItem? = null

}