package com.hitasanti.hitaapp.feature.webtab


import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import timber.log.Timber
import java.io.IOException
import java.net.SocketTimeoutException

class WebTabPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                      private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<WebTabContract.View, WebTabViewState>(), WebTabContract.Presenter {

    //region Private
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private var _GetSettingDisposable: Disposable? = null
    private var _ObserveUserDisposable: Disposable? = null
    private var _GetInviteLinkDisposable: Disposable? = null
    private var _VerifyPurchaseDisposable: Disposable? = null
    //endregion Private

    //region BasePresenter
    override fun onAttachView(view: WebTabContract.View) {
        super.onAttachView(view)
    }

    override fun onDestroy() {
        super.onDestroy()

        _GetSettingDisposable?.dispose()
        _ObserveUserDisposable?.dispose()
        _GetInviteLinkDisposable?.dispose()
        _VerifyPurchaseDisposable?.dispose()
    }
    //endregion BasePresenter

    //region WebTabContract.Presenter
}