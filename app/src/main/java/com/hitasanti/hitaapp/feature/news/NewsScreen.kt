package com.hitasanti.hitaapp.feature.news

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class NewsScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is NewsScreen && (other === this)

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<NewsScreen> = object : Parcelable.Creator<NewsScreen> {
            override fun createFromParcel(parcel: Parcel): NewsScreen {
                return NewsScreen()
            }

            override fun newArray(size: Int) = arrayOfNulls<NewsScreen?>(size)
        }
    }
}