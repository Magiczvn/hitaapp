package com.hitasanti.hitaapp.feature.store

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class StoreScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is StoreScreen && (other === this)

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<StoreScreen> = object : Parcelable.Creator<StoreScreen> {
            override fun createFromParcel(parcel: Parcel): StoreScreen {
                return StoreScreen()
            }

            override fun newArray(size: Int) = arrayOfNulls<StoreScreen?>(size)
        }
    }
}