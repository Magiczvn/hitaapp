package com.hitasanti.hitaapp.feature.news

import android.content.Context
import android.graphics.Color
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemAdapter
import com.hitasanti.hitaapp.app.decoration.Divider
import com.hitasanti.hitaapp.app.decoration.SimpleItemDecorationCallback
import com.hitasanti.hitaapp.app.decoration.VerticalDividerDecoration
import com.hitasanti.hitaapp.feature.news.item.ArticleItem
import com.hitasanti.hitaapp.feature.news.item.HeaderItem
import com.hitasanti.hitaapp.feature.news.item.NewsItem
import com.hitasanti.hitaapp.feature.news.viewholder.ArticleItemViewHolder
import com.hitasanti.hitaapp.feature.news.viewholder.HeaderItemViewHolder
import com.hitasanti.hitaapp.feature.news.viewholder.NewsItemViewHolder


class NewsAdapter(private val _Glide: RequestManager) : BaseItemAdapter() {


    companion object {
        private const val ITEM_NEWS = 3
        private const val ITEM_HEADER = 4
    }

    private var _Decorations = emptyList<RecyclerView.ItemDecoration>()


    fun setNewTheme(context: Context) {
        val res = context.resources
        val paddingNormal = res.getDimensionPixelSize(R.dimen.paddingNormal)
        val dividerSmall2 = res.getDimensionPixelSize(R.dimen.dividerSmall)

        removeDecorations()
        _Decorations = listOf(
            VerticalDividerDecoration(Divider(dividerSmall2, paddingNormal, Color.GRAY, Color.GRAY), false,
                SimpleItemDecorationCallback(ArticleItem::class.java)))
        addDecorations()
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItemAt(position)
        return when (item) {
            is NewsItem -> ITEM_NEWS
            is HeaderItem -> ITEM_HEADER
            else -> throw RuntimeException("Not support item $item")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        ITEM_NEWS-> NewsItemViewHolder(parent, R.layout.news_item_viewholder, event, _Glide)
        ITEM_HEADER -> HeaderItemViewHolder(parent, R.layout.header_item_viewholder,event)
        else -> throw RuntimeException("Not support type $viewType")
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        addDecorations()
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        if (!mIsDestroyed) {
            removeDecorations()
        }
        super.onDetachedFromRecyclerView(recyclerView)
    }

    private fun addDecorations() {
        _Decorations.forEach { mRecyclerView?.addItemDecoration(it) }
    }

    private fun removeDecorations() {
        _Decorations.forEach { mRecyclerView?.removeItemDecoration(it) }
    }

}