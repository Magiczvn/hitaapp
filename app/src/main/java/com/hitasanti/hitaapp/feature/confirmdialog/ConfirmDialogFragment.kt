package com.hitasanti.hitaapp.feature.confirmdialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.mvp.BaseDialogFragment
import com.hitasanti.hitaapp.view.NumberUtil
import io.reactivex.Completable
import kotlinx.android.synthetic.main.cart_confirm_dialog.*
import kotlinx.android.synthetic.main.product_dialog_fragment.*
import java.text.DecimalFormat

class ConfirmDialogFragment: BaseDialogFragment<ConfirmDialogScreen>() {

    interface Listener {
        fun onConfirm()

        fun onChange()
    }

    companion object {

        fun instance(screen: ConfirmDialogScreen): ConfirmDialogFragment {
            val fragment = ConfirmDialogFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Private
    private val _Listener: Listener? get() = parentFragment as? Listener ?: context as? Listener
    private var quantity = 1
    //endregion Private

    //region MVP
    override val layoutResource get() = R.layout.cart_confirm_dialog
    //endregion MVP

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(R.drawable.rounded_dialog_bg)

        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        txt_shipping_address?.text = screen.address
        txt_shipping_name?.text = screen.name
        txt_shipping_phone?.text = screen.phone
        order_item_price?.text = NumberUtil.formatPrice(context!!, screen.price)

        confirm_buy?.setOnClickListener {
            positiveClick()
        }

        confirm_cancel?.setOnClickListener {
            negativeClick()
        }

        super.onViewCreated(view, savedInstanceState)
    }
    //endregion Lifecycle


    //region Handle Click
    private fun positiveClick() {
        dismissAllowingStateLoss()
        _Listener?.onConfirm()
    }

    private fun negativeClick() {
        dismissAllowingStateLoss()
        _Listener?.onChange()
    }
    //endregion Handle Click
}