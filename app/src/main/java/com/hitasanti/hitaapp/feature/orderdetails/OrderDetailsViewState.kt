package com.hitasanti.hitaapp.feature.orderdetails


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.mvp.BaseViewState

class OrderDetailsViewState(val screen: OrderDetailsScreen)
    : BaseViewState() {
    var items: List<Item>? = null

}