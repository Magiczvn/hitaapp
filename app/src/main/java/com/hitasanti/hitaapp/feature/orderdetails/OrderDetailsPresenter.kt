package com.hitasanti.hitaapp.feature.orderdetails

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.history.HistoryContract
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.Callable


class OrderDetailsPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>,
                     private val _ItemBuilder: Lazy<NewsItemBuilder>)
    : BasePresenter<OrderDetailsContract.View, OrderDetailsViewState>(), OrderDetailsContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private val _Items = ItemsResult()

    private var _CancelOrderDisposable: Disposable? = null
    private var _ShowProductsDisposable: Disposable? = null

    //endregion Private

    override fun onAttachView(view: OrderDetailsContract.View) {
        super.onAttachView(view)
        showHistoryAsync()
    }

    private fun showHistoryAsync(){
        val work = Callable<Unit> {
            val newItems = _ItemBuilder.get().addOrderDetails()
            mViewState.items = newItems
            _Items.pushItems(newItems)
        }

        _ShowProductsDisposable?.dispose()
        _ShowProductsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
            }, ErrorConsumer())
    }

    override fun cancelOrder(orderId: String, reason: String) {
        _CancelOrderDisposable?.dispose()
        _CancelOrderDisposable = _UseCaseFactory.get().cancelOrder(orderId, reason)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                val order = it.value
                if(order != null){
                    Cart.orderDetails = order
                    mView?.showCancelOrderResult(true)
                    showHistoryAsync()
                }
                else {
                    mView?.showCancelOrderResult(false)
                }
            }, {
                mView?.showCancelOrderResult(false)
            })
    }

    private fun showItemResult() {
        val items = _Items.popItems()

        items?.let { mView?.showItems(it) }
    }



    override fun onDestroy() {
        super.onDestroy()
        _CancelOrderDisposable?.dispose()
        _ShowProductsDisposable?.dispose()


    }
    //endregion BasePresenter

}