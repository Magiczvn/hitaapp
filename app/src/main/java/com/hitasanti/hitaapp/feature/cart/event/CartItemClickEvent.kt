package com.hitasanti.hitaapp.feature.cart.event

class CartItemClickEvent(val productId:Int, val variationId: Int?)