package com.hitasanti.hitaapp.feature.login

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class LoginScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is LoginScreen &&  (other === this)

    override fun toString() = "LoginScreen"

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<LoginScreen> = object : Parcelable.Creator<LoginScreen> {
            override fun createFromParcel(parcel: Parcel): LoginScreen {


                return LoginScreen()
            }

            override fun newArray(size: Int) = arrayOfNulls<LoginScreen?>(size)
        }
    }
}