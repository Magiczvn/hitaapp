package com.hitasanti.hitaapp.feature.main


import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [MainModule::class])
interface MainComponent  {

    val presenter: MainContract.Presenter

    fun inject(activity: MainActivity)
}
