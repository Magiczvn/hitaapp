package com.hitasanti.hitaapp.feature.news

import android.app.ActivityManager
import android.app.Application
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.FragmentScope
import com.hitasanti.hitaapp.dependency.Type
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class NewsModule(private val _Fragment: NewsFragment) {
    @Provides
    @FragmentScope
    internal fun provideNewsItemBuilder(application: Application,
                                               bus: RxBus): NewsItemBuilder = NewsItemBuilder(application,bus)

    @Provides
    @FragmentScope
    internal fun provideGlide(): RequestManager = GlideApp.with(_Fragment)

    @Provides
    @FragmentScope
    internal fun provideAdapter(glide: RequestManager): NewsAdapter = NewsAdapter(glide)

    @Provides
    @FragmentScope
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>,
                                newsItemBuilder: Lazy<NewsItemBuilder>): NewsContract.Presenter = NewsPresenter(useCaseFactory, schedulerFactory, newsItemBuilder)
}
