package com.hitasanti.hitaapp.feature.orderdetails

import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemAdapter
import com.hitasanti.hitaapp.feature.cart.CartAdapter
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import com.hitasanti.hitaapp.feature.cart.item.CartItem
import com.hitasanti.hitaapp.feature.cart.item.OrderItem
import com.hitasanti.hitaapp.feature.cart.item.StatusItem
import com.hitasanti.hitaapp.feature.cart.viewholder.AddressItemViewHolder
import com.hitasanti.hitaapp.feature.cart.viewholder.OrderItemViewHolder
import com.hitasanti.hitaapp.feature.orderdetails.viewholder.OrderDetailsHeaderViewHolder
import com.hitasanti.hitaapp.feature.orderdetails.viewholder.OrderStatusItemViewHolder
import com.hitasanti.hitaapp.feature.product.item.ProductItem
import com.hitasanti.hitaapp.feature.product.viewholder.ProductItemViewHolder


class OrderDetailsAdapter(private val _Glide: RequestManager) : BaseItemAdapter() {

    companion object {
        private const val CART_ITEM = 1
        private const val ADDRESS_ITEM = 2
        private const val STATUS_ITEM = 3
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItemAt(position)
        return when (item) {

            is CartItem -> CART_ITEM
            is AddressItem -> ADDRESS_ITEM
            is StatusItem -> STATUS_ITEM
            else -> throw RuntimeException("Not support item $item")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        CART_ITEM -> OrderItemViewHolder(parent, R.layout.cart_order_item_viewholder, event)
        ADDRESS_ITEM ->OrderDetailsHeaderViewHolder(parent, R.layout.order_details_header_viewholder, event)
        STATUS_ITEM -> OrderStatusItemViewHolder(parent, R.layout.order_details_order_status_viewholder, event)
        else -> throw RuntimeException("Not support type $viewType")
    }

}