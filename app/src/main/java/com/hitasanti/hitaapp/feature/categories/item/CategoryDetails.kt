package com.hitasanti.hitaapp.feature.categories.item

import com.hitasanti.hitaapp.R

class CategoryDetails(val title: Int, val categoryId: Int, val iconId: Int, val colorId: Int) {
    companion object{
        //val HitaChayCategory = CategoryDetails(R.string.hita_chay_title_vertical, 555, R.drawable.ic_hita_chay, R.color.hitaChayBackground)
        val HitaCofeeCategory = CategoryDetails(R.string.hita_coffee_title_vertical, 61, R.drawable.ic_hita_coffee, R.color.hitaCoffeeBackground)
        val HitaTeaCategory = CategoryDetails(R.string.hita_tea_title_vertical, 537, R.drawable.ic_hita_tea, R.color.hitaTeaBackground)
        //val HitaFarmCategory = CategoryDetails(R.string.hita_farm_title_vertical, 544, R.drawable.ic_hita_farm, R.color.hitaFarmBackground)
        val HitaNhangCategory = CategoryDetails(R.string.hita_nhang_title_vertical, 75, R.drawable.ic_hita_nhang, R.color.hitaNhangBackground)
        //val allProductQueryString = "${HitaCofeeCategory.categoryId},${HitaChayCategory.categoryId},${HitaTeaCategory.categoryId},${HitaFarmCategory.categoryId},${HitaNhangCategory.categoryId}"
        val allProductQueryString = "${HitaCofeeCategory.categoryId},${HitaTeaCategory.categoryId},${HitaNhangCategory.categoryId}"
    }
}