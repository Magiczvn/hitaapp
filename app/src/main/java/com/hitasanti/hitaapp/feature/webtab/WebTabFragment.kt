package com.hitasanti.hitaapp.feature.webtab

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.WindowManager
import android.webkit.*
import android.widget.FrameLayout

import com.hitasanti.hitaapp.feature.webtab.event.WebTabFragmentEvent
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.mvp.BaseMvpFragment

import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.webtab_fragment.*
import timber.log.Timber
import javax.inject.Inject


class WebTabFragment : BaseMvpFragment<WebTabContract.View, WebTabContract.Presenter, WebTabViewState, WebTabScreen>(),
        HasComponent<WebTabComponent>, WebTabContract.View {

    companion object {

        fun instance(screen: WebTabScreen): WebTabFragment {
            val fragment = WebTabFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory

    @Inject
    lateinit var _Bus: RxBus
    //endregion Dependency

    //region Private
    private var _Disposable: CompositeDisposable? = null
    var _WebChromeClient: CustomWebChromeClient? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context?) = component.presenter

    override fun onCreateViewState(context: Context?) = WebTabViewState()

    override val viewStateTag: String get() = WebTabViewState::class.java.name

    override val layoutResource get() = R.layout.webtab_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(context!!).component.plus(WebTabModule())
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component.inject(this)

        val s = webview_wv?.settings
        s?.builtInZoomControls = false
        s?.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
        s?.useWideViewPort = true
        s?.loadWithOverviewMode = true
        s?.javaScriptEnabled = true
        s?.domStorageEnabled = true
        s?.databaseEnabled = true
        s?.allowFileAccess = true

        _WebChromeClient = CustomWebChromeClient()
        webview_wv?.webViewClient = CustomWebViewClient()
        webview_wv?.webChromeClient = _WebChromeClient
        webview_pv_progress?.visibility = if(screen.showProgress) View.VISIBLE else View.GONE

        _Disposable = CompositeDisposable(

        )

        webtab_srl?.setOnRefreshListener {
            webview_wv?.reload()
        }

        if (!screen.swipeToClose) {
            webtab_srl?.isEnabled = false
        }

        if (screen.url.contains("isLandscape=1", true)) {
            _Bus.post(WebTabFragmentEvent(screen.url, WebTabFragmentEvent.Type.SCREEN_LANSCAPE))
        } else {
            _Bus.post(WebTabFragmentEvent(screen.url, WebTabFragmentEvent.Type.SCREEN_PORTRAIT))
        }
        loadWeb()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        _Disposable?.dispose()

        _WebChromeClient?.cleanUp()

        super.onDestroyView()
    }

    override fun onResume() {
        super.onResume()

        webview_wv?.onResume()
    }

    override fun onPause() {
        super.onPause()

        webview_wv?.onPause()
    }
    //endregion Lifecycle


    fun invalidate() {
        webview_wv?.invalidate()
    }

    //region Helper
    private fun loadWeb() {
        val headers = mutableMapOf<String, String>()
        var url = screen.url

        try {
            val uri = Uri.parse(url).buildUpon().appendQueryParameter("inapp", "true")
            url = uri.build().toString()

        }catch (ex: java.lang.Exception){

        }
        webview_wv?.loadUrl(url, headers)
    }


    inner class CustomWebChromeClient : WebChromeClient() {

        private var _Progress = 0
        private var _Handler: Handler? = Handler(Looper.getMainLooper())

        internal fun cleanUp() {
            _Handler?.removeCallbacksAndMessages(null)
            _Handler = null
        }

        override fun onProgressChanged(view: WebView, newProgress: Int) {
            if (_Progress != newProgress) {
                _Progress = newProgress

                webview_pv_progress?.progress = _Progress / 100f

                if (_Progress == 100) {
                    _Handler?.postDelayed({
                        webview_pv_progress?.stop()
                    }, 200)
                }
            }
        }

        override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            try {
                activity?.window?.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
            } catch (ex: Exception) {
            }
            return false
        }

        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null
        private var mOriginalOrientation: Int = 0
        private var mOriginalSystemUiVisibility: Int = 0

        override fun onHideCustomView() {
//            super.onHideCustomView()

            val activity = activity ?: return
            (activity.window.decorView as? FrameLayout)?.removeView(this.mCustomView)
            this.mCustomView = null
            activity.window.decorView.systemUiVisibility = this.mOriginalSystemUiVisibility
            activity.requestedOrientation = this.mOriginalOrientation
            this.mCustomViewCallback?.onCustomViewHidden()
            this.mCustomViewCallback = null
        }

        override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
//            super.onShowCustomView(view, callback)

            val activity = activity ?: return

            if (this.mCustomView != null) {
                onHideCustomView()
                return
            }
            this.mCustomView = view
            this.mOriginalSystemUiVisibility = activity.window.decorView.visibility
            this.mOriginalOrientation = activity.requestedOrientation
            this.mCustomViewCallback = callback
            this.mCustomView?.setBackgroundColor(Color.BLACK)

            (activity.window.decorView as? FrameLayout)?.addView(this.mCustomView, FrameLayout.LayoutParams(-1, -1))
            activity.window.decorView.visibility = View.VISIBLE
            activity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        }
    }

    inner class CustomWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView?, baseUrl: String): Boolean {

            var url = baseUrl

            try {
                val uri = Uri.parse(url).buildUpon().appendQueryParameter("inapp", "true")
                url = uri.build().toString()

            }catch (ex: java.lang.Exception){

            }

            when {
                url.startsWith("http://") -> return super.shouldOverrideUrlLoading(view, url)
                url.startsWith("https://") -> return super.shouldOverrideUrlLoading(view, url)
                url.startsWith("intent://") -> {
                    try {
                        val intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

                        startActivity(intent)
                    } catch (e: Exception) {
                    }
                }
                else -> {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    try {
                        startActivity(intent)
                    } catch (e: Exception) {
                    }
                }
            }
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            if (url.contains("isLandscape=1", true)) { //  || url.contains("zingplay", true)
                _Bus.post(WebTabFragmentEvent(screen.url, WebTabFragmentEvent.Type.SCREEN_LANSCAPE))
            } else {
                _Bus.post(WebTabFragmentEvent(screen.url, WebTabFragmentEvent.Type.SCREEN_PORTRAIT))
            }
            webview_pv_progress?.stop()
            if (webtab_srl?.isRefreshing == true) {
                webtab_srl?.isRefreshing = false
            }
        }
    }
    //endregion Helper

}