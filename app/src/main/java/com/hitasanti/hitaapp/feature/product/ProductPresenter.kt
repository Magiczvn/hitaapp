package com.hitasanti.hitaapp.feature.product

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.Callable


class ProductPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>,
                     private val _ItemBuilder: Lazy<NewsItemBuilder>)
    : BasePresenter<ProductContract.View, ProductViewState>(), ProductContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private val _Items = ItemsResult()

    private var _GetProductsDisposable: Disposable? = null
    private var _GetVariationDisposable: Disposable? = null
    private var _ShowProductsDisposable: Disposable? = null


    //endregion Private
    private fun getProducts() {
        if(mViewState.products != null) return
        _GetProductsDisposable?.dispose()
        _GetProductsDisposable = _UseCaseFactory.get().getProducts(mViewState.screen.categoryId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_WorkerScheduler)
            .subscribe( Consumer {
                mViewState.products = it
                Cart.addToProductsMap(it)
                showProductsAsync()
            }, ErrorConsumer())
    }

    /*override fun getProductVariation(productId: Int) {
        _GetVariationDisposable?.dispose()
        _GetVariationDisposable = _UseCaseFactory.get().getProductVariations(productId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe( Consumer {
                Cart.addToVariationMap(it)
                val product = Cart.productsMap[productId]
                if(product!=null)
                    mView?.showProductDialog(product)
            }, ErrorConsumer())
    }*/

    private fun showProductsAsync() {
        if (mViewState.products == null)
            return
        val products = mViewState.products.orEmpty()

        val work = Callable<Unit> {
            val oldItems = mViewState.items.orEmpty()
            val newItems = _ItemBuilder.get().addProducts(oldItems, products).orEmpty()
            mViewState.items = newItems
            _Items.pushItems(newItems)
        }

        _ShowProductsDisposable?.dispose()
        _ShowProductsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
            }, ErrorConsumer())
    }

    private fun showItemResult() {
        val items = _Items.popItems()

        items?.let { mView?.showItems(it) }
    }

    //region BasePresenter
    override fun onAttachView(view: ProductContract.View) {
        super.onAttachView(view)
        mViewState.items?.let {
            view.showItems(it)
        }
        getProducts()
    }



    override fun onDestroy() {
        super.onDestroy()
        _GetProductsDisposable?.dispose()
        _ShowProductsDisposable?.dispose()
        _GetVariationDisposable?.dispose()

    }
    //endregion BasePresenter

}