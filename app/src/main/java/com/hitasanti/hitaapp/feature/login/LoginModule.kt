package com.hitasanti.hitaapp.feature.login

import android.app.Application
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.ActivityScope
import com.hitasanti.hitaapp.dependency.FragmentScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class LoginModule(private val _Activity: LoginActivity) {


    @Provides
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>): LoginContract.Presenter = LoginPresenter(useCaseFactory, schedulerFactory)
}
