package com.hitasanti.hitaapp.feature.webtab.event


/**
 * Copyright (c) 2019, loiphan. All rights reserved.
 *
 * @author loiphan <loipn1804@gmail.com>
 * @since July 17, 2019
 * @version 1.0
 */
class WebTabFragmentEvent(val url: String, val type: Type) {

    enum class Type(val value: Int) {

        SCREEN_LANSCAPE(0),
        SCREEN_PORTRAIT(1),
        GO_FIRST_PAGE(2)
    }
}