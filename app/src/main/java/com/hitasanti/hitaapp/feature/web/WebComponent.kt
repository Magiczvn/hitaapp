package com.hitasanti.hitaapp.feature.web

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [WebModule::class])
interface WebComponent {

    val presenter: WebContract.Presenter

    fun inject(activity: WebActivity)
}
