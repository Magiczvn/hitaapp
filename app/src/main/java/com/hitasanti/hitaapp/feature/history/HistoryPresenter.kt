package com.hitasanti.hitaapp.feature.history

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import com.hitasanti.hitaapp.repository.model.Order
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.Callable


class HistoryPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>,
                       private val _ItemBuilder: Lazy<NewsItemBuilder>)
    : BasePresenter<HistoryContract.View, HistoryViewState>(), HistoryContract.Presenter {

    //region Private

    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private val _Items = ItemsResult()

    private var _GetProductsDisposable: Disposable? = null
    private var _ShowProductsDisposable: Disposable? = null

    //endregion Private


    //region BasePresenter
    override fun onAttachView(view: HistoryContract.View) {
        super.onAttachView(view)
        getHistory()
    }

    override fun addNewOrder(order: Order) {
        val orders = mViewState.histories.orEmpty().toMutableList()
        orders.add(0,order)
        mViewState.histories = orders
        showHistoryAsync()
    }

    private fun getHistory() {
        if(mViewState.histories != null) return
        _GetProductsDisposable?.dispose()
        _GetProductsDisposable = _UseCaseFactory.get().getOrdersHistory()
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe( Consumer {
                mViewState.histories = it
                showHistoryAsync()
            }, ErrorConsumer())
    }

    override fun orderCancelled() {
        val currentOrder = Cart.orderDetails?:return
        val orders = mViewState.histories?.toMutableList()
        var pos = -1
        orders?.forEachIndexed { index, order ->
            if(order.id == currentOrder.id)
                pos = index
        }
        orders?.removeAt(pos)
        orders?.add(pos, currentOrder)
        mViewState.histories = orders

        mViewState.items = emptyList()

        showHistoryAsync()
    }

    private fun showHistoryAsync(){
        val work = Callable<Unit> {
            val oldItems = mViewState.items.orEmpty()
            val newItems = _ItemBuilder.get().addHistories(oldItems,mViewState.histories.orEmpty()).orEmpty()
            mViewState.items = newItems
            _Items.pushItems(newItems)
        }

        _ShowProductsDisposable?.dispose()
        _ShowProductsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
            }, ErrorConsumer())
    }

    private fun showItemResult() {
        val items = _Items.popItems()

        items?.let { mView?.showItems(it) }
    }


    override fun onDestroy() {
        super.onDestroy()
        _GetProductsDisposable?.dispose()
        _ShowProductsDisposable?.dispose()

    }
    //endregion BasePresenter

}