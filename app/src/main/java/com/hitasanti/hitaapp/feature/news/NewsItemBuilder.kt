package com.hitasanti.hitaapp.feature.news

import android.content.Context
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import com.hitasanti.hitaapp.feature.cart.item.CartItem
import com.hitasanti.hitaapp.feature.cart.item.OrderItem
import com.hitasanti.hitaapp.feature.cart.item.StatusItem
import com.hitasanti.hitaapp.feature.categories.item.CategoryDetails
import com.hitasanti.hitaapp.feature.categories.item.CategoryItem
import com.hitasanti.hitaapp.feature.history.item.OrderHistoryItem
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.feature.news.item.ArticleItem
import com.hitasanti.hitaapp.feature.news.item.HeaderItem
import com.hitasanti.hitaapp.feature.news.item.NewsItem
import com.hitasanti.hitaapp.feature.product.item.ProductItem
import com.hitasanti.hitaapp.repository.model.Article
import com.hitasanti.hitaapp.repository.model.Order
import com.hitasanti.hitaapp.repository.model.Product

class NewsItemBuilder(private val _Context: Context,
                      private val _Bus: RxBus) {

    internal fun addHeaderItem(items: List<Item>): List<Item> {
        val list = items.filter {
            when (it) {
                is HeaderItem -> false
                else -> true
            }
        }.toMutableList()

        list.add(0, HeaderItem())

        return list
    }

    internal fun addNewsArticle(items: List<Item>,
                                articles: List<Article>,
                                offers: List<Article>): List<Item>? {
        val list = items.filter {
            when (it) {
                is NewsItem -> false
                else -> true
            }
        }.toMutableList()

        var isAdded = false

        val listOffers = emptyList<ArticleItem>().toMutableList()
        offers.forEach {
            listOffers.add(ArticleItem(it.title.orEmpty(), it.thumbnailURL.orEmpty(), it.excerpt.orEmpty(), it.link.orEmpty()))
            isAdded = true
        }
        if(listOffers.isNotEmpty())
            list.add(NewsItem(listOffers, _Context.getString(R.string.offers_title)))

        val listArticles = emptyList<ArticleItem>().toMutableList()
        articles.forEach {
            listArticles.add(ArticleItem(it.title.orEmpty(), it.thumbnailURL.orEmpty(), it.excerpt.orEmpty(), it.link.orEmpty()))
            isAdded = true
        }
        if(listArticles.isNotEmpty())
        list.add(NewsItem(listArticles, _Context.getString(R.string.news_title)))

        return if (isAdded || list.size != items.size) list else null
    }

    internal fun addProducts(items: List<Item>,
                                products: List<Product>): List<Item>? {
        val list = items.filter {
            when (it) {
                is ProductItem -> false
                else -> true
            }
        }.toMutableList()

        var isAdded = false
        products.forEach {
            val image = it.imgages?.getOrNull(0)?.link.orEmpty()
            val id = it.id?:return@forEach
            val name = it.name?:return@forEach
            val price = it.price?:return@forEach
            val regular_price = it.regularPrice?:price
            list.add(ProductItem(id, name, price, regular_price, it.shortDescription,  image, it.onSale?:false))
        }

        return if (isAdded || list.size != items.size) list else null
    }

    internal fun addCategoryItems(items: List<Item>,
                                    productsMap: HashMap<Int,List<Product>>): List<Item>{
        val list = items.toMutableList()



        var products = emptyList<ProductItem>().toMutableList()
        val coffee = productsMap[CategoryDetails.HitaCofeeCategory.categoryId].orEmpty()
        coffee.forEach {
            val image = it.imgages?.getOrNull(0)?.link.orEmpty()
            val id = it.id?:return@forEach
            val name = it.name?:return@forEach
            val price = it.price?:return@forEach
            val regular_price = it.regularPrice?:price
            products.add(ProductItem(id, name, price, regular_price, it.shortDescription,  image,it.onSale?:false))
        }
        list.add(CategoryItem(products, CategoryDetails.HitaCofeeCategory))

        products = emptyList<ProductItem>().toMutableList()
        val nhang = productsMap[CategoryDetails.HitaNhangCategory.categoryId].orEmpty()
        nhang.forEach {
            val image = it.imgages?.getOrNull(0)?.link.orEmpty()
            val id = it.id?:return@forEach
            val name = it.name?:return@forEach
            val price = it.price?:return@forEach
            val regular_price = it.regularPrice?:price
            products.add(ProductItem(id, name, price, regular_price, it.shortDescription,  image, it.onSale?:false))
        }
        list.add(CategoryItem(products, CategoryDetails.HitaNhangCategory))

        products = emptyList<ProductItem>().toMutableList()
        val tea = productsMap[CategoryDetails.HitaTeaCategory.categoryId].orEmpty()
        tea.forEach {
            val image = it.imgages?.getOrNull(0)?.link.orEmpty()
            val id = it.id?:return@forEach
            val name = it.name?:return@forEach
            val price = it.price?:return@forEach
            val regular_price = it.regularPrice?:price
            products.add(ProductItem(id, name, price, regular_price, it.shortDescription,  image, it.onSale?:false))
        }
        list.add(CategoryItem(products, CategoryDetails.HitaTeaCategory))



        /*products = emptyList<ProductItem>().toMutableList()
        val chay = productsMap[CategoryDetails.HitaChayCategory.categoryId].orEmpty()
        chay.forEach {
            val image = it.imgages?.getOrNull(0)?.link.orEmpty()
            val id = it.id?:return@forEach
            val name = it.name?:return@forEach
            val price = it.price?:return@forEach
            val regular_price = it.regularPrice?:price
            products.add(ProductItem(id, name, price, regular_price, it.shortDescription,  image))
        }
        list.add(CategoryItem(products, CategoryDetails.HitaChayCategory))

        products = emptyList<ProductItem>().toMutableList()
        val farm = productsMap[CategoryDetails.HitaFarmCategory.categoryId].orEmpty()
        farm.forEach {
            val image = it.imgages?.getOrNull(0)?.link.orEmpty()
            val id = it.id?:return@forEach
            val name = it.name?:return@forEach
            val price = it.price?:return@forEach
            val regular_price = it.regularPrice?:price
            products.add(ProductItem(id, name, price, regular_price, it.shortDescription,  image))
        }
        list.add(CategoryItem(products, CategoryDetails.HitaFarmCategory))*/


        return list
    }

    internal fun addCartAddress(items: List<Item>): List<Item>? {
        val list = items.toMutableList()
        val userInfo = User.mUserInfo

        list.add(AddressItem(name = User.getUserName(), address = userInfo?.shippingAddress.orEmpty(), phone = userInfo?.phone.orEmpty()))
        return list
    }

    internal fun updateCartAddress(items: List<Item>): List<Item>? {
        val list = items.filter {
            when (it) {
                is AddressItem -> false
                else -> true
            }
        }.toMutableList()

        val userInfo = User.mUserInfo

        list.add(0, AddressItem(name = User.getUserName(), address = userInfo?.shippingAddress.orEmpty(), phone = userInfo?.phone.orEmpty()))
        return list
    }

    internal fun addCart(items: List<Item>): List<Item>? {
        val list = items.filter {
            when (it) {
                is CartItem -> false
                else -> true
            }
        }.toMutableList()

        val shoppingList = Cart.shoppingList
        val productMap = Cart.productsMap
        val variationMap = Cart.variationsMap
        val cartMap = Cart.cartMap

        val orderList = mutableListOf<OrderItem>()

        shoppingList.forEach {
            val variation = variationMap[it]
            if(variation != null){
                val product = productMap[variation.productId]?: return@forEach
                val quantity = cartMap[it]?: return@forEach
                val image = product.imgages?.getOrNull(0)?.link.orEmpty()
                orderList.add(OrderItem(product.id, "${product.name} ${variation.name}", variation.price, null, quantity, product.shortDescription, image, variation.id))
            }
            else{
                val product = productMap[it]?: return@forEach
                val quantity = cartMap[it]?: return@forEach
                val image = product.imgages?.getOrNull(0)?.link.orEmpty()
                orderList.add(OrderItem(product.id, product.name, product.price, null,  quantity, product.shortDescription, image, null))
            }
        }

        list.add(CartItem(orderList, Cart.totalPrice, Cart.getShippingFee(), Cart.getDiscountPrice()))


        return list
    }

    internal fun addHistories(items: List<Item>,
                             histories: List<Order>): List<Item>? {
        val list = items.filter {
            when (it) {
                is OrderHistoryItem -> false
                else -> true
            }
        }.toMutableList()

        var isAdded = false
        histories.forEach {
            list.add(OrderHistoryItem(it))
        }

        return list
    }

    internal fun addOrderDetails(): List<Item> {

        val list = mutableListOf<Item>()
        val orderDetails = Cart.orderDetails?:return list


        val orderList = mutableListOf<OrderItem>()

        list.add(StatusItem(orderDetails.status, orderDetails.shippingNotes))

        var tempPrice = 0
        orderDetails.orderDetails.forEach {
            tempPrice += it.subTotal
            orderList.add(OrderItem(it.product_id, it.name, it.price, it.subTotal, it.quantity,"", "", null, false))
        }

        list.add(CartItem(orderList, tempPrice, orderDetails.shippingTotal, orderDetails.discountTotal))

        val addressItem = AddressItem(orderDetails.shippingAddress, orderDetails.shippingPhone, orderDetails.shippingName, orderDetails.shippingNotes)
        list.add(addressItem)

        return list
    }

}