package com.hitasanti.hitaapp.feature.order

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CheckedTextView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.CartActivity
import com.hitasanti.hitaapp.feature.cart.CartScreen
import com.hitasanti.hitaapp.feature.home.event.OpenHomePageEvent
import com.hitasanti.hitaapp.feature.home.event.OpenOrderPageEvent
import com.hitasanti.hitaapp.feature.home.event.OpenSettingPageEvent
import com.hitasanti.hitaapp.feature.home.event.OpenStorePageEvent
import com.hitasanti.hitaapp.feature.order.event.CartUpdateEvent
import com.hitasanti.hitaapp.feature.product.ProductScreen
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import com.hitasanti.hitaapp.mvp.MvpCleaner
import com.hitasanti.hitaapp.repository.model.Category
import com.hitasanti.hitaapp.view.CheckedImageView
import com.hitasanti.hitaapp.view.NumberUtil
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.order_fragment.*
import javax.inject.Inject

class OrderActivity : BaseSwipeMvpActivity<OrderContract.View, OrderContract.Presenter, OrderViewState, OrderScreen>(),
        HasComponent<OrderComponent>, OrderContract.View {

    companion object {

        fun instance(context: Context, screen: OrderScreen): Intent {
            val intent = Intent(context, OrderActivity::class.java)
            setScreen(intent, screen)
            return intent
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>
    //endregion Dependency
    private var _Adapter: OrderAdapter? = null
    private var _MvpCleaner: MvpCleaner? = null
    private var _Disposable: CompositeDisposable? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = OrderViewState(screen)

    override val viewStateTag: String get() = OrderViewState::class.java.name

    override val layoutResource get() = R.layout.order_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(OrderModule())
    }
    //endregion HasComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _MvpCleaner = MvpCleaner(presenterCache, viewStateCache)

        component.inject(this)
        enableSwipe(true)

        categories_tl?.addOnTabSelectedListener(OnTabSelectedListener())

        categories_vp?.setSwipeable(true)
        categories_vp?.addOnPageChangeListener(OnPageChangeListener())


        _Disposable = CompositeDisposable(  _Bus.register(CartUpdateEvent::class.java)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                onCartUpdated()
            }, ErrorConsumer()),
            _Bus.register(OpenOrderPageEvent::class.java)
                .filter { it.sender !== this }
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    finish()
                }, ErrorConsumer()))

        ll_cart?.setOnClickListener {
            viewCart()
        }

        ic_home?.setOnClickListener {
            _Bus.post(OpenHomePageEvent())
            finish()
        }

        val tvChecked = ic_order?.findViewById<CheckedTextView>(R.id.item_home_tab_tv)
        tvChecked?.isChecked = true
        val icChecked = ic_order?.findViewById<CheckedImageView>(R.id.item_home_tab_icon)
        icChecked?.isChecked = true

        ic_order?.setOnClickListener {
            _Bus.post(OpenOrderPageEvent(this))
            finish()
        }

        ic_store?.setOnClickListener {
            _Bus.post(OpenStorePageEvent())
            finish()
        }

        ic_setting?.setOnClickListener {
            _Bus.post(OpenSettingPageEvent())
            finish()
        }

        onCartUpdated()
    }



    private fun viewCart(){
        val intent = CartActivity.instance(context = this, screen = CartScreen(1,""))
        startActivity(intent)
    }

    private fun onCartUpdated(){
        val quantity = Cart.totalItems
        val price = Cart.totalPrice
        ll_cart.visibility = if(quantity > 0) View.VISIBLE else View.GONE
        cart_quantity.text = quantity.toString()
        cart_price.text = NumberUtil.formatPrice(this, price)
    }

    private fun cleanAllPresenter() {
        val adapter = _Adapter ?: return

        // Remove adapter will trigger persit Presenter and ViewState of child fragment.
        categories_vp?.adapter = null
        adapter.savedStates.forEach {
            _MvpCleaner?.clean(it, false, true) //Keep ViewState for faster register.
        }

        _Adapter = null
    }

    override fun onDestroy() {
        cleanAllPresenter()
        _Disposable?.dispose()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
    }
    //endregion Lifecycle

    override fun showCategories(categories: List<Category>) {
        if(categories.isEmpty())
        {
            fl_top_bar.visibility = View.GONE
            txtComingSoon.visibility = View.VISIBLE
            categories_vp.visibility = View.GONE
            return
        }

        val screens = categories.map {
            val id = it.id?:return
            val name = it.name?:return
            return@map ProductScreen(id, name)
        }
        if (_Adapter == null) {
            _Adapter = OrderAdapter(supportFragmentManager, screens)
            categories_vp?.adapter = _Adapter
            categories_tl?.setupWithViewPager(categories_vp)
        }
    }

    override fun onBackPressed() {
        if (categories_vp?.currentItem != 0)
            categories_vp?.setCurrentItem(0, true)
        else
            finish()
    }

    private inner class OnTabSelectedListener : TabLayout.OnTabSelectedListener {

        override fun onTabSelected(tab: TabLayout.Tab) {
        }

        override fun onTabUnselected(tab: TabLayout.Tab) {
        }

        override fun onTabReselected(tab: TabLayout.Tab) {


        }
    }

    private inner class OnPageChangeListener : ViewPager.OnPageChangeListener {

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            enableSwipe(position == 0 || presenter.isEmpty)
        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }
    //endregion Helper
}