package com.hitasanti.hitaapp.feature.orderdetails

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.cart.item.AddressItem

interface OrderDetailsContract {

    interface View {
        fun showItems(items: List<Item>)

        fun showCancelOrderResult(result: Boolean)
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, OrderDetailsViewState> {
        fun cancelOrder(orderId: String, reason: String)
    }
}