package com.hitasanti.hitaapp.feature.login


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import com.hitasanti.hitaapp.mvp.BaseViewState
import com.hitasanti.hitaapp.repository.model.Product

class LoginViewState(val screen: LoginScreen)
    : BaseViewState() {


}