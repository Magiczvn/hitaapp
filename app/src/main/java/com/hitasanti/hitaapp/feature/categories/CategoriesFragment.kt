package com.hitasanti.hitaapp.feature.categories

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.epi.feature.webtab.event.GoBackEvent
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseLinearLayoutManager
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.CartActivity
import com.hitasanti.hitaapp.feature.cart.CartScreen
import com.hitasanti.hitaapp.feature.news.event.HeaderClickedEvent
import com.hitasanti.hitaapp.feature.news.event.NewsLoadedEvent
import com.hitasanti.hitaapp.feature.order.OrderActivity
import com.hitasanti.hitaapp.feature.order.OrderScreen
import com.hitasanti.hitaapp.feature.order.event.CartUpdateEvent
import com.hitasanti.hitaapp.feature.product.event.ProductViewEvent
import com.hitasanti.hitaapp.feature.productdialog.ProductDialogFragment
import com.hitasanti.hitaapp.feature.productdialog.ProductDialogScreen
import com.hitasanti.hitaapp.mvp.BaseMvpFragment
import com.hitasanti.hitaapp.repository.model.Product
import com.hitasanti.hitaapp.view.NumberUtil
import com.hitasanti.hitaapp.view.ViewUtil
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.categories_fragment.*
import kotlinx.android.synthetic.main.categories_fragment.cart_price
import kotlinx.android.synthetic.main.categories_fragment.cart_quantity
import kotlinx.android.synthetic.main.categories_fragment.ll_cart
import kotlinx.android.synthetic.main.order_fragment.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CategoriesFragment : BaseMvpFragment<CategoriesContract.View, CategoriesContract.Presenter, CategoriesViewState, CategoriesScreen>(),
        HasComponent<CategoriesComponent>, CategoriesContract.View, ProductDialogFragment.Listener {

    companion object {

        fun instance(screen: CategoriesScreen): CategoriesFragment {
            val fragment = CategoriesFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>
    //endregion Dependency
    @Inject
    lateinit var _Adapter: CategoriesAdapter

    private var _Disposable: CompositeDisposable? = null

    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context?) = component.presenter

    override fun onCreateViewState(context: Context?) = CategoriesViewState(screen)

    override val viewStateTag: String get() = CategoriesViewState::class.java.name

    override val layoutResource get() = R.layout.categories_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(context!!).component.plus(CategoriesModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component.inject(this)
        categories_rv?.adapter = _Adapter

        categories_rv?.let {
            ViewUtil.doOnGlobalLayout(it) {
                _Adapter.setReady(true)
            }
        }
        categories_rv?.layoutManager = BaseLinearLayoutManager(context!!)


        _Disposable = CompositeDisposable(_Adapter.event
            .throttleFirst(BuildConfig.BUTTON_DELAY, TimeUnit.MILLISECONDS)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                when (it) {
                    is ProductViewEvent ->{
                        showProduct(it)
                    }
                    is HeaderClickedEvent -> onCategoryClicked(it.catedoryId)
                }
            }, ErrorConsumer()),
            _Bus.register(NewsLoadedEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    presenter.startLoading()
                }, ErrorConsumer()),
            _Bus.register(CartUpdateEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    onCartUpdated()
                }, ErrorConsumer())
        )

        ll_cart.setOnClickListener {
            viewCart()
        }


        categories_rv?.setOrientation(resources.configuration.orientation)
        onCartUpdated()


        super.onViewCreated(view, savedInstanceState)
    }

    private fun viewCart(){
        val intent = CartActivity.instance(context = context!!, screen = CartScreen(1,""))
        startActivity(intent)
    }

    private fun onCartUpdated(){
        val quantity = Cart.totalItems
        val price = Cart.totalPrice
        ll_cart.visibility = if(quantity > 0) View.VISIBLE else View.GONE
        cart_quantity.text = quantity.toString()
        cart_price.text = NumberUtil.formatPrice(context!!, price)
    }

    override fun showProductDialog(product: Product) {
        val productDialogScreen = ProductDialogScreen( id = product.id,
            price = product.price,
            shortDescription = product.shortDescription,
            name = product.name,
            imageUrl =  product.imgages?.getOrNull(0)?.link.orEmpty(),
            quantity = 1,
            variationId = null,
            canRemove = false)
        ProductDialogFragment.instance(productDialogScreen)
            .show(childFragmentManager)
    }

    private fun showProduct(event: ProductViewEvent){
        val product = Cart.productsMap[event.id]?:return
        /*if(!product.variations.isNullOrEmpty())
            presenter.getProductVariation(event.id)
        else*/
        showProductDialog(product)
    }

    private fun onRefreshClick(){

    }

    private fun onCategoryClicked(categoryId: Int?){
        if(categoryId!= null) {
            val intent =
                OrderActivity.instance(context = context!!, screen = OrderScreen(categoryId))
            startActivity(intent)
        }
    }


    override fun onDestroyView() {
        _Disposable?.dispose()
        _Adapter.onDestroy()

        super.onDestroyView()
    }

    override fun showItems(items: List<Item>) {
        Cart.loadProducts()
        _Bus.post(CartUpdateEvent())
        _Adapter.updateItems(items)
    }

    override fun onAddToCart(productId: Int, quantity: Int, isVariation: Boolean) {
        if(!isVariation) {
            val product = Cart.productsMap[productId] ?: return
            Cart.addProduct(product, quantity)
            val toast = Toast.makeText(context, context?.getString(R.string.txt_added_to_cart, quantity, product.name), Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
            Cart.saveProducts()
        }
        else{
            val variation = Cart.variationsMap[productId]?:return
            val product = Cart.productsMap[variation.productId]?:return
            Cart.addProduct(variation, quantity)
            val toast = Toast.makeText(context, context?.getString(R.string.txt_added_to_cart, quantity, "${product.name} ${variation.name}"), Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
            Cart.saveProducts()
        }

        onCartUpdated()
    }

    override fun onUpdateCart(productId: Int, quantity: Int, isVariation: Boolean) {

    }

    override fun onCancel() {

    }


    override fun onResume() {
        super.onResume()
    }

}