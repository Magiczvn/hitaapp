package com.hitasanti.hitaapp.feature.userinfo

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [UserInfoModule::class])
interface UserInfoComponent {

    val presenter: UserInfoContract.Presenter

    fun inject(activity: UserInfoActivity)
}
