package com.hitasanti.hitaapp.feature.history

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class HistoryScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is HistoryScreen && (other === this)

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HistoryScreen> = object : Parcelable.Creator<HistoryScreen> {
            override fun createFromParcel(parcel: Parcel): HistoryScreen {
                return HistoryScreen()
            }

            override fun newArray(size: Int) = arrayOfNulls<HistoryScreen?>(size)
        }
    }
}