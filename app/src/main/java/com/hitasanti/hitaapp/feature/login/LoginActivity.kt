package com.hitasanti.hitaapp.feature.login

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.data.model.GoogleAuthResponse
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.login.event.LoginSuccessEvent
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.login_activity.*
import org.json.JSONObject
import javax.inject.Inject


class LoginActivity : BaseSwipeMvpActivity<LoginContract.View, LoginContract.Presenter, LoginViewState, LoginScreen>(),
    HasComponent<LoginComponent>, LoginContract.View {
    companion object {

        fun instance(context: Context, screen: LoginScreen): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            setScreen(intent, screen)
            return intent
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus

    //endregion Dependency

    //region Private
    private var _Disposable: CompositeDisposable? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = LoginViewState(screen)

    override val viewStateTag: String get() = LoginViewState::class.java.name

    override val layoutResource get() = R.layout.login_activity

    private val callbackManager = CallbackManager.Factory.create()

    private var isLoggingIn = false
    private var mGoogleSignInClient:GoogleSignInClient? = null
    private val RC_SIGN_IN = 2610
    private var _Dialog: Dialog? = null

    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(LoginModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)


        enableSwipe(true)

        btnLogin.setOnClickListener {
            if(!isLoggingIn) {
                isLoggingIn = true
                login()
            }
        }

        btnFBLogin?.setOnClickListener {
            if(!isLoggingIn) {
                isLoggingIn = true
                LoginManager.getInstance().logIn(this, listOf("email"))
            }
        }

        btnGoogleLogin?.setOnClickListener {
            if(mGoogleSignInClient == null) {
                val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestServerAuthCode(getString(R.string.google_client_id))
                    .requestEmail()
                    .build()
                mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
            }

            if(!isLoggingIn) {
                isLoggingIn = true
                googleLogin()
            }
        }

        setResult(Activity.RESULT_CANCELED)

        setupUI(root_view)



        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val token = loginResult?.accessToken?:return
                val request: GraphRequest = GraphRequest.newMeRequest(token) { data, response ->
                    val email = data?.get("email")?.toString()
                    val id = data?.get("id")?.toString()

                    if(!id.isNullOrEmpty() && !email.isNullOrEmpty()){
                        presenter.facebookLoogin(token, email, id)
                        showProgressBar()
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,email")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                isLoggingIn = false
            }

            override fun onError(exception: FacebookException) {
                showLoginResult(false, "Facebook")
            }
        })

        _Disposable = CompositeDisposable()

    }


    private fun showProgressBar(){
        _Dialog = MaterialDialog.Builder(this)
            .contentGravity(GravityEnum.CENTER)
            .title("Đang xử lý")
            .progress(true, 0)
            .theme(Theme.LIGHT)
            .show()
    }



    fun hideSoftKeyboard() {
        val inputMethodManager: InputMethodManager = getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            currentFocus?.windowToken, 0
        )
    }

    fun setupUI(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { v, event ->
                hideSoftKeyboard()
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView)
            }
        }
    }

    private fun googleLogin() {
        val signInIntent: Intent = mGoogleSignInClient?.signInIntent ?:return
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun login (){
        if(txtUsername.text.isNullOrEmpty() || txt_password.text.isNullOrEmpty()){
            return
        }

        btnLogin.isClickable = false
        txtLogin.visibility = View.INVISIBLE
        loading_pv.visibility = View.VISIBLE

        presenter.Login(txtUsername.text.toString(), txt_password.text.toString())
    }

    override fun showLoginResult(result: Boolean) {
        if(result){
            _Bus.post(LoginSuccessEvent())
            val toast = Toast.makeText(this,"Đăng nhập thành công!", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER,0,0)
            toast.show()
            setResult(Activity.RESULT_OK)
            finish()
        }
        else{
            val toast = Toast.makeText(this,"Sai tên đăng nhập hoặc mật khẩu.", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER,0,0)
            toast.show()
            onLoginError()
        }
        isLoggingIn = false
    }

    override fun showLoginResult(result: Boolean, service: String) {
        if(result){
            _Bus.post(LoginSuccessEvent())
            val toast = Toast.makeText(this,"Đăng nhập $service thành công!", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER,0,0)
            toast.show()
            setResult(Activity.RESULT_OK)
            finish()
        }
        else{
            val toast = Toast.makeText(this,"Có lỗi khi đăng nhập $service!", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER,0,0)
            toast.show()
        }
        isLoggingIn = false
        _Dialog?.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            if(account != null){
                val code = account.serverAuthCode
                if(code != null){
                    User.mGoogleAccount = account
                    presenter.getGoogleAuthCode(code, getString(R.string.google_client_id))
                    showProgressBar()
                }
                else{
                    showLoginResult(false, "Google")
                }
            }

        } catch (e: ApiException) {
            showLoginResult(false, "Google")
        }
    }

    override fun showGoogleAuthCode(googleAuthResponse: GoogleAuthResponse) {
        presenter.googleLogin(googleAuthResponse)
    }

    private fun onLoginError(){
        btnLogin.isClickable = true
        txtLogin.visibility = View.VISIBLE
        loading_pv.visibility = View.INVISIBLE
    }


    override fun onDestroy() {
        _Disposable?.dispose()

        super.onDestroy()
    }

}