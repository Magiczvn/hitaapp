package com.hitasanti.hitaapp.feature.web

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class WebScreen(val url: String,
                val swipeToClose: Boolean)
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(url)
        dest?.writeInt(if (swipeToClose) 1 else 0)
    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is WebScreen && (other === this || (other.url == url &&
            other.swipeToClose == swipeToClose))

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<WebScreen> = object : Parcelable.Creator<WebScreen> {
            override fun createFromParcel(parcel: Parcel): WebScreen {
                val url = parcel.readString() ?: ""
                val openAnim = parcel.readInt() == 1
                return WebScreen(url, openAnim)
            }

            override fun newArray(size: Int) = arrayOfNulls<WebScreen?>(size)
        }
    }
}