package com.hitasanti.hitaapp.feature.cart.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item

class AddressItem(var address: String = "", var phone: String = "", var name: String = "", var notes: String = ""): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is AddressItem && (other === this)
}