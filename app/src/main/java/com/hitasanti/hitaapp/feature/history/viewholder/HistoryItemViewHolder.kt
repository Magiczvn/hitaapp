package com.hitasanti.hitaapp.feature.history.viewholder

import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.history.event.HistoryClickedEvent
import com.hitasanti.hitaapp.feature.history.event.ReorderClickEvent
import com.hitasanti.hitaapp.feature.history.item.OrderHistoryItem
import com.hitasanti.hitaapp.view.NumberUtil
import io.reactivex.subjects.Subject
import kotterknife.bindView

class HistoryItemViewHolder(parent: ViewGroup,
                                  resId: Int,
                                  private val _EventSubject: Subject<Any>,
                                  private val _Glide: RequestManager) :
    BaseItemViewHolder<OrderHistoryItem>(parent, resId) {

    private val _AddressView: TextView by bindView(R.id.txt_shipping_address)
    private val _TimeView: TextView by bindView(R.id.txt_order_time)
    private val _PriceView: TextView by bindView(R.id.order_item_price)
    private val _StatusView: TextView by bindView(R.id.order_status)
    private val _BtnReorder: TextView by bindView(R.id.btn_reorder)




    init {
        itemView.setOnClickListener {
            val order = item?.order
            if(order != null){
                Cart.orderDetails = order
                _EventSubject.onNext(HistoryClickedEvent())
            }
        }

        _BtnReorder.setOnClickListener {
            val order = item?.order
            if(order != null){
                _EventSubject.onNext(ReorderClickEvent(order))
            }
        }
    }

    override fun onBindItem(item: OrderHistoryItem) {
        val oldItem = this.item

        if (oldItem == null || oldItem.order.shippingAddress != item.order.shippingAddress) {
            _AddressView.text = item.order.shippingAddress
        }

        if (oldItem == null || oldItem.order.dateCreated != item.order.dateCreated) {
            _TimeView.text = NumberUtil.formatDateTime(item.order.dateCreated)
        }

        if (oldItem == null || oldItem.order.price != item.order.price) {
            _PriceView.text = NumberUtil.formatPrice(itemView.context, item.order.price)
        }

        if (oldItem == null || oldItem.order.status != item.order.status) {
            val status = Cart.getOrderStatus(itemView.context, item.order.status)
            _StatusView.text = status
        }


        super.onBindItem(item)
    }
}