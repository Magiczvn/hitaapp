package com.hitasanti.hitaapp.feature.cart

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [CartModule::class])
interface CartComponent {

    val presenter: CartContract.Presenter

    fun inject(fragment: CartActivity)
}
