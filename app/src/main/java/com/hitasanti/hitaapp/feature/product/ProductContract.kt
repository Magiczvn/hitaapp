package com.hitasanti.hitaapp.feature.product

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.repository.model.Product

interface ProductContract {

    interface View {
        fun showItems(items: List<Item>)

        fun showProductDialog(product: Product)
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, ProductViewState> {
        //fun getProductVariation(productId: Int)
    }
}