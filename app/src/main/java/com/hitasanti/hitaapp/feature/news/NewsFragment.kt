package com.hitasanti.hitaapp.feature.news

import android.content.Context
import android.os.Bundle
import android.view.View
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseLinearLayoutManager
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.app.event.BackPressEvent
import com.hitasanti.hitaapp.app.event.ForegroundTabEvent
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.news.event.HeaderClickedEvent
import com.hitasanti.hitaapp.feature.news.event.NewsClickedEvent
import com.hitasanti.hitaapp.feature.news.event.NewsLoadedEvent
import com.hitasanti.hitaapp.feature.order.OrderActivity
import com.hitasanti.hitaapp.feature.order.OrderScreen
import com.hitasanti.hitaapp.feature.webtab.WebTabFragment
import com.hitasanti.hitaapp.feature.webtab.WebTabScreen
import com.hitasanti.hitaapp.mvp.BaseMvpFragment
import com.hitasanti.hitaapp.view.ViewUtil
import com.jakewharton.rxbinding2.view.RxView
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.news_fragment.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class NewsFragment : BaseMvpFragment<NewsContract.View, NewsContract.Presenter, NewsViewState, NewsScreen>(),
        HasComponent<NewsComponent>, NewsContract.View {

    companion object {

        fun instance(screen: NewsScreen): NewsFragment {
            val fragment = NewsFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>
    //endregion Dependency
    @Inject
    lateinit var _Adapter: NewsAdapter

    private var _Disposable: CompositeDisposable? = null
    var _WebTabFragment: WebTabFragment? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context?) = component.presenter

    override fun onCreateViewState(context: Context?) = NewsViewState(screen)

    override val viewStateTag: String get() = NewsViewState::class.java.name

    override val layoutResource get() = R.layout.news_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(context!!).component.plus(NewsModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component.inject(this)
        news_rv?.adapter = _Adapter

        news_rv?.let {
            ViewUtil.doOnGlobalLayout(it) {
                _Adapter.setReady(true)
            }
        }
        news_rv?.layoutManager = BaseLinearLayoutManager(context!!)


        _Disposable = CompositeDisposable(_Adapter.event
            .throttleFirst(BuildConfig.BUTTON_DELAY, TimeUnit.MILLISECONDS)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                when (it) {
                    is NewsClickedEvent -> onNewsClicked(it.link) //ErrorItemViewHolder
                    is HeaderClickedEvent -> onCategoryClicked(it.catedoryId)
                }
            }, ErrorConsumer()),
            _Bus.register(ForegroundTabEvent::class.java)
                .filter {
                    it.screen == screen && (it.sender == activity || it.sender == parentFragment)
                }.observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                   closeWeb()
                }, ErrorConsumer()),
            _Bus.register(BackPressEvent::class.java)
                .filter { it.sender !== this }
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    if(_WebTabFragment != null)
                        closeWeb()
                    else
                        _Bus.post(BackPressEvent(this))
                }, ErrorConsumer())
        )

        hita_logo?.let {
            _Disposable?.add(
                RxView.clicks(it)
                .throttleFirst(BuildConfig.BUTTON_DELAY, TimeUnit.MILLISECONDS)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer { _ ->
                    if(_WebTabFragment == null)
                        openHitaHomePage()
                    else closeWeb() }, ErrorConsumer()))
        }


        news_rv?.setOrientation(resources.configuration.orientation)



        super.onViewCreated(view, savedInstanceState)
    }

    private fun closeWeb(){
        val fragment = _WebTabFragment
        if(fragment!= null) {
            web_fl_fragment.visibility = View.GONE
            news_rv.visibility = View.VISIBLE
            ll_audio.visibility = View.VISIBLE
            val transaction = childFragmentManager.beginTransaction()
            transaction.remove(fragment)
            transaction.commitAllowingStateLoss()
            _WebTabFragment = null
        }
    }

    private fun onRefreshClick(){

    }

    private fun onCategoryClicked(categoryId: Int?){
        if (categoryId!= null) {
            val intent =
                OrderActivity.instance(context = context!!, screen = OrderScreen(categoryId))
            startActivity(intent)
        }
        else{
            openHitaHomePage()
        }
    }

    private fun openHitaHomePage(){
        onNewsClicked("https://hitasanti.com")
    }

    private fun onNewsClicked(url: String){
        web_fl_fragment.visibility = View.VISIBLE
        news_rv.visibility = View.GONE
        ll_audio.visibility = View.GONE
        if (childFragmentManager.findFragmentById(R.id.web_fl_fragment) == null) {
            val webTabScreen = WebTabScreen(url = url,
                swipeToClose = true)
            val fragment = WebTabFragment.instance(screen = webTabScreen)
            _WebTabFragment = fragment
            val transaction = childFragmentManager.beginTransaction()
            transaction.replace(R.id.web_fl_fragment, fragment)
            transaction.commitAllowingStateLoss()
        }
    }

    override fun notifyNewsLoaded() {
        _Bus.post(NewsLoadedEvent())
    }

    override fun onDestroyView() {
        _Disposable?.dispose()
        _Adapter.onDestroy()

        super.onDestroyView()
    }

    override fun showItems(items: List<Item>) {
        _Adapter.updateItems(items)
    }


    override fun onResume() {
        super.onResume()
    }

}