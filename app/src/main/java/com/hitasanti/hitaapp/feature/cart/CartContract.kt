package com.hitasanti.hitaapp.feature.cart

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import com.hitasanti.hitaapp.repository.model.Coupon
import com.hitasanti.hitaapp.repository.model.Order

interface CartContract {

    interface View {
        fun showItems(items: List<Item>)

        fun showOrderResult(result: Boolean, order: Order?)

        fun showCouponResult(coupon: Coupon?)
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, CartViewState> {
        fun updateCart()

        fun updateCartAddress()

        fun getAddressItem() :AddressItem?

        fun placeOrder(shippingNote: String, shippingName: String)

        fun getCoupon(code: String)
    }
}