package com.hitasanti.hitaapp.feature.store

interface StoreContract {

    interface View {

    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, StoreViewState> {


    }
}