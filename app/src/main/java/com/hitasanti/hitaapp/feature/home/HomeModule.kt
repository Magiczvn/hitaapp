package com.hitasanti.hitaapp.feature.home

import com.hitasanti.hitaapp.dependency.FragmentScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class HomeModule(val _Fragment: HomeFragment) {

    @Provides
    @FragmentScope
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>): HomeContract.Presenter = HomePresenter(useCaseFactory, schedulerFactory)

}
