package com.hitasanti.hitaapp.feature.cart

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class CartScreen(val categoryId: Int, val title: String)
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(categoryId)
        dest?.writeString(title)
    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is CartScreen &&  (other === this || (other.categoryId == categoryId &&
            other.title == title))

    override fun toString() = "CartScreen ${categoryId}"

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CartScreen> = object : Parcelable.Creator<CartScreen> {
            override fun createFromParcel(parcel: Parcel): CartScreen {
                val categoryId = parcel.readInt()
                val title = parcel.readString() ?: ""

                return CartScreen(categoryId, title)
            }

            override fun newArray(size: Int) = arrayOfNulls<CartScreen?>(size)
        }
    }
}