package com.hitasanti.hitaapp.feature.news.viewholder

import android.text.Html
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.news.event.NewsClickedEvent
import com.hitasanti.hitaapp.feature.news.item.ArticleItem
import io.reactivex.subjects.Subject
import kotterknife.bindView

class ArticleItemViewHolder(parent: ViewGroup,
                            resId: Int,
                            private val _EventSubject: Subject<Any>,
                            private val _Glide: RequestManager) :BaseItemViewHolder<ArticleItem>(parent, resId) {

    private val _TitleView: TextView by bindView(R.id.article_title)
    private val _DetailView: TextView by bindView(R.id.article_detail)
    private val _LogoView: ImageView by bindView(R.id.article_logo)

    init {

        this.itemView.setOnClickListener {
            onClick()
        }
    }

    private fun onClick (){
        val link = item?.link?:return

        _EventSubject.onNext(NewsClickedEvent(this, link))
    }

    override fun onBindItem(item: ArticleItem) {
        val oldItem = this.item

        if (oldItem == null || oldItem.title != item.title) {
            _TitleView.text = HtmlCompat.fromHtml(item.title,HtmlCompat.FROM_HTML_MODE_LEGACY)
        }

        if (oldItem == null || oldItem.desc != item.desc) {
            if(item.desc.isEmpty()){
                _DetailView.text = "..."
            }
            else{
                _DetailView.text = HtmlCompat.fromHtml(item.desc,HtmlCompat.FROM_HTML_MODE_LEGACY)
            }

        }

        if (oldItem == null || oldItem.thumb != item.thumb) {
            _Glide.load(item.thumb)
                .into(_LogoView)
        }

        super.onBindItem(item)
    }
}