package com.hitasanti.hitaapp.feature.categories.item

import android.graphics.Color
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.product.item.ProductItem


class CategoryItem(val products: List<ProductItem>, val categoryDetails: CategoryDetails ): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is CategoryItem && (other === this || (other.products == products && other.categoryDetails == categoryDetails))

}