package com.hitasanti.hitaapp.feature.editinfo

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class EditInfoScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is EditInfoScreen &&  (other === this)


    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<EditInfoScreen> = object : Parcelable.Creator<EditInfoScreen> {
            override fun createFromParcel(parcel: Parcel): EditInfoScreen {

                return EditInfoScreen( )
            }

            override fun newArray(size: Int) = arrayOfNulls<EditInfoScreen?>(size)
        }
    }
}