package com.hitasanti.hitaapp.feature.webtab

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen


class WebTabScreen(val url: String,
                   val swipeToClose: Boolean = true,
                   val showProgress: Boolean = true)
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(url)
        dest?.writeInt(if (swipeToClose) 1 else 0)
        dest?.writeInt(if (showProgress) 1 else 0)
    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is WebTabScreen && (other === this || (other.url == url &&
            other.swipeToClose == swipeToClose
            && other.showProgress == showProgress))

    override fun toString() = "WebTabScreen $url"

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<WebTabScreen> = object : Parcelable.Creator<WebTabScreen> {
            override fun createFromParcel(parcel: Parcel): WebTabScreen {
                val url = parcel.readString() ?: ""
                val swipeToClose = parcel.readInt() == 1
                val showProgress = parcel.readInt() == 1
                return WebTabScreen(url, swipeToClose, showProgress)
            }

            override fun newArray(size: Int) = arrayOfNulls<WebTabScreen?>(size)
        }
    }
}