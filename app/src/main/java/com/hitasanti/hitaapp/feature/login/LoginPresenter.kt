package com.hitasanti.hitaapp.feature.login

import com.facebook.AccessToken
import com.facebook.Profile
import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.data.EmptyUserException
import com.hitasanti.hitaapp.data.model.GoogleAuthResponse
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import java.io.EOFException


class LoginPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<LoginContract.View, LoginViewState>(), LoginContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private val _Items = ItemsResult()
    private var _LoginDisposable: Disposable? = null
    private var _RegisterDisposable: Disposable? = null
    private var _GetAuthCodeDisposable: Disposable? = null
    private var _GetUserInfoDisposable: Disposable? = null

    //endregion Private

    //region BasePresenter
    override fun onAttachView(view: LoginContract.View) {
        super.onAttachView(view)

    }

    override fun Login(username: String, password: String) {
        _LoginDisposable?.dispose()
        _LoginDisposable = _UseCaseFactory.get().Login(username, password)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                getUserInfo("Hita")
            }, {
                mView?.showLoginResult(false)
            })
    }

    override fun getGoogleAuthCode(authCode: String, clientId: String) {
        _GetAuthCodeDisposable?.dispose()
        _GetAuthCodeDisposable = _UseCaseFactory.get().getGoogleAuthCode(authCode, clientId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                mView?.showGoogleAuthCode(it)
            }, {
                mView?.showLoginResult(false, "Google")
            })
    }

    override fun facebookLoogin(accessToken: AccessToken, email: String, fbId: String) {
        _LoginDisposable?.dispose()
        _LoginDisposable = _UseCaseFactory.get().facebookLogin(accessToken.token, email)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                getUserInfo("Facebook")
            }, {
                if(it is EmptyUserException || it is EOFException){
                    register(fbId, email, User.FACEBOOK)
                }
                else{
                    mView?.showLoginResult(false,"Facebook")
                }
            })
    }

    private fun getUserInfo(service: String){
        val id = User.mId?: return
        _GetUserInfoDisposable?.dispose()
        _GetUserInfoDisposable = _UseCaseFactory.get().getUserInfo(id.toString())
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                User.mUserInfo = it
                User.save()
                mView?.showLoginResult(true,service)
            }, {
                User.logout()
                mView?.showLoginResult(false,service)
            })
    }

    override fun googleLogin(googleAuthResponse: GoogleAuthResponse) {
        _LoginDisposable?.dispose()
        _LoginDisposable = _UseCaseFactory.get().googleLogin(googleAuthResponse)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                getUserInfo("Google")
            }, {
                if(it is EmptyUserException || it is EOFException){
                    val userId = User.mGoogleAccount?.id
                    val email = User.mGoogleAccount?.email

                    if(userId != null && email != null){
                        register(userId, email, User.GOOGLE)
                    }
                    else{
                        mView?.showLoginResult(false,"Google")
                    }
                }
                else{
                    mView?.showLoginResult(false,"Google")
                }
            })
    }

    override fun register(userId: String, email: String, service: Int){
        _RegisterDisposable?.dispose()
        _RegisterDisposable = _UseCaseFactory.get().register(User.getRegisterRequest(userId, email))
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                if(service == User.FACEBOOK){
                    User.login("",email, Profile.getCurrentProfile()?.name.orEmpty(), it, service, Profile.getCurrentProfile()?.getProfilePictureUri(600,600).toString())
                    getUserInfo("Facebook")
                }
                else if (service == User.GOOGLE){
                    User.login("", email, User.mGoogleAccount?.displayName.orEmpty(), it, service, User.mGoogleAccount?.photoUrl?.toString().orEmpty())
                    getUserInfo("Google")
                }

            }, {
                mView?.showLoginResult(false,if(service == User.FACEBOOK) "Facebook" else "Google")
            })
    }

    override fun onDestroy() {
        _LoginDisposable?.dispose()
        _GetAuthCodeDisposable?.dispose()
        _GetUserInfoDisposable?.dispose()
        _RegisterDisposable?.dispose()
        super.onDestroy()
    }
    //endregion BasePresenter

}