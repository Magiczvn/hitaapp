package com.hitasanti.hitaapp.feature.webtab

interface WebTabContract {

    interface View {


    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, WebTabViewState> {


    }
}
