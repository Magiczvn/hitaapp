package com.hitasanti.hitaapp.feature.history.event

import com.hitasanti.hitaapp.repository.model.Order

class ReorderClickEvent(val order: Order)