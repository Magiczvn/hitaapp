package com.hitasanti.hitaapp.feature.cart.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item

class CartItem(val orders: List<OrderItem>, val tempPrice: Int, val shippingPrice: Int, val disCountPrice: Int): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is CartItem && (other === this)
}