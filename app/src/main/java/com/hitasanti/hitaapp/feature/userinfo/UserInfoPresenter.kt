package com.hitasanti.hitaapp.feature.userinfo

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.history.HistoryContract
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.Callable


class UserInfoPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<UserInfoContract.View, UserInfoViewState>(), UserInfoContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }

    //endregion Private

    override fun onAttachView(view: UserInfoContract.View) {
        super.onAttachView(view)

    }


    override fun onDestroy() {
        super.onDestroy()


    }
    //endregion BasePresenter

}