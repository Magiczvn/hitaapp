package com.hitasanti.hitaapp.feature.cart.item


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item

class StatusItem(val status: String, val reason: String): Item {

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is StatusItem && (other === this)
}