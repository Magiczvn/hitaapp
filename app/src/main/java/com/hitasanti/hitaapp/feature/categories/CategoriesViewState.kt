package com.hitasanti.hitaapp.feature.categories


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.mvp.BaseViewState
import com.hitasanti.hitaapp.repository.model.Product

class CategoriesViewState(val screen: CategoriesScreen)
    : BaseViewState() {

    var currentTab = 1
    var productsMap: HashMap<Int, List<Product>>? = null
    var items: List<Item>? = null

}