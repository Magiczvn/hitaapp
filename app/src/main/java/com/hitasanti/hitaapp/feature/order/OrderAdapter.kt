package com.hitasanti.hitaapp.feature.order


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.hitasanti.hitaapp.app.adapter.viewpager.BetterFragmentStatePagerAdapter
import com.hitasanti.hitaapp.feature.product.ProductFragment
import com.hitasanti.hitaapp.feature.product.ProductScreen
import com.hitasanti.hitaapp.mvp.Screen


class OrderAdapter(fm: FragmentManager,
                            private var _Screens: List<Screen>)
    : BetterFragmentStatePagerAdapter(fm) {

    private var _OldScreens = _Screens

    fun setScreens(screens: List<Screen>) {
        _OldScreens = _Screens
        _Screens = screens
        notifyDataSetChanged()
    }

    fun getScreen(index: Int): Screen? = when {
        index < 0 -> null
        index > _Screens.size - 1 -> null
        else -> _Screens[index]
    }

    fun indexOf(categoryId: Int) = _Screens.indexOfFirst {
        when {
            it is ProductScreen && it.categoryId == categoryId -> true
            else -> false
        }
    }

    val savedStates: Collection<Fragment.SavedState> get() = mSavedState.values

    override fun getItem(position: Int): Fragment {
        val screen = _Screens[position]
        return when (screen) {
            is ProductScreen -> ProductFragment.instance(screen)
            else -> throw RuntimeException("Not support screen type $screen")
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        val screen = _Screens[position]
        return when (screen) {
            is ProductScreen -> screen.title
            else -> throw RuntimeException("Not support screen type $screen")
        }
    }

    override fun getCount() = _Screens.size

    override fun getId(position: Int, fragment: Fragment?) = when (fragment) {
        is ProductFragment -> fragment.screen.categoryId.toString()
        else -> {
            val screen = _Screens[position]
            when (screen) {
                is ProductScreen -> screen.categoryId.toString()

                else -> throw RuntimeException("Not support screen type $screen")
            }
        }
    }

    override fun getItemPosition(`object`: Any) = when (`object`) {
        is ProductFragment -> {
            if (_OldScreens.indexOf(`object`.screen) == _Screens.indexOf(`object`.screen)) {
                POSITION_UNCHANGED
            } else {
                POSITION_NONE
            }
        }
        else -> POSITION_NONE
    }
}