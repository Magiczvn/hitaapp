package com.hitasanti.hitaapp.feature.history


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.mvp.BaseViewState
import com.hitasanti.hitaapp.repository.model.Category
import com.hitasanti.hitaapp.repository.model.Order
import com.hitasanti.hitaapp.repository.model.Product

class HistoryViewState(val screen: HistoryScreen)
    : BaseViewState() {

    var histories: List<Order>? = null
    var items: List<Item>? = null

}