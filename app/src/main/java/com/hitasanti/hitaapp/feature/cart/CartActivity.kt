package com.hitasanti.hitaapp.feature.cart

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.cart.event.CartItemClickEvent
import com.hitasanti.hitaapp.feature.cart.event.NewOrderEvent
import com.hitasanti.hitaapp.feature.confirmdialog.ConfirmDialogFragment
import com.hitasanti.hitaapp.feature.confirmdialog.ConfirmDialogScreen
import com.hitasanti.hitaapp.feature.home.event.OpenOrderPageEvent
import com.hitasanti.hitaapp.feature.login.LoginActivity
import com.hitasanti.hitaapp.feature.login.LoginScreen
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.feature.login.event.LoginSuccessEvent
import com.hitasanti.hitaapp.feature.order.event.CartUpdateEvent
import com.hitasanti.hitaapp.feature.productdialog.ProductDialogFragment
import com.hitasanti.hitaapp.feature.productdialog.ProductDialogScreen
import com.hitasanti.hitaapp.mvp.BaseSwipeMvpActivity
import com.hitasanti.hitaapp.repository.model.Coupon
import com.hitasanti.hitaapp.repository.model.Order
import com.hitasanti.hitaapp.view.NumberUtil
import com.hitasanti.hitaapp.view.ViewUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.cart_activity.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class CartActivity : BaseSwipeMvpActivity<CartContract.View, CartContract.Presenter, CartViewState, CartScreen>(),
    HasComponent<CartComponent>, CartContract.View, ProductDialogFragment.Listener, ConfirmDialogFragment.Listener {
    companion object {

        fun instance(context: Context, screen: CartScreen): Intent {
            val intent = Intent(context, CartActivity::class.java)
            setScreen(intent, screen)
            return intent
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _Adapter: CartAdapter

    //endregion Dependency

    //region Private
    private var _Disposable: CompositeDisposable? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context) = component.presenter

    override fun onCreateViewState(context: Context) = CartViewState(screen)

    override val viewStateTag: String get() = CartViewState::class.java.name

    override val layoutResource get() = R.layout.cart_activity

    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(this).component.plus(CartModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        cart_rv?.adapter = _Adapter

        cart_rv?.let {
            ViewUtil.doOnGlobalLayout(it) {
                _Adapter.setReady(true)
            }
        }
        cart_rv?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        enableSwipe(true)


        _Disposable = CompositeDisposable(_Adapter.event
            .throttleFirst(BuildConfig.BUTTON_DELAY, TimeUnit.MILLISECONDS)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                when (it) {
                    is CartItemClickEvent ->{
                        showProduct(it)
                    }
                }
            }, ErrorConsumer()),
            _Bus.register(LoginSuccessEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    presenter.updateCartAddress()
                }, ErrorConsumer())
        )

        btn_coupon?.setOnClickListener {
            onCouponClick()
        }

        order_button?.setOnClickListener {
            showConfirmDialog()
        }

        updateTotalPrice()
        setupUI(root_view)

    }

    override fun showOrderResult(result: Boolean, order: Order?) {
        if(result) {
            val ord = order?:return
            val toast = Toast.makeText(
                applicationContext,
                "Đơn hàng #${ord.id} đã được đặt thành công!",
                Toast.LENGTH_LONG
            )
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
            Cart.clearAllProducts()
            _Bus.post(CartUpdateEvent())
            _Bus.post(NewOrderEvent(ord))
            _Bus.post(OpenOrderPageEvent(this))
            Cart.coupon = null
            finish()
        }
        else{
            txtPlaceOrder.visibility = View.VISIBLE
            loading_pv.visibility = View.INVISIBLE
            order_button.isClickable = true
            val toast = Toast.makeText(
                applicationContext,
                "Đặt hàng không thành công!",
                Toast.LENGTH_LONG
            )
            toast.setGravity(Gravity.CENTER, 0, 0)
            toast.show()
        }
    }

    override fun onConfirm() {
        val address = presenter.getAddressItem()?:return
        txtPlaceOrder.visibility = View.INVISIBLE
        loading_pv.visibility = View.VISIBLE
        order_button.isClickable = false
        presenter.placeOrder(address.notes, address.name)
    }

    override fun onChange() {

    }

    fun hideSoftKeyboard() {
        val inputMethodManager: InputMethodManager = getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            currentFocus?.windowToken, 0
        )
    }

    fun setupUI(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { v, event ->
                hideSoftKeyboard()
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView)
            }
        }
    }


    override fun finish() {
        val address = presenter.getAddressItem()
        val phone = address?.phone
        val add = address?.address

        Cart.shippingAddress = add.orEmpty()
        Cart.shippingPhone = phone.orEmpty()

        Cart.coupon = null
        Cart.couponResult = null

        super.finish()
    }


    private fun showProduct(event: CartItemClickEvent){
        val id = event.productId
        val product = Cart.productsMap[id]?: return
        val variation = Cart.variationsMap[event.variationId]

        val quantity = if(variation!= null) Cart.cartMap[variation.id]?:return else Cart.cartMap[id]?:return

        val image = product.imgages?.getOrNull(0)?.link.orEmpty()

        val productDialogScreen = ProductDialogScreen( id = id,
            price = product.price,
            shortDescription = product.shortDescription,
            name = product.name,
            imageUrl = image,
            quantity = quantity,
            variationId = variation?.id,
            canRemove = true)
        ProductDialogFragment.instance(productDialogScreen)
            .show(supportFragmentManager)
    }

    private fun showLogin(){
        val intent = LoginActivity.instance(this, LoginScreen())
        startActivity(intent)
    }

    private fun showConfirmDialog(){
        val isLoggedIn = User.isLoggedIn()
        if(!isLoggedIn){
            showLogin()
        }
        else{
            val address = presenter.getAddressItem()?:return
            val name = address.name
            val phone = address.phone
            val add = address.address

            if(name.isEmpty() || phone.isEmpty() || add.isEmpty()){
                val toast = Toast.makeText(this, "Bạn chưa nhập đủ thông tin", Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.CENTER,0,0)
                toast.show()
                return
            }

            if(!NumberUtil.isValidPhoneNumber(phone)){
                val toast = Toast.makeText(this, "Số điện thoại không hợp lệ", Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.CENTER,0,0)
                toast.show()
                return
            }

            Cart.shippingAddress = add
            Cart.shippingPhone = phone

            val confirmScreen = ConfirmDialogScreen(name = address.name,
                price = Cart.getFinalPrice(),
                address = address.address,
                phone = address.phone)
            ConfirmDialogFragment.instance(confirmScreen)
                .show(supportFragmentManager)
        }
    }

    private fun updateTotalPrice(){
        cart_cash_price?.text = NumberUtil.formatPrice(this, Cart.getFinalPrice())
        cart_price?.text = cart_cash_price.text
    }

    var couponDialog: MaterialDialog? = null
    private fun onCouponClick(){
         couponDialog = MaterialDialog.Builder(this)
            .title(R.string.txt_coupon)
            .positiveText(R.string.txt_apply)
            .theme(Theme.LIGHT)
             .contentColor(Color.RED)
            .input(null, null, true, MaterialDialog.InputCallback { dialog, input ->
                val coupon_input = input.toString()
                if(coupon_input.isNotEmpty()){
                    dialog.getActionButton(DialogAction.POSITIVE).isEnabled = false
                    dialog.setContent("")
                    presenter.getCoupon(coupon_input)
                }
                else{
                    dialog.dismiss()
                }
            })
            .autoDismiss(false)
            .onNegative { dialog, which ->
                dialog.dismiss()
            }
            .negativeText(R.string.txt_no)
            .show()
        couponDialog?.inputEditText?.setTextColor(Color.BLACK)

    }

    override fun showCouponResult(coupon: Coupon?) {
        if(coupon == null) {
            couponDialog?.getActionButton(DialogAction.POSITIVE)?.isEnabled = true
             couponDialog?.setContent("Không tìm thấy coupon")
        }
        else {

            val result = Cart.applyCoupon(coupon)
            if(result.result) {
                couponDialog?.dismiss()
                couponDialog = null
                showDialog("Đã thêm code '${coupon.code}' vào đơn hàng")
                Cart.coupon = coupon
                Cart.couponResult = result
                updateTotalPrice()
                presenter.updateCart()
            }else{
                couponDialog?.getActionButton(DialogAction.POSITIVE)?.isEnabled = true
                couponDialog?.setContent(result.message)
            }
        }
    }

    private fun showDialog(message: String){
        MaterialDialog.Builder(this)
            .theme(Theme.LIGHT)
            .content(message)
            .positiveText(R.string.txt_yes)
            .show()
    }

    override fun onAddToCart(productId: Int, quantity: Int, isVariation: Boolean) {
        if(!isVariation) {
            val product = Cart.productsMap[productId] ?: return
            Cart.addProduct(product, quantity)
        }
        else{
            val variation = Cart.variationsMap[productId]?:return
            val product = Cart.productsMap[variation.productId]?:return
            Cart.addProduct(variation, quantity)
        }
        if(Cart.coupon != null){
            val couponResult = Cart.applyCoupon(Cart.coupon!!)
            if(!couponResult.result){
                Cart.coupon = null
                Cart.couponResult = null
                showDialog(couponResult.message.orEmpty())
            }
            else{
                Cart.couponResult = couponResult
            }
        }
        updateTotalPrice()
        _Bus.post(CartUpdateEvent())
        presenter.updateCart()
    }



    override fun onUpdateCart(productId: Int, quantity: Int, isVariation: Boolean) {
        if(!isVariation) {
            val product = Cart.productsMap[productId] ?: return
            Cart.updateProduct(product, quantity)
        }
        else{
            val variation = Cart.variationsMap[productId]?:return
            Cart.updateProduct(variation, quantity)
        }
        Cart.saveProducts()

        _Bus.post(CartUpdateEvent())
        if(Cart.totalItems == 0)
        {
            _Bus.post(OpenOrderPageEvent(this))
            finish()
        }
        else {
            if(Cart.coupon != null){
                val couponResult = Cart.applyCoupon(Cart.coupon!!)
                if(!couponResult.result){
                    Cart.coupon = null
                    Cart.couponResult = null
                    showDialog(couponResult.message.orEmpty())
                }
                else{
                    Cart.couponResult = couponResult
                }
            }
            updateTotalPrice()
            presenter.updateCart()
        }

    }

    override fun onCancel() {

    }

    override fun showItems(items: List<Item>) {
        _Adapter.updateItems(items)
    }

    override fun onDestroy() {
        _Disposable?.dispose()

        super.onDestroy()
    }

}