package com.hitasanti.hitaapp.feature.webtab
import com.hitasanti.hitaapp.dependency.ActivityScope

import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [WebTabModule::class])
interface WebTabComponent {

    val presenter: WebTabContract.Presenter

    fun inject(fragment: WebTabFragment)
}
