package com.hitasanti.hitaapp.feature.userinfo

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.feature.cart.item.AddressItem

interface UserInfoContract {

    interface View {

    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, UserInfoViewState> {

    }
}