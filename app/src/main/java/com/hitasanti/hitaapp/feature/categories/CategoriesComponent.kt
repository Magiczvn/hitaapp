package com.hitasanti.hitaapp.feature.categories

import com.hitasanti.hitaapp.dependency.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [CategoriesModule::class])
interface CategoriesComponent {
    val presenter: CategoriesContract.Presenter

    fun inject(fragment: CategoriesFragment)
}
