package com.hitasanti.hitaapp.feature.store

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.hitasanti.hitaapp.app.adapter.viewpager.BetterFragmentStatePagerAdapter
import com.hitasanti.hitaapp.mvp.Screen


class StoreAdapter(fm: FragmentManager,
                   private var _Screens: List<Screen>)
    : BetterFragmentStatePagerAdapter(fm) {

    private var _OldScreens = emptyList<Screen>()

    fun getScreen(index: Int) = when {
        index < 0 -> null
        index > _Screens.size - 1 -> null
        else -> _Screens[index]
    }

    override fun getItem(position: Int): Fragment {
        val screen = _Screens[position]
        return when (screen) {
            //is UserZoneScreen -> UserZoneFragment.instance(screen)
            //is HomeScreen -> HomeFragment.instance(screen)
            else -> throw RuntimeException("Not support screen type $screen")
        }
    }

    override fun getCount() = _Screens.size

    override fun getId(position: Int, fragment: Fragment?) = when (fragment) {
       // is HomeFragment -> "home"
        // UserZoneFragment -> "userzone"
        else -> {
            val screen = _Screens[position]
            when (screen) {
               // is HomeScreen -> "home"
                //is UserZoneScreen -> "userzone"
                else -> throw RuntimeException("Not support screen type $screen")
            }
        }
    }

    override fun getItemPosition(`object`: Any) = when (`object`) {
        /*is HomeFragment -> {
            if (_OldScreens.indexOf(`object`.screen) == _Screens.indexOf(`object`.screen)) {
                POSITION_UNCHANGED
            } else {
                POSITION_NONE
            }
        }
        is UserZoneFragment -> {
            if (_OldScreens.indexOf(`object`.screen) == _Screens.indexOf(`object`.screen)) {
                POSITION_UNCHANGED
            } else {
                POSITION_NONE
            }
        }
        else -> POSITION_NONE*/
        else-> POSITION_NONE
    }
}