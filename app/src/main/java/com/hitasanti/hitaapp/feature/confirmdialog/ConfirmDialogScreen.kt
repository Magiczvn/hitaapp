package com.hitasanti.hitaapp.feature.confirmdialog


import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class ConfirmDialogScreen(val name: String, val price:Int, val address: String, val phone: String) : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(name)
        dest?.writeInt(price)
        dest?.writeString(address)
        dest?.writeString(phone)

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is ConfirmDialogScreen && (other === this || (other.name == name &&
            other.price == price &&
            other.address == address &&
            other.phone == phone))

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ConfirmDialogScreen> = object : Parcelable.Creator<ConfirmDialogScreen> {
            override fun createFromParcel(parcel: Parcel): ConfirmDialogScreen {
                val name = parcel.readString().orEmpty()
                val price = parcel.readInt()
                val address = parcel.readString().orEmpty()
                val phone = parcel.readString().orEmpty()


                return ConfirmDialogScreen(name, price, address, phone)
            }

            override fun newArray(size: Int) = arrayOfNulls<ConfirmDialogScreen?>(size)
        }
    }
}