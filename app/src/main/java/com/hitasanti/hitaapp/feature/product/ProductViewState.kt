package com.hitasanti.hitaapp.feature.product


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.mvp.BaseViewState
import com.hitasanti.hitaapp.repository.model.Product

class ProductViewState(val screen: ProductScreen)
    : BaseViewState() {

    var products:List<Product>? = null
    var items: List<Item>? = null

}