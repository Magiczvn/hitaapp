package com.hitasanti.hitaapp.feature.cart.viewholder


import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import io.reactivex.subjects.Subject
import kotterknife.bindView

class AddressItemViewHolder(parent: ViewGroup,
                          resId: Int,
                          private val _EventSubject: Subject<Any>) :BaseItemViewHolder<AddressItem>(parent, resId) {
    private val _Name: EditText by bindView(R.id.txt_shipping_name)
    private val _Phone: EditText by bindView(R.id.txt_shipping_phone)
    private val _Address: EditText by bindView(R.id.txt_shipping_address)
    private val _Notes: EditText by bindView(R.id.txt_shipping_notes)

    init {
        _Name.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                item?.name = p0?.toString().orEmpty()
            }
        })

        _Phone.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                item?.phone = p0?.toString().orEmpty()
            }
        })

        _Address.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                item?.address = p0?.toString().orEmpty()
            }
        })

        _Notes.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                item?.notes = p0?.toString().orEmpty()
            }
        })
    }

    override fun onBindItem(item: AddressItem) {

        _Name.setText(item.name)
        _Phone.setText(item.phone)
        _Address.setText(item.address)
        _Notes.setText(item.notes)

        super.onBindItem(item)
    }
}