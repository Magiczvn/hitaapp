package com.hitasanti.hitaapp.feature.login

import com.facebook.AccessTokenManager
import com.facebook.Profile
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.hitasanti.hitaapp.data.model.RegisterRequest
import com.hitasanti.hitaapp.repository.model.UserInfo
import io.paperdb.Paper
import kotlin.random.Random


object User {
    private const val USER = "user"
    private const val TOKEN = "TOKEN"
    private const val EMAIL = "EMAIL"
    private const val NAME = "NAME"
    private const val ID = "ID"
    private const val SERVICE = "SERVICE"
    private const val AVATAR = "AVATAR"
    private const val USERINFO = "USERINFO"

    const val HITA = 0
    const val GOOGLE = 1
    const val FACEBOOK = 2


    private val _Book = Paper.book(USER)
    var mToken: String? = null
    var mEmail: String? = null
    var mName: String? = null
    var mId: Int? = null
    var mService: Int? = null
    var mAvatarUrl: String? = null
    var mGoogleAccount: GoogleSignInAccount? = null
    var mUserInfo: UserInfo? = null

    init {

    }

    fun isLoggedIn(): Boolean{
        return  (!mToken.isNullOrEmpty() && !mEmail.isNullOrEmpty() && !mName.isNullOrEmpty()&& mService == HITA)
                ||(mService == FACEBOOK && mId != null && !mName.isNullOrEmpty())
                ||(mService == GOOGLE && mId != null && !mName.isNullOrEmpty())
    }

    fun getAvatarUrl():String?{
        return if(mService == HITA)
            mUserInfo?.avatar_url?: mAvatarUrl
        else mAvatarUrl
    }

    fun getUserName(): String{
        val first_name = mUserInfo?.first_name.orEmpty()
        val last_name = mUserInfo?.last_name.orEmpty()

        if(first_name.isEmpty() && last_name.isEmpty())
            return mName.orEmpty()
        return "$last_name $first_name"
    }

    val template = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#\$%^&*"
    val len = template.length

    fun getRegisterRequest(userId: String, email: String): RegisterRequest {
        val buffer = StringBuffer()
        val rd = Random(System.currentTimeMillis())
        while (buffer.length < 12){
            buffer.append(template[rd.nextInt(len)])
        }
        val userName =  email.split('@')[0] + '_' + userId
        return RegisterRequest(userName,  email, buffer.toString())
    }

    fun logout(){
        LoginManager.getInstance().logOut()

        mName = null
        mEmail = null
        mToken = null
        mId = null
        mService = null
        mGoogleAccount = null
        mAvatarUrl = null
        mUserInfo = null

        save()
    }

    fun login(token: String, email: String, name: String, id: Int, service: Int, avatarUrl: String?){
        mToken = token
        mEmail = email
        mName = name
        mId = id
        mService = service
        mAvatarUrl = avatarUrl
        mGoogleAccount = null
        save()
    }

    fun save(){
        try {
            _Book.write(TOKEN, mToken.orEmpty())
            _Book.write(EMAIL, mEmail.orEmpty())
            _Book.write(NAME, mName.orEmpty())
            _Book.write(AVATAR, mAvatarUrl.orEmpty())

            _Book.write(ID, mId?:0)
            _Book.write(SERVICE, mService?:0)
            if(mUserInfo != null)
                _Book.write(USERINFO, mUserInfo)
            else
                _Book.delete(USERINFO)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun load(){
        try {
            mToken = _Book.read<String>(TOKEN)
            mEmail = _Book.read<String>(EMAIL)
            mName = _Book.read<String>(NAME)
            mAvatarUrl = _Book.read<String>(AVATAR)
            mId = _Book.read<Int>(ID)
            mService = _Book.read<Int>(SERVICE)?: HITA
            mUserInfo = _Book.read<UserInfo>(USERINFO)
        } catch (ex: Exception) {
        }
    }
}