package com.hitasanti.hitaapp.feature.web

interface WebContract {

    interface View {


    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, WebViewState> {

    }
}
