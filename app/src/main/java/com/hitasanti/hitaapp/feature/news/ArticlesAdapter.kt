package com.hitasanti.hitaapp.feature.news

import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemAdapter
import com.hitasanti.hitaapp.feature.news.item.ArticleItem
import com.hitasanti.hitaapp.feature.news.viewholder.ArticleItemViewHolder
import io.reactivex.subjects.Subject

class ArticlesAdapter( private val _Glide: RequestManager, private val _EventSubject: Subject<Any>)
    : BaseItemAdapter() {

    companion object {
        private const val ITEM_ARTICLE = 0
    }

    init {
        setReady(true)
    }


    override fun getItemViewType(position: Int): Int {
        val item = getItemAt(position)
        return when (item) {
            is ArticleItem -> ITEM_ARTICLE
            else -> throw RuntimeException("Not support item $item")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        ITEM_ARTICLE -> ArticleItemViewHolder(parent, R.layout.article_item_viewholder, _EventSubject, _Glide)
        else -> throw RuntimeException("Not support type $viewType")
    }

}