package com.hitasanti.hitaapp.feature.categories

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class CategoriesScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is CategoriesScreen && (other === this)

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CategoriesScreen> = object : Parcelable.Creator<CategoriesScreen> {
            override fun createFromParcel(parcel: Parcel): CategoriesScreen {
                return CategoriesScreen()
            }

            override fun newArray(size: Int) = arrayOfNulls<CategoriesScreen?>(size)
        }
    }
}