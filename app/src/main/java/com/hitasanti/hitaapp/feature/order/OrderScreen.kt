package com.hitasanti.hitaapp.feature.order

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class OrderScreen(val categoryId: Int)
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(categoryId)
    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is OrderScreen && (other === this)

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<OrderScreen> = object : Parcelable.Creator<OrderScreen> {
            override fun createFromParcel(parcel: Parcel): OrderScreen {
                val categoryId = parcel.readInt()
                return OrderScreen(categoryId)
            }

            override fun newArray(size: Int) = arrayOfNulls<OrderScreen?>(size)
        }
    }
}