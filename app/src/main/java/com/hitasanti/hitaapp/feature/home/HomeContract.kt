package com.hitasanti.hitaapp.feature.home



interface HomeContract {

    interface View {


    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, HomeViewState> {
        var currentTab: Int

    }
}