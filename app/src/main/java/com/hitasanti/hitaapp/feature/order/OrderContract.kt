package com.hitasanti.hitaapp.feature.order

import com.hitasanti.hitaapp.repository.model.Category

interface OrderContract {

    interface View {
        fun showCategories(categories: List<Category>)
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, OrderViewState> {
        val isEmpty:Boolean
    }
}