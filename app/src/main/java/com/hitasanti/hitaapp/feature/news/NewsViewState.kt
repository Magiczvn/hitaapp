package com.hitasanti.hitaapp.feature.news


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.mvp.BaseViewState
import com.hitasanti.hitaapp.repository.model.Article

class NewsViewState(val screen: NewsScreen)
    : BaseViewState() {

    var currentTab = 1
    var articles:List<Article>? = null
    var offers:List<Article>? = null
    var items: List<Item>? = null

}