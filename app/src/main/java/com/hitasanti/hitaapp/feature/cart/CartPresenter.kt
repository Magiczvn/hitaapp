package com.hitasanti.hitaapp.feature.cart

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.cart.Cart
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.Callable


class CartPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>,
                     private val _ItemBuilder: Lazy<NewsItemBuilder>)
    : BasePresenter<CartContract.View, CartViewState>(), CartContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private val _Items = ItemsResult()

    private var _GetProductsDisposable: Disposable? = null
    private var _OrderDisposable: Disposable? = null
    private var _ShowProductsDisposable: Disposable? = null

    //endregion Private

    override fun updateCart() {
        val work = Callable<Unit> {
            val oldItems = mViewState.items.orEmpty()
            val newItems = _ItemBuilder.get().addCart(oldItems).orEmpty()
            mViewState.items = newItems
            _Items.pushItems(newItems)
        }

        _ShowProductsDisposable?.dispose()
        _ShowProductsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
            }, ErrorConsumer())
    }

    override fun updateCartAddress() {
        val work = Callable<Unit> {
            val oldItems = mViewState.items.orEmpty()

            val newItems = _ItemBuilder.get().updateCartAddress(oldItems).orEmpty()
            mViewState.items = newItems
            mViewState.addressItem = newItems.getOrNull(0) as AddressItem?
            _Items.pushItems(newItems)
        }

        _ShowProductsDisposable?.dispose()
        _ShowProductsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
            }, ErrorConsumer())
    }

    override fun placeOrder(shippingNote: String, shippingName: String) {
        _OrderDisposable?.dispose()
        _OrderDisposable = _UseCaseFactory.get().placeOrder(shippingNote, shippingName)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                val order = it.value
                if(order == null){
                    mView?.showOrderResult(false, null)
                }
                else{
                    mView?.showOrderResult(true, order)
                }
            }, {
                mView?.showOrderResult(false, null)
            })
    }

    override fun getCoupon(code: String) {
        _OrderDisposable?.dispose()
        _OrderDisposable = _UseCaseFactory.get().getCoupon(code)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe({
                mView?.showCouponResult(it)
            }, {
                mView?.showCouponResult(null)
            })
    }



    fun addCartAddress() {
        val oldItems = mViewState.items.orEmpty()
        mViewState.items = _ItemBuilder.get().addCartAddress(oldItems)
        mViewState.addressItem = mViewState.items?.getOrNull(0) as AddressItem?
    }

    override fun getAddressItem(): AddressItem? {
        return mViewState.addressItem
    }

    private fun showItemResult() {
        val items = _Items.popItems()

        items?.let { mView?.showItems(it) }
    }

    //region BasePresenter
    override fun onAttachView(view: CartContract.View) {
        super.onAttachView(view)
        mViewState.items?.let {
            view.showItems(it)
        }

        addCartAddress()
        updateCart()
    }



    override fun onDestroy() {
        super.onDestroy()
        _GetProductsDisposable?.dispose()
        _ShowProductsDisposable?.dispose()
        _OrderDisposable?.dispose()

    }
    //endregion BasePresenter

}