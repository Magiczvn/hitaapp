package com.hitasanti.hitaapp.feature.categories

import android.app.Application
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.FragmentScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class CategoriesModule(private val _Fragment: CategoriesFragment) {
    @Provides
    @FragmentScope
    internal fun provideNewsItemBuilder(application: Application,
                                               bus: RxBus): NewsItemBuilder = NewsItemBuilder(application,bus)

    @Provides
    @FragmentScope
    internal fun provideGlide(): RequestManager = GlideApp.with(_Fragment)

    @Provides
    @FragmentScope
    internal fun provideAdapter(glide: RequestManager): CategoriesAdapter = CategoriesAdapter(glide)

    @Provides
    @FragmentScope
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>,
                                newsItemBuilder: Lazy<NewsItemBuilder>): CategoriesContract.Presenter = CategoriesPresenter(useCaseFactory, schedulerFactory, newsItemBuilder)
}
