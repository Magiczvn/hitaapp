package com.hitasanti.hitaapp.feature.order

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.Callable


class OrderPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                     private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<OrderContract.View, OrderViewState>(), OrderContract.Presenter {

    //region Private

    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }

    override val isEmpty: Boolean
        get() = mViewState.categories.isNullOrEmpty()

    private var _GetProductsDisposable: Disposable? = null
    private var _ShowProductsDisposable: Disposable? = null

    //endregion Private


    //region BasePresenter
    override fun onAttachView(view: OrderContract.View) {
        super.onAttachView(view)
        getCategories()
    }

    private fun getCategories() {
        if(mViewState.categories != null) return
        _GetProductsDisposable?.dispose()
        _GetProductsDisposable = _UseCaseFactory.get().getCategories(mViewState.screen.categoryId)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_SchedulerFactory.get().main())
            .subscribe( Consumer {
                mViewState.categories = it
                mView?.showCategories(it)
            }, ErrorConsumer())
    }

    override fun onDestroy() {
        super.onDestroy()
        _GetProductsDisposable?.dispose()
        _ShowProductsDisposable?.dispose()

    }
    //endregion BasePresenter

}