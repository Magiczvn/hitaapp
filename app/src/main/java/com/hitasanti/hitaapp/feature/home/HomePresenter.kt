package com.hitasanti.hitaapp.feature.home

import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler

class HomePresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                    private val _SchedulerFactory: Lazy<SchedulerFactory>)
    : BasePresenter<HomeContract.View, HomeViewState>(), HomeContract.Presenter {

    //region Private
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }

    override var currentTab
        get() = mViewState.currentTab
        set(value) {
            mViewState.currentTab = value
        }

    //endregion Private

    //region BasePresenter
    override fun onAttachView(view: HomeContract.View) {
        super.onAttachView(view)

    }

    override fun onDestroy() {
        super.onDestroy()


    }
    //endregion BasePresenter

}