package com.hitasanti.hitaapp.feature.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckedTextView
import androidx.viewpager.widget.ViewPager
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.event.ForegroundTabEvent
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.categories.CategoriesScreen
import com.hitasanti.hitaapp.feature.home.event.OpenHomePageEvent
import com.hitasanti.hitaapp.feature.home.event.OpenOrderPageEvent
import com.hitasanti.hitaapp.feature.home.event.OpenSettingPageEvent
import com.hitasanti.hitaapp.feature.home.event.OpenStorePageEvent
import com.hitasanti.hitaapp.feature.news.NewsScreen
import com.hitasanti.hitaapp.feature.setting.SettingScreen
import com.hitasanti.hitaapp.feature.store.StoreScreen

import com.hitasanti.hitaapp.mvp.BaseMvpFragment
import com.hitasanti.hitaapp.mvp.MvpCleaner
import com.hitasanti.hitaapp.view.HomeTabView
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.home_fragment.*
import javax.inject.Inject

class HomeFragment : BaseMvpFragment<HomeContract.View, HomeContract.Presenter, HomeViewState, HomeScreen>(),
        HasComponent<HomeComponent>, HomeContract.View {

    companion object {
        private const val TAB_NEWS = "tab_news"
        private const val TAB_ORDER = "tab_order"
        private const val TAB_STORE = "tab_store"
        private const val TAB_SETTING = "tab_setting"

        fun instance(screen: HomeScreen): HomeFragment {
            val fragment = HomeFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>

    //endregion Dependency

    //region Bind View

    //endregion Bind View

    //region Private
    private var _Adapter: HomeAdapter? = null
    private var _Disposable: Disposable? = null
    private var _MvpCleaner: MvpCleaner? = null
    private var screenSize = intArrayOf(1080, 1920)
    private var _SettingIndex: Int? = null
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context?) = component.presenter

    override fun onCreateViewState(context: Context?) = HomeViewState()

    override val viewStateTag: String get() = HomeViewState::class.java.name

    override val layoutResource get() = R.layout.home_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(context!!).component.plus(HomeModule(this))
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _MvpCleaner = MvpCleaner(cacheFactory.presenterCache, cacheFactory.viewStateCache)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component.inject(this)

        home_vp?.addOnPageChangeListener(OnPageChangeListener())

        home_mtv?.setOnCurrentTabChangedListener(OnCurrentTabChangedListener())

        _Disposable = CompositeDisposable(_Bus.register(OpenOrderPageEvent::class.java)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                home_vp?.setCurrentItem(1, false)
            }, ErrorConsumer()),
            _Bus.register(OpenHomePageEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    home_vp?.setCurrentItem(0, false)
                }, ErrorConsumer()),
            _Bus.register(OpenStorePageEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    home_vp?.setCurrentItem(2, false)
                }, ErrorConsumer()),
            _Bus.register(OpenSettingPageEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    home_vp?.setCurrentItem(3, false)
                }, ErrorConsumer()))

        showTabs()
        super.onViewCreated(view, savedInstanceState)
    }


    override fun onDestroyView() {
        _Disposable?.dispose()

        cleanAllPresenter()

        super.onDestroyView()
    }

    override fun onPause() {

        super.onPause()
    }

    override fun onResume() {
        super.onResume()

    }
    //endregion Lifecycle

     private fun showTabs() {

        val screens = mutableListOf(NewsScreen(),
                CategoriesScreen(),
                StoreScreen(),
                SettingScreen())

        _Adapter = HomeAdapter(childFragmentManager, screens)
        home_vp?.adapter = _Adapter
        home_vp?.offscreenPageLimit = screens.size - 1
        if (home_vp?.currentItem != presenter.currentTab) {
            home_vp?.post {
                home_vp?.currentItem = presenter.currentTab
            }
        } else {
            home_vp?.post {
                _Bus.post(ForegroundTabEvent(screens[presenter.currentTab], this))
            }
        }

        home_mtv?.removeAllViews()
        val layoutInflater = LayoutInflater.from(context)
        screens.forEachIndexed { index, screen ->
            when (screen) {
                is NewsScreen -> {
                    val tabItem = layoutInflater.inflate(R.layout.item_home_tab_news, home_mtv, false)
                    val tvChecked = tabItem.findViewById<CheckedTextView>(R.id.item_home_tab_tv)
                    tvChecked.text = getString(R.string.home_tab_news)
                    tvChecked.isChecked = true
                    tabItem.tag = TAB_NEWS
                    tabItem.id = index
                    home_mtv?.addView(tabItem)
                }
                is CategoriesScreen -> {
                    val tabItem = layoutInflater.inflate(R.layout.item_home_tab_order, home_mtv, false)
                    val tvChecked = tabItem.findViewById<CheckedTextView>(R.id.item_home_tab_tv)
                    tvChecked.text = getString(R.string.home_tab_order)
                    tabItem.tag = TAB_ORDER
                    tabItem.id = index
                    home_mtv?.addView(tabItem)
                }

                is StoreScreen -> {
                    val tabItem = layoutInflater.inflate(R.layout.item_home_tab_store, home_mtv, false)
                    val tvChecked = tabItem.findViewById<CheckedTextView>(R.id.item_home_tab_tv)

                    tvChecked.text = getString(R.string.home_tab_store)
                    tabItem.tag = TAB_STORE
                    tabItem.id = index
                    home_mtv?.addView(tabItem)
                }

                is SettingScreen -> {
                    val tabItem = layoutInflater.inflate(R.layout.item_home_tab_setting, home_mtv, false)
                    val tvChecked = tabItem.findViewById<CheckedTextView>(R.id.item_home_tab_tv)

                    tvChecked.text = getString(R.string.home_tab_setting)

                    tabItem.tag = TAB_SETTING
                    tabItem.id = index
                    _SettingIndex = index
                    home_mtv?.addView(tabItem)
                }
            }
        }

        home_mtv?.currentTab = presenter.currentTab

    }




    private fun cleanAllPresenter() {
        val adapter = _Adapter ?: return

        // Remove adapter will trigger persit Presenter and ViewState of child fragment.
        home_vp?.adapter = null
        adapter.getSavedStates().forEach {
            _MvpCleaner?.clean(it, false, true) //Keep ViewState for faster register.
        }

        _Adapter = null
    }



    private inner class OnCurrentTabChangedListener : HomeTabView.OnCurrentTabChangedListener {

        override fun onTabChanged(tab: Int) {
            home_vp?.setCurrentItem(tab, false)

        }

        override fun onTabReselected(tab: Int) {
            val screen = _Adapter?.getScreen(tab) ?: return
            if(screen is NewsScreen)
            {
                _Bus.post(ForegroundTabEvent(screen, this@HomeFragment))
            }

        }
    }

    private inner class OnPageChangeListener : ViewPager.OnPageChangeListener {

        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            if (home_mtv?.currentTab != position) {
                home_mtv?.currentTab = position
            }

            val adapter = _Adapter ?: return


            val previousScreen = adapter.getScreen(presenter.currentTab)
            if (previousScreen != null) {
                //_Bus.post(BackgroundTabEvent(previousScreen, this@HomeFragment))
            }

            presenter.currentTab = position

            val currentScreen = adapter.getScreen(presenter.currentTab) ?: return
            _Bus.post(ForegroundTabEvent(currentScreen, this@HomeFragment))
        }
    }

}