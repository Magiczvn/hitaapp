package com.hitasanti.hitaapp.feature.store

import com.hitasanti.hitaapp.dependency.FragmentScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class StoreModule {

    @Provides
    @FragmentScope
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>): StoreContract.Presenter = StorePresenter(useCaseFactory, schedulerFactory)
}
