package com.hitasanti.hitaapp.feature.news

import com.hitasanti.hitaapp.app.adapter.recyclerview.ItemsResult
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.mvp.BasePresenter
import dagger.Lazy
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import timber.log.Timber
import java.util.concurrent.Callable


class NewsPresenter(private val _UseCaseFactory: Lazy<UseCaseFactory>,
                    private val _SchedulerFactory: Lazy<SchedulerFactory>,
                    private val _ItemBuilder: Lazy<NewsItemBuilder>)
    : BasePresenter<NewsContract.View, NewsViewState>(), NewsContract.Presenter {

    //region Private
    private val _PageSize = 40
    private val _WorkerScheduler: Scheduler by lazy {
        _SchedulerFactory.get().single()
    }
    private val _Items = ItemsResult()
    private var _GetNewsDisposable: Disposable? = null
    private var _GetOffersDisposable: Disposable? = null
    private var _ShowNewsDisposable: Disposable? = null

    //endregion Private

    private fun getNews() {
        _GetNewsDisposable?.dispose()
        _GetNewsDisposable = _UseCaseFactory.get().getLatestArticle(10, 524)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_WorkerScheduler)
            .subscribe( Consumer {
              mViewState.articles = it
                showNewsAsync()
            }, ErrorConsumer())
    }

    private fun getOffers() {
        _GetOffersDisposable?.dispose()
        _GetOffersDisposable = _UseCaseFactory.get().getLatestArticle(10, 144)
            .subscribeOn(_SchedulerFactory.get().io())
            .observeOn(_WorkerScheduler)
            .subscribe( Consumer {
                mViewState.offers = it
                showNewsAsync()
            }, ErrorConsumer())
    }

    private fun showHeaderAsync() {
        val work = Callable<Unit> {
            val newItems = _ItemBuilder.get().addHeaderItem(emptyList())
            mViewState.items = newItems
            _Items.pushItems(newItems)
        }

        _ShowNewsDisposable?.dispose()
        _ShowNewsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
            }, ErrorConsumer())
    }

    private fun showNewsAsync() {
        if (mViewState.articles == null || mViewState.offers == null)
            return
        val articles = mViewState.articles.orEmpty()
        val offers = mViewState.offers.orEmpty()

        val work = Callable<Unit> {
            val oldItems = mViewState.items.orEmpty()
            val newItems = _ItemBuilder.get().addNewsArticle(oldItems, articles, offers).orEmpty()
            mViewState.items = newItems
            _Items.pushItems(newItems)
        }

        _ShowNewsDisposable?.dispose()
        _ShowNewsDisposable = _UseCaseFactory.get().doWork(work)
            .subscribeOn(_WorkerScheduler)
            .observeOn(_SchedulerFactory.get().main())
            .subscribe(Consumer {
                showItemResult()
                mView?.notifyNewsLoaded()
            }, ErrorConsumer())
    }


    //region BasePresenter
    override fun onAttachView(view: NewsContract.View) {
        super.onAttachView(view)
        mViewState.items?.let {
            view.showItems(it)
        }
        showHeaderAsync()
        getNews()
        getOffers()
    }

    override fun onDestroy() {
        super.onDestroy()
        _GetNewsDisposable?.dispose()
        _GetOffersDisposable?.dispose()
        _ShowNewsDisposable?.dispose()
    }

    private fun showItemResult() {
        val items = _Items.popItems()

        items?.let { mView?.showItems(it) }
    }
    //endregion BasePresenter

}