package com.hitasanti.hitaapp.feature.order


import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.mvp.BaseViewState
import com.hitasanti.hitaapp.repository.model.Category
import com.hitasanti.hitaapp.repository.model.Product

class OrderViewState(val screen: OrderScreen)
    : BaseViewState() {

    var categories:List<Category>? = null

}