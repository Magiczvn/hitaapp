package com.hitasanti.hitaapp.feature.userinfo

import android.os.Parcel
import android.os.Parcelable
import com.hitasanti.hitaapp.mvp.Screen

class UserInfoScreen()
    : Screen {

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun hashCode() = super.hashCode()

    override fun equals(other: Any?) = other is UserInfoScreen &&  (other === this)


    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<UserInfoScreen> = object : Parcelable.Creator<UserInfoScreen> {
            override fun createFromParcel(parcel: Parcel): UserInfoScreen {

                return UserInfoScreen( )
            }

            override fun newArray(size: Int) = arrayOfNulls<UserInfoScreen?>(size)
        }
    }
}