package com.hitasanti.hitaapp.feature.history

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.repository.model.Order

interface HistoryContract {

    interface View {
        fun showItems(items: List<Item>)
    }

    interface Presenter : com.hitasanti.hitaapp.mvp.Presenter<View, HistoryViewState> {
        fun addNewOrder(order: Order)

        fun orderCancelled()
    }
}