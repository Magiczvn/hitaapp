package com.hitasanti.hitaapp.feature.cart

import android.app.Application
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.ActivityScope
import com.hitasanti.hitaapp.dependency.FragmentScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class CartModule(private val _Activity: CartActivity) {

    @Provides
    @ActivityScope
    internal fun provideNewsItemBuilder(application: Application,
                                        bus: RxBus
    ): NewsItemBuilder = NewsItemBuilder(application,bus)

    @Provides
    @ActivityScope
    internal fun provideGlide(): RequestManager = GlideApp.with(_Activity)

    @Provides
    @ActivityScope
    internal fun provideAdapter(glide: RequestManager): CartAdapter = CartAdapter(glide)


    @Provides
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>,
                                  newsItemBuilder: Lazy<NewsItemBuilder>): CartContract.Presenter = CartPresenter(useCaseFactory, schedulerFactory, newsItemBuilder)
}
