package com.hitasanti.hitaapp.feature.product.event


open class ProductViewEvent(val id: Int, val name:String, val price: Int, val description: String, val imageURL: String)