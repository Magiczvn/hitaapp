package com.hitasanti.hitaapp.feature.setting


import com.hitasanti.hitaapp.mvp.BaseViewState

class SettingViewState(val screen: SettingScreen)
    : BaseViewState() {

    var currentTab = 1

}