package com.hitasanti.hitaapp.feature.main

import com.hitasanti.hitaapp.mvp.BaseViewState


class MainViewState(val forceLogin: Boolean)
    : BaseViewState() {


    var isShowedSplashScreen = false
    var isNoRunForceLogin = false

}
