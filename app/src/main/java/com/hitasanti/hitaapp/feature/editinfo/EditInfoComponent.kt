package com.hitasanti.hitaapp.feature.editinfo

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [EditInfoModule::class])
interface EditInfoComponent {

    val presenter: EditInfoContract.Presenter

    fun inject(activity: EditInfoActivity)
}
