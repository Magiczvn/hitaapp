package com.hitasanti.hitaapp.feature.orderdetails

import com.hitasanti.hitaapp.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [OrderDetailsModule::class])
interface OrderDetailsComponent {

    val presenter: OrderDetailsContract.Presenter

    fun inject(fragment: OrderDetailsActivity)
}
