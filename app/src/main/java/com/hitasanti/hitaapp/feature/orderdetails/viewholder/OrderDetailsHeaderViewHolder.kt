package com.hitasanti.hitaapp.feature.orderdetails.viewholder


import android.view.ViewGroup
import android.widget.EditText

import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.cart.item.AddressItem
import io.reactivex.subjects.Subject
import kotterknife.bindView

class OrderDetailsHeaderViewHolder(parent: ViewGroup,
                            resId: Int,
                            private val _EventSubject: Subject<Any>) :BaseItemViewHolder<AddressItem>(parent, resId) {
    private val _Name: EditText by bindView(R.id.txt_shipping_name)
    private val _Phone: EditText by bindView(R.id.txt_shipping_phone)
    private val _Address: EditText by bindView(R.id.txt_shipping_address)
    private val _Notes: EditText by bindView(R.id.txt_shipping_notes)

    init {

    }

    override fun onBindItem(item: AddressItem) {

        _Name.setText(item.name)
        _Phone.setText(item.phone)
        _Address.setText(item.address)
        _Notes.setText(item.notes)

        super.onBindItem(item)
    }
}