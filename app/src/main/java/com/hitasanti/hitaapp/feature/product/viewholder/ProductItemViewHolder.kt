package com.hitasanti.hitaapp.feature.product.viewholder

import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemViewHolder
import com.hitasanti.hitaapp.feature.product.event.ProductViewEvent
import com.hitasanti.hitaapp.feature.product.item.ProductItem
import com.hitasanti.hitaapp.view.NumberUtil
import io.reactivex.subjects.Subject
import kotterknife.bindView
import java.text.DecimalFormat
import java.text.NumberFormat

class ProductItemViewHolder(parent: ViewGroup,
                            resId: Int,
                            private val _EventSubject: Subject<Any>,
                            private val _Glide: RequestManager) :BaseItemViewHolder<ProductItem>(parent, resId) {

    private val _NameView: TextView by bindView(R.id.product_name)
    private val _PriceView: TextView by bindView(R.id.product_price)
    private val _RegularPriceView: TextView by bindView(R.id.product_regular_price)
    private val _BuyView: TextView by bindView(R.id.product_buy)
    private val _LogoView: ImageView by bindView(R.id.product_logo)
    private val _SaleView: TextView by bindView(R.id.lblSale)


    init {

        this.itemView.setOnClickListener {
            onClick()
        }
    }

    private fun onClick (){
        val productItem = item ?: return

        _EventSubject.onNext(ProductViewEvent(productItem.id, productItem.name, productItem.price, productItem.description, productItem.imageURL))
    }

    override fun onBindItem(item: ProductItem) {
        val oldItem = this.item

        if (oldItem == null || oldItem.name != item.name) {
            _NameView.text = item.name
        }

        if (oldItem == null || oldItem.price != item.price) {
            _PriceView.text = NumberUtil.formatPrice(itemView.context, item.price)
        }

        if (oldItem == null || oldItem.regularPrice != item.regularPrice) {
            val string = SpannableString(NumberUtil.formatPrice(itemView.context, item.regularPrice))
            string.setSpan(StrikethroughSpan(), 0, string.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            _RegularPriceView.text = string
        }

        if(item.regularPrice != item.price){
            _RegularPriceView.visibility = View.VISIBLE
            _PriceView.setTextColor(Color.parseColor("#a40429"))
        }
        else{
            _RegularPriceView.visibility = View.GONE
            _PriceView.setTextColor(Color.BLACK)
        }

        _SaleView.visibility = if(item.onSale) View.VISIBLE else  View.GONE

        if (oldItem == null || oldItem.imageURL != item.imageURL) {
            _Glide.load(item.imageURL)
                .into(_LogoView)
        }

        super.onBindItem(item)
    }
}