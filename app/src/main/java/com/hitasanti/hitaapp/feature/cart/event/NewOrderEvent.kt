package com.hitasanti.hitaapp.feature.cart.event

import com.hitasanti.hitaapp.repository.model.Order

class NewOrderEvent(val order: Order)