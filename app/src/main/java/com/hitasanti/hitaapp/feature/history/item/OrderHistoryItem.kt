package com.hitasanti.hitaapp.feature.history.item

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item
import com.hitasanti.hitaapp.repository.model.Order

class OrderHistoryItem(val order: Order):Item