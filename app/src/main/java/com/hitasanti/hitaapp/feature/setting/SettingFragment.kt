package com.hitasanti.hitaapp.feature.setting

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.HitaApplication
import com.hitasanti.hitaapp.R
import com.hitasanti.hitaapp.common.ErrorConsumer
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.DataCache
import com.hitasanti.hitaapp.dependency.HasComponent
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.feature.history.HistoryActivity
import com.hitasanti.hitaapp.feature.history.HistoryScreen
import com.hitasanti.hitaapp.feature.login.LoginActivity
import com.hitasanti.hitaapp.feature.login.LoginScreen
import com.hitasanti.hitaapp.feature.login.User
import com.hitasanti.hitaapp.feature.login.event.LoginSuccessEvent
import com.hitasanti.hitaapp.feature.userinfo.UserInfoActivity
import com.hitasanti.hitaapp.feature.userinfo.UserInfoScreen
import com.hitasanti.hitaapp.feature.userinfo.event.UpdateInfoSuccessEvent
import com.hitasanti.hitaapp.mvp.BaseMvpFragment
import dagger.Lazy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.setting_fragment.*
import javax.inject.Inject

class SettingFragment : BaseMvpFragment<SettingContract.View, SettingContract.Presenter, SettingViewState, SettingScreen>(),
        HasComponent<SettingComponent>, SettingContract.View {

    companion object {

        fun instance(screen: SettingScreen): SettingFragment {
            val fragment = SettingFragment()
            fragment.setScreen(screen)
            return fragment
        }
    }

    //region Dependency
    @Inject
    lateinit var _SchedulerFactory: SchedulerFactory
    @Inject
    lateinit var _Bus: RxBus
    @Inject
    lateinit var _DataCache: Lazy<DataCache>
    //endregion Dependency

    //region Private
    private var _Adapter: SettingAdapter? = null
    private var _Disposable: CompositeDisposable? = null
    private var _LoginToViewHistory = false
    private var _LoginToViewUserInfo = false
    private var LOGIN_CODE = 1111
    //endregion Private

    //region MVP
    override fun onCreatePresenter(context: Context?) = component.presenter

    override fun onCreateViewState(context: Context?) = SettingViewState(screen)

    override val viewStateTag: String get() = SettingViewState::class.java.name

    override val layoutResource get() = R.layout.setting_fragment
    //endregion MVP

    //region HasComponent
    override val component by lazy {
        HitaApplication.get(context!!).component.plus(SettingModule())
    }
    //endregion HasComponent

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component.inject(this)



        _Disposable = CompositeDisposable( _Bus.register(LoginSuccessEvent::class.java)
            .observeOn(_SchedulerFactory.main())
            .subscribe(Consumer {
                onLoginSuccess(false)
                if(_LoginToViewHistory){
                    _LoginToViewHistory = false
                    showHistory()
                }
                if(_LoginToViewUserInfo){
                    _LoginToViewUserInfo = false
                    showUserInfo()
                }
            }, ErrorConsumer()),
            _Bus.register(UpdateInfoSuccessEvent::class.java)
                .observeOn(_SchedulerFactory.main())
                .subscribe(Consumer {
                    txtUsername.text = User.getUserName()
                }, ErrorConsumer()))

        ll_login.setOnClickListener {
            showLogin()
        }

        ll_history.setOnClickListener {
            showHistory()
        }

        ll_userinfo.setOnClickListener {
            showUserInfo()
        }

        txtLogout.setOnClickListener {
            logOut()
        }

        if(User.isLoggedIn()){
            onLoginSuccess(true)
        }
        else{
            txtLogout.visibility = View.GONE
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun showLogin(){
        val intent = LoginActivity.instance(context!!, LoginScreen())
        startActivityForResult(intent, LOGIN_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == LOGIN_CODE){
            if(requestCode != Activity.RESULT_OK){
                _LoginToViewHistory = false
                _LoginToViewUserInfo = false
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showHistory(){
        if(User.isLoggedIn()) {
            val intent = HistoryActivity.instance(context!!, HistoryScreen())
            startActivity(intent)
        }
        else{
            val toast = Toast.makeText(context!!, "Vui lòng đăng nhập để xem", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0,0)
            toast.show()
            _LoginToViewHistory = true
            showLogin()
        }
    }

    private fun showUserInfo(){
        if(User.isLoggedIn()) {
            val intent = UserInfoActivity.instance(context!!, UserInfoScreen())
            startActivity(intent)
        }
        else{
            val toast = Toast.makeText(context!!, "Vui lòng đăng nhập để xem", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.CENTER, 0,0)
            toast.show()
            _LoginToViewUserInfo = true
            showLogin()
        }
    }

    private fun logOut(){
        User.logout()
        ll_login.isClickable = true
        txtLogout.visibility = View.INVISIBLE
        txtUsername.text = getText(R.string.txtLogin)

        GlideApp.with(this)
            .load(R.drawable.ic_avatar)
            .centerCrop()
            .dontAnimate()
            .into(avatar)

        val toast = Toast.makeText(context!!,"Đăng xuất thành công!", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER,0,0)
        toast.show()
    }

    private fun onLoginSuccess(firstLoad: Boolean){
        txtUsername.text = User.getUserName()
        ll_login.isClickable = false
        txtLogout.visibility = View.VISIBLE
        val avatarUrl = User.getAvatarUrl()
        if(avatarUrl == null) {
            GlideApp.with(this)
                .load(R.drawable.ic_avatar)
                .dontAnimate()
                .into(avatar)
        }
        else{
            GlideApp.with(this)
                .load(avatarUrl)
                .dontAnimate()
                .into(avatar)
        }
        if(firstLoad)
            presenter.getUserInfo()
    }

    override fun onDestroyView() {
        _Disposable?.dispose()

        super.onDestroyView()
    }

    override fun onResume() {
        super.onResume()
    }
    //endregion Lifecycle

    private inner class OnPageChangeListener : ViewPager.OnPageChangeListener {

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {

        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }
    //endregion Helper
}