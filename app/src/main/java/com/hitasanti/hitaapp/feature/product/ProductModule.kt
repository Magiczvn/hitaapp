package com.hitasanti.hitaapp.feature.product

import android.app.Application
import com.bumptech.glide.RequestManager
import com.hitasanti.hitaapp.GlideApp
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.dependency.FragmentScope
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.news.NewsItemBuilder
import dagger.Lazy
import dagger.Module
import dagger.Provides

@Module
class ProductModule(private val _Fragment: ProductFragment) {

    @Provides
    @FragmentScope
    internal fun provideNewsItemBuilder(application: Application,
                                        bus: RxBus
    ): NewsItemBuilder = NewsItemBuilder(application,bus)

    @Provides
    @FragmentScope
    internal fun provideGlide(): RequestManager = GlideApp.with(_Fragment)

    @Provides
    @FragmentScope
    internal fun provideAdapter(glide: RequestManager): ProductAdapter = ProductAdapter(glide)


    @Provides
    internal fun providePresenter(useCaseFactory: Lazy<UseCaseFactory>,
                                  schedulerFactory: Lazy<SchedulerFactory>,
                                  newsItemBuilder: Lazy<NewsItemBuilder>): ProductContract.Presenter = ProductPresenter(useCaseFactory, schedulerFactory, newsItemBuilder)
}
