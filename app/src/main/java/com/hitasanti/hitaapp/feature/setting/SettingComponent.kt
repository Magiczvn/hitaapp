package com.hitasanti.hitaapp.feature.setting

import com.hitasanti.hitaapp.dependency.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [SettingModule::class])
interface SettingComponent {

    val presenter: SettingContract.Presenter

    fun inject(fragment: SettingFragment)
}
