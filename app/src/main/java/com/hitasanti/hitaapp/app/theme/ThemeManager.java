package com.hitasanti.hitaapp.app.theme;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Looper;
import android.util.SparseArray;
import android.view.View;

/**
 * Created by Rey on 10/25/2016.
 */
public class ThemeManager {

    private volatile static ThemeManager mInstance;

    public static final int STYLE_UNDEFINED = 0;

    private SparseArray<int[]> mStyles =  new SparseArray<>();
    private int mCurrentTheme = 0;

    private ViewStyler mViewStyler = new ViewStyler();



    /**
     * Get the singleton instance of ThemeManager.
     * @return The singleton instance of ThemeManager.
     */
    public static ThemeManager getInstance(){
        if(mInstance == null){
            synchronized (ThemeManager.class){
                if(mInstance == null)
                    mInstance = new ThemeManager();
            }
        }

        return mInstance;
    }

    private int[] loadStyleList(Context context, int resId){
        if(context == null)
            return null;

        TypedArray array = context.getResources().obtainTypedArray(resId);
        int[] result = new int[array.length()];
        for(int i = 0; i < result.length; i++)
            result[i] = array.getResourceId(i, 0);
        array.recycle();

        return result;
    }

    private int[] getStyleList(Context context, int styleId){
        if(mStyles == null)
            return null;

        int[] list = mStyles.get(styleId);
        if(list == null){
            list = loadStyleList(context, styleId);
            mStyles.put(styleId, list);
        }

        return list;
    }


    /**
     * Get the current theme.
     * @return The current theme.
     */
    public int getCurrentTheme(){
        return mCurrentTheme;
    }

    /**
     * Set the current theme. Should be called in main thread (UI thread).
     * @param theme The current theme.
     * @return True if set theme successfully, False if method's called on main thread or theme already set.
     */
    public boolean setCurrentTheme(int theme){
        if (Looper.getMainLooper().getThread() != Thread.currentThread())
            return false;

        if(mCurrentTheme != theme){
            mCurrentTheme = theme;
            return true;
        }

        return false;
    }

    /**
     * Get current style of a styleId.
     * @param context The context.
     * @param styleId The styleId.
     * @return The current style.
     */
    public int getCurrentStyle(Context context, int styleId){
        return getStyle(context, styleId, mCurrentTheme);
    }

    /**
     * Get a specific style of a styleId.
     * @param context The context.
     * @param styleId The styleId.
     * @param theme The theme.
     * @return The specific style.
     */
    public int getStyle(Context context, int styleId, int theme){
        int[] styles = getStyleList(context, styleId);
        return styles == null ? 0 : styles[theme];
    }



    public void applyStyle(View view, int styleRes){
        mViewStyler.applyStyle(view, styleRes);
    }

}
