package com.hitasanti.hitaapp.app.adapter.viewpager

import android.view.View

interface ViewHolder<T : Item> {

    fun getView(): View

    fun bind(T: Item)
}