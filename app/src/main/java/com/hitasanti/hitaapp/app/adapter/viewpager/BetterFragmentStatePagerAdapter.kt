package com.hitasanti.hitaapp.app.adapter.viewpager

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.PagerAdapter
import android.view.View
import android.view.ViewGroup

abstract class BetterFragmentStatePagerAdapter(private val mFragmentManager: FragmentManager)
    : PagerAdapter() {

    private var mCurTransaction: FragmentTransaction? = null

    protected var mSavedState = HashMap<String, Fragment.SavedState>()
    private var mFragments = HashMap<String, Fragment>()
    private var mCurrentPrimaryItem: Fragment? = null

    /**
     * Return the Fragment associated with a specified position.
     */
    abstract fun getItem(position: Int): Fragment

    open fun getId(position: Int, fragment: Fragment?): String = "$position"

    override fun startUpdate(container: ViewGroup) {
        if (container.id == View.NO_ID) {
            throw IllegalStateException("ViewPager with adapter " + this
                    + " requires a view id")
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        // If we already have this item instantiated, there is nothing
        // to do. This can happen when we are restoring the entire pager
        // from its saved state, where the fragment manager has already
        // taken care of restoring the fragments we previously had instantiated.
        val id = getId(position, null)
        var fragment = mFragments[id]
        if (fragment != null) {
            return fragment
        }

        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction()
        }

        fragment = getItem(position)

        val savedState = mSavedState[id]
        if (savedState != null) {
            fragment.setInitialSavedState(savedState)
        }

        fragment.setMenuVisibility(false)
        fragment.userVisibleHint = false
        mFragments[id] = fragment
        mCurTransaction?.add(container.id, fragment)

        return fragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val fragment = `object` as Fragment
        val id = getId(position, fragment)

        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction()
        }
        if (fragment.isAdded) {
            val state = mFragmentManager.saveFragmentInstanceState(fragment)
            if (state != null) {
                mSavedState[id] = state
            } else {
                mSavedState.remove(id)
            }
        } else {
            mSavedState.remove(id)
        }
        mFragments.remove(id)

        mCurTransaction?.remove(fragment)
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        val fragment = `object` as? Fragment
        if (fragment !== mCurrentPrimaryItem) {
            mCurrentPrimaryItem?.setMenuVisibility(false)
            mCurrentPrimaryItem?.userVisibleHint = false
            fragment?.setMenuVisibility(true)
            fragment?.userVisibleHint = true
            mCurrentPrimaryItem = fragment
        }
    }

    override fun finishUpdate(container: ViewGroup) {
        mCurTransaction?.commitNowAllowingStateLoss()
        mCurTransaction = null
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return (`object` as Fragment).view === view
    }

    override fun saveState(): Parcelable? {
        val bundle = Bundle()

        mSavedState.entries.forEach {
            bundle.putParcelable("s${it.key}", it.value)
        }

        mFragments.entries.forEach {
            if (it.value.isAdded) {
                mFragmentManager.putFragment(bundle, "f${it.key}", it.value)
            }
        }

        return bundle
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
        val bundle = state as? Bundle ?: return
        bundle.classLoader = loader
        mSavedState.clear()
        mFragments.clear()

        val keys = bundle.keySet()
        for (key in keys) {
            if (key.startsWith("s")) {
                val id = key.substring(1)
                val savedState = bundle.getParcelable<Fragment.SavedState>(key)?:continue
                mSavedState[id] = savedState
            }
            if (key.startsWith("f")) {
                val id = key.substring(1)
                var f: Fragment? = null
                try {
                    f = mFragmentManager.getFragment(bundle, key)
                } catch (e: Exception) {
                }

                if (f != null) {
                    f.setMenuVisibility(false)
                    mFragments[id] = f
                }
            }
        }
    }
}