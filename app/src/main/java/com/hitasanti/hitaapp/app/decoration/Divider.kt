package com.hitasanti.hitaapp.app.decoration

data class Divider(val height: Int,
                   val paddingLeftRight: Int?,
                   val color: Int?,
                   val bgColor: Int?)