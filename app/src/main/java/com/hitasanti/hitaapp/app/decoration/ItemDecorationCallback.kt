package com.hitasanti.hitaapp.app.decoration

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item

interface ItemDecorationCallback {
    fun shouldDecor(item: Item, nextItem: Item?): Boolean
}