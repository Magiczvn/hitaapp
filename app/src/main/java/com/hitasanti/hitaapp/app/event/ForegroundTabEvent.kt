package com.hitasanti.hitaapp.app.event

import com.hitasanti.hitaapp.mvp.Screen

class ForegroundTabEvent(val screen: Screen,
                         val sender: Any)