package com.hitasanti.hitaapp.app.decoration

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.hitasanti.hitaapp.view.ColorUtil


class VerticalDividerDecoration(private val _Divider: Divider,
                                reverse: Boolean,
                                callback: ItemDecorationCallback)
    : BaseItemDecoration(reverse, callback) {

    private val _Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        _Paint.strokeWidth = _Divider.height.toFloat()
        _Paint.style = Paint.Style.STROKE
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        if (shouldDecor(parent, position)) {
            if (mReverse)
                outRect.top = _Divider.height
            else
                outRect.bottom = _Divider.height
        }
    }

    override fun onDrawDecor(c: Canvas, parent: RecyclerView, state: RecyclerView.State, child: View) {
        if (mReverse) {
            val params = child.layoutParams as RecyclerView.LayoutParams
            val y = child.top - params.topMargin + child.translationY - _Divider.height / 2f
            if (y > -(_Divider.height + 1) / 2f) {
                if (_Divider.paddingLeftRight != null && _Divider.bgColor != null) {
                    drawLine(c, _Divider.bgColor, parent.paddingLeft.toFloat(), parent.width.toFloat() - parent.paddingRight, y, child.alpha)
                    if (_Divider.color != null) {
                        drawLine(c, _Divider.color, parent.paddingLeft.toFloat() + _Divider.paddingLeftRight, parent.width.toFloat() - parent.paddingRight - _Divider.paddingLeftRight, y, child.alpha)
                    }
                } else if (_Divider.color != null) {
                    drawLine(c, _Divider.color, parent.paddingLeft.toFloat(), parent.width.toFloat() - parent.paddingRight, y, child.alpha)
                }
            }
        } else {
            val params = child.layoutParams as RecyclerView.LayoutParams
            val y = child.bottom.toFloat() + params.bottomMargin.toFloat() + child.translationY + _Divider.height / 2f
            if (y < parent.height + (_Divider.height + 1) / 2f) {
                if (_Divider.paddingLeftRight != null && _Divider.bgColor != null) {
                    drawLine(c, _Divider.bgColor, parent.paddingLeft.toFloat(), parent.width.toFloat() - parent.paddingRight, y, child.alpha)
                    if (_Divider.color != null) {
                        drawLine(c, _Divider.color, parent.paddingLeft.toFloat() + _Divider.paddingLeftRight, parent.width.toFloat() - parent.paddingRight - _Divider.paddingLeftRight, y, child.alpha)
                    }
                } else if (_Divider.color != null) {
                    drawLine(c, _Divider.color, parent.paddingLeft.toFloat(), parent.width.toFloat() - parent.paddingRight, y, child.alpha)
                }
            }
        }
    }

    private fun drawLine(c: Canvas, color: Int, left: Float, right: Float, y: Float, alpha: Float) {
        _Paint.color = ColorUtil.getColor(color, alpha)
        c.drawLine(left, y, right, y, _Paint)
    }
}