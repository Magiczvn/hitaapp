package com.hitasanti.hitaapp.app.decoration

import android.graphics.Canvas
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.hitasanti.hitaapp.app.adapter.recyclerview.BaseItemAdapter
import com.hitasanti.hitaapp.app.adapter.recyclerview.Item


open class BaseItemDecoration(val mReverse: Boolean,
                              private val _Callback: ItemDecorationCallback)
    : RecyclerView.ItemDecoration() {

    fun getItemAt(adapter: RecyclerView.Adapter<*>?, position: Int): Item? {
        if (adapter !is BaseItemAdapter)
            throw IllegalArgumentException("Adapter should be extended from BaseItemAdapter.")

        return adapter.getItemAt(position)
    }

    fun shouldDecor(parent: RecyclerView, position: Int): Boolean {
        val item = getItemAt(parent.adapter, position) ?: return false
        return _Callback.shouldDecor(item, getItemAt(parent.adapter, getNextPosition(position)))
    }

    private fun getNextPosition(position: Int) = if (mReverse) position - 1 else position + 1

    open fun onDrawDecor(c: Canvas, parent: RecyclerView, state: RecyclerView.State, child: View) {}

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val layoutManager = parent.layoutManager as LinearLayoutManager

        val firstVisiblePosition = layoutManager.findFirstVisibleItemPosition()
        val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()

        if (firstVisiblePosition < 0) {
            return
        }

        if (mReverse) {
            for (i in lastVisiblePosition downTo firstVisiblePosition) {
                val child = layoutManager.findViewByPosition(i) ?: continue
                if (child.visibility != View.GONE && shouldDecor(parent, i))
                    onDrawDecor(c, parent, state, child)
            }
        } else {
            for (i in firstVisiblePosition..lastVisiblePosition) {
                val child = layoutManager.findViewByPosition(i) ?: continue
                if (child.visibility != View.GONE && shouldDecor(parent, i))
                    onDrawDecor(c, parent, state, child)
            }
        }
    }
}