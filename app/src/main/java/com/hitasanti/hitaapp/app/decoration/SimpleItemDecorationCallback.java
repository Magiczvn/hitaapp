package com.hitasanti.hitaapp.app.decoration;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hitasanti.hitaapp.app.adapter.recyclerview.Item;


public class SimpleItemDecorationCallback implements ItemDecorationCallback {

    private Class<? extends Item>[] mClasses;

    @SafeVarargs
    public SimpleItemDecorationCallback(Class<? extends Item>... classes) {
        mClasses = classes;
    }

    protected boolean valid(Item item){
        if(item != null)
            for(Class clazz : mClasses)
                if(clazz.isAssignableFrom(item.getClass()))
                    return true;
        return false;
    }

    @Override
    public boolean shouldDecor(@NonNull Item item, @Nullable Item nextItem) {
        return nextItem != null && valid(item);
    }
}
