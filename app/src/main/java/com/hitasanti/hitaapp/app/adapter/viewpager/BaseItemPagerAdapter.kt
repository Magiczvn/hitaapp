package com.hitasanti.hitaapp.app.adapter.viewpager

import androidx.viewpager.widget.PagerAdapter
import android.view.View
import android.view.ViewGroup
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import kotlin.collections.ArrayList

abstract class BaseItemPagerAdapter<V : View, U> : PagerAdapter() {

    val event: Subject<Any> = PublishSubject.create<Any>().toSerialized()
    private var _Items: List<U>? = null
    private val _CachedView = ArrayList<V>()
    protected val mUseView = HashMap<Int, V>()

    fun setItems(items: List<U>?) {
        _Items = items
        notifyDataSetChanged()
    }

    fun getItemAt(index: Int?): U? {
        val items = _Items ?: return null
        return when {
            index == null -> null
            index < 0 -> null
            index > items.size - 1 -> null
            else -> items[index]
        }
    }

    fun getViewAt(index: Int?): V? = when(index) {
        null -> null
        else -> mUseView[index]
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: V
        if (_CachedView.isEmpty()) {
            view = createView(container, position)
        } else {
            view = _CachedView.last()
            _CachedView.remove(view)
        }
        bindView(view, position)
        container.addView(view)
        mUseView[position] = view
        return view
    }

    @Suppress("UNCHECKED_CAST")
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val view = `object` as? V ?: return
        destroyView(view)
        container.removeView(view)
        _CachedView.add(view)
        mUseView.remove(position)
    }

    override fun isViewFromObject(view: View, `object`: Any) = view === `object`

    override fun getCount() = _Items?.size ?: 0

    override fun getItemPosition(`object`: Any) = PagerAdapter.POSITION_NONE

    abstract fun createView(container: ViewGroup, position: Int): V
    abstract fun bindView(view: V, position: Int)
    abstract fun destroyView(view: V)
}