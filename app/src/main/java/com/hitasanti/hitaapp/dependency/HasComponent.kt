package com.hitasanti.hitaapp.dependency

interface HasComponent<T> {

    val component: T
}
