package com.hitasanti.hitaapp.dependency

import com.google.gson.Gson
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.data.news.ApiNewsDataSource
import com.hitasanti.hitaapp.data.news.NewsApi
import com.hitasanti.hitaapp.repository.NewsRepository
import com.hitasanti.hitaapp.repository.datasource.NewsDataSource
import com.hitasanti.hitaapp.repository.impl.NewsRepositoryImpl
import dagger.Lazy
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
internal class NewsRepositoryModule {
    @Provides
    @Singleton
    fun provideContentApi(@Type("oauth") client: OkHttpClient, gson: Gson): NewsApi = Retrofit.Builder()
        .baseUrl(BuildConfig.ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(client)
        .build()
        .create(NewsApi::class.java)

    @Provides
    @Singleton
    internal fun provideNewsDataSource(api: NewsApi): NewsDataSource = ApiNewsDataSource(api)

    @Provides
    @Singleton
    fun provideNewsRepository(networkDataSourceLazy: Lazy<NewsDataSource>): NewsRepository = NewsRepositoryImpl(networkDataSourceLazy)

}
