package com.hitasanti.hitaapp.dependency

import com.hitasanti.hitaapp.mvp.Screen

interface DataCache {


    fun putScreen(key: String, screen: Screen)

    fun popScreen(key: String): Screen?
}
