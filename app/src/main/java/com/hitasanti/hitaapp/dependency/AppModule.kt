package com.hitasanti.hitaapp.dependency

import android.app.ActivityManager
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.telephony.TelephonyManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.data.HeaderInterceptor
import com.hitasanti.hitaapp.data.MemoryDataCache
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.domain.impl.SchedulerFactoryImpl
import com.hitasanti.hitaapp.domain.impl.UseCaseFactoryImpl
import com.hitasanti.hitaapp.repository.NewsRepository
import com.hitasanti.hitaapp.repository.ProductRepository
import dagger.Lazy
import dagger.Module
import dagger.Provides
import io.paperdb.Paper
import oauth.signpost.signature.QueryStringSigningStrategy
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer
import se.akerfeldt.okhttp.signpost.SigningInterceptor
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class AppModule(private val _Application: Application) {

    init {
        Paper.init(_Application)
    }

    @Provides
    @Singleton
    internal fun provideApplication(): Application = _Application

    @Provides
    @Singleton
    internal fun provideBus(): RxBus = RxBus()

    @Provides
    @Singleton
    internal fun provideSharesPreferences(application: Application): SharedPreferences = application.getSharedPreferences(application.packageName, Application.MODE_PRIVATE)



    @Provides
    @Singleton
    @Type("oauth")
    internal fun provideOkHttpClient(cache: Cache): OkHttpClient {
        val consumer = OkHttpOAuthConsumer("ck_5bb3445d2ebe56a82db0a0e453a29e066e1442f6", "cs_681802c072f1713cc57748040b4b70b620844493")
        consumer.setSigningStrategy(QueryStringSigningStrategy())
        val builder = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(SigningInterceptor(consumer))
            .addInterceptor(HeaderInterceptor())
            .cache(cache)

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            builder.addInterceptor(logging)
        }

        return builder.build()
    }

    @Provides
    @Singleton
    internal fun provideConnectivityManager(application: Application): ConnectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    @Provides
    @Singleton
    internal fun provideActivityManager(application: Application): ActivityManager = application.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

    @Provides
    @Singleton
    internal fun provideTelephonyManager(application: Application): TelephonyManager = application.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

    @Provides
    @Singleton
    internal fun provideGson(): Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .setLenient()
            .create()

    @Provides
    @Singleton
    internal fun provideOkHttpCache(application: Application): Cache = Cache(File(application.cacheDir, "HttpResponseCache"), 10L * 1024L * 1024L)


    @Provides
    @Singleton
    internal fun provideSchedulerFactory(): SchedulerFactory = SchedulerFactoryImpl()

    @Provides
    @Singleton
    internal fun provideUseCaseFactory(newsRepositoryLazy: Lazy<NewsRepository>,
                                       productRepositoryLazy: Lazy<ProductRepository>): UseCaseFactory = UseCaseFactoryImpl(newsRepositoryLazy, productRepositoryLazy)

    @Provides
    @Singleton
    internal fun provideDataCache(): DataCache = MemoryDataCache()

    @Provides
    @Singleton
    @Type("activity_stack")
    internal fun provideActivityStack(): MutableList<String> = ArrayList()

    @Provides
    @Singleton
    @Type("current_activity_stack")
    internal fun provideCurrentActivityStack(): MutableList<String> = ArrayList()

}
