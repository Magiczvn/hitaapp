package com.hitasanti.hitaapp.dependency

import com.google.gson.Gson
import com.hitasanti.hitaapp.BuildConfig
import com.hitasanti.hitaapp.data.product.ApiProductDataSource
import com.hitasanti.hitaapp.data.product.ProductApi
import com.hitasanti.hitaapp.repository.ProductRepository
import com.hitasanti.hitaapp.repository.datasource.ProductDataSource
import com.hitasanti.hitaapp.repository.impl.ProductRepositoryImpl
import dagger.Lazy
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
internal class ProductRepositoryModule {
    @Provides
    @Singleton
    fun provideContentApi(@Type("oauth") client: OkHttpClient, gson: Gson): ProductApi = Retrofit.Builder()
        .baseUrl(BuildConfig.ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(client)
        .build()
        .create(ProductApi::class.java)

    @Provides
    @Singleton
    internal fun provideNewsDataSource(api: ProductApi): ProductDataSource = ApiProductDataSource(api)

    @Provides
    @Singleton
    fun provideNewsRepository(networkDataSourceLazy: Lazy<ProductDataSource>): ProductRepository = ProductRepositoryImpl(networkDataSourceLazy)
}