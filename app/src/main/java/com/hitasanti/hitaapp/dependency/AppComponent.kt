package com.hitasanti.hitaapp.dependency

import android.app.Application
import android.graphics.drawable.Drawable
import android.telephony.TelephonyManager

import com.hitasanti.hitaapp.common.RxBus
import com.hitasanti.hitaapp.domain.SchedulerFactory
import com.hitasanti.hitaapp.domain.UseCaseFactory
import com.hitasanti.hitaapp.feature.cart.CartComponent
import com.hitasanti.hitaapp.feature.cart.CartModule
import com.hitasanti.hitaapp.feature.categories.CategoriesComponent
import com.hitasanti.hitaapp.feature.categories.CategoriesModule
import com.hitasanti.hitaapp.feature.editinfo.EditInfoComponent
import com.hitasanti.hitaapp.feature.editinfo.EditInfoModule
import com.hitasanti.hitaapp.feature.history.HistoryComponent
import com.hitasanti.hitaapp.feature.history.HistoryModule
import com.hitasanti.hitaapp.feature.home.HomeComponent
import com.hitasanti.hitaapp.feature.home.HomeModule
import com.hitasanti.hitaapp.feature.login.LoginComponent
import com.hitasanti.hitaapp.feature.login.LoginModule
import com.hitasanti.hitaapp.feature.main.MainComponent
import com.hitasanti.hitaapp.feature.main.MainModule
import com.hitasanti.hitaapp.feature.news.NewsComponent
import com.hitasanti.hitaapp.feature.news.NewsModule
import com.hitasanti.hitaapp.feature.order.OrderComponent
import com.hitasanti.hitaapp.feature.order.OrderModule
import com.hitasanti.hitaapp.feature.orderdetails.OrderDetailsComponent
import com.hitasanti.hitaapp.feature.orderdetails.OrderDetailsModule
import com.hitasanti.hitaapp.feature.product.ProductComponent
import com.hitasanti.hitaapp.feature.product.ProductModule
import com.hitasanti.hitaapp.feature.setting.SettingComponent
import com.hitasanti.hitaapp.feature.setting.SettingModule
import com.hitasanti.hitaapp.feature.store.StoreComponent
import com.hitasanti.hitaapp.feature.store.StoreModule
import com.hitasanti.hitaapp.feature.userinfo.UserInfoComponent
import com.hitasanti.hitaapp.feature.userinfo.UserInfoModule
import com.hitasanti.hitaapp.feature.web.WebComponent
import com.hitasanti.hitaapp.feature.web.WebModule
import com.hitasanti.hitaapp.feature.webtab.WebTabComponent
import com.hitasanti.hitaapp.feature.webtab.WebTabModule
import com.hitasanti.hitaapp.repository.model.OrderDetail
import com.hitasanti.hitaapp.repository.model.UserInfo
import dagger.Component
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NewsRepositoryModule::class, ProductRepositoryModule::class])
interface AppComponent {

    val application: Application
    val bus: RxBus

    operator fun plus(mainModule: MainModule): MainComponent

    operator fun plus(homeModule: HomeModule): HomeComponent

    operator fun plus(newsModule: NewsModule): NewsComponent

    operator fun plus(userInfoModule: UserInfoModule): UserInfoComponent

    operator fun plus(editInfoModule: EditInfoModule): EditInfoComponent

    operator fun plus(loginModule: LoginModule): LoginComponent

    operator fun plus(categoriesModule: CategoriesModule): CategoriesComponent

    operator fun plus(orderModule: OrderModule): OrderComponent

    operator fun plus(orderDetailsModule: OrderDetailsModule): OrderDetailsComponent

    operator fun plus(productModule: ProductModule): ProductComponent

    operator fun plus(cartModule: CartModule): CartComponent

    operator fun plus(storeModule: StoreModule): StoreComponent

    operator fun plus(settingModule: SettingModule): SettingComponent

    operator fun plus(webModule: WebModule): WebComponent

    operator fun plus(historyModule: HistoryModule): HistoryComponent

    operator fun plus(webTabModule: WebTabModule): WebTabComponent

    val useCaseFactory: UseCaseFactory
    val schedulerFactory: SchedulerFactory
    val dataCache: DataCache

    @get:Type("activity_stack")
    val activityStack: MutableList<String>

    @get:Type("current_activity_stack")
    val currentActivityStack: MutableList<String>

    val telephonyManager: TelephonyManager

}
